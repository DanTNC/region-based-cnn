import argparse
import os
import shutil

import mlflow
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision

import models
from configs.arg_helper import (add_cnn_config_args, model_according_to,
                                model_path_by)
from configs.cnn_config import load_config_into
from data.dataset import get_data_loaders
from utils import loss as custom_losses
from utils.criteria import Scheduler, make_criteria
from utils.mlflow_helper import get_tracking_uri
from utils.model_manager import load_checkpoint, save_checkpoint
from utils.torch_helper import auto_device, auto_parallel
from utils.training_helper import score, training_loop

parser = argparse.ArgumentParser(description='PyTorch CNN Training')

add_cnn_config_args(parser)
parser.add_argument('--print-interval', type=int, default=100,
                    help='print loss for each this # of iterations')
parser.add_argument('--at', default=False, action='store_true',
                    help='whether to use adversarial training instead of TRADES loss')
args = parser.parse_args()

mlflow.set_tracking_uri(get_tracking_uri())
mlflow.set_experiment("training")

model_type = model_according_to(args.model, args.dataset)
load_config_into(__import__(__name__), model_type, models, custom_losses, natural=args.natural, training=True)

if not os.path.exists(model_dir):
    os.makedirs(model_dir)

device, kwargs = auto_device()

model = use_model(pretrained=pretrained, **model_init_kwargs) if pretrained else use_model(**model_init_kwargs)

model = auto_parallel(model)

start_epoch = 0

train_loader, test_loader = get_data_loaders(torch.cuda.device_count() * 16, dataset, kwargs,
                                            transform_train=transform_train, transform_test=transform_test,
                                            download=args.download)

model.to(device)

optimizer = getattr(torch.optim, opt)(model.parameters(), lr=lr, **opt_kwargs)

if "trigger_epochs" in early_stop_kwargs and early_stop_kwargs["trigger_epochs"] is not None:
    scheduler = Scheduler(lr, optimizer, lr_decay_rate, lr_decay_max_times, mlflow=mlflow)

    early_stop_kwargs["scheduler"] = scheduler

early_stop = make_criteria(**early_stop_kwargs)

print(early_stop.profile())

e_loss = lambda y, y_star, x: loss(y, y_star)
if not args.natural:
    if args.at:
        Loss = lambda y, y_star, x: custom_losses.AT_loss(model=model,
                                x_natural=x,
                                y=y_star,
                                optimizer=optimizer,
                                step_size=step_size,
                                epsilon=epsilon_,
                                perturb_steps=num_steps,
                                loss_func=loss)
    else:
        Loss = lambda y, y_star, x: custom_losses.trades_loss(model=model,
                                x_natural=x,
                                y=y_star,
                                optimizer=optimizer,
                                step_size=step_size,
                                epsilon=epsilon_,
                                perturb_steps=num_steps,
                                beta=beta,
                                loss_func=loss)
else:
    Loss = e_loss

mlflow.start_run()

mlflow.log_param('model', model_type)
mlflow.log_param('opt', str(optimizer).replace("\n", "&"))
mlflow.log_param('dataset', dataset)
mlflow.log_param('loss', str(loss).replace("\n", "&"))
mlflow.log_param('learning_rate', lr)
mlflow.log_param('epsilon', epsilon_)
mlflow.log_param('num_steps', num_steps)
mlflow.log_param('step_size', step_size)
mlflow.log_param('beta', beta)
mlflow.log_param('model_dir', model_dir)
mlflow.log_param('pretrained', str(pretrained))
mlflow.log_param('early_stop', str(early_stop.profile()))
mlflow.log_param('natural', str(args.natural))

model, epoch = training_loop(model, train_loader, Loss, optimizer, early_stop, device,
                test_data_loader=test_loader, print_interval=args.print_interval,
                save_func=lambda model, epoch: (
                    torch.save(model.state_dict(), '{}/model-epoch{:03d}.pt'.format(model_dir, epoch)),
                    mlflow.log_artifact('{}/model-epoch{:03d}.pt'.format(model_dir, epoch)),
                    print('got better model')
                ), start_epoch=start_epoch, e_loss=e_loss,
                mlflow_track=True)

fse = early_stop.final_saved_epoch
test_score = score(model, device, test_loader)
print("final saved epoch:", fse)
print("testing accuracy:", test_score)

mlflow.log_metric('final_saved_epoch', fse)
mlflow.log_metric('test_acc', test_score)
mlflow.set_tag('stopping reason', str(early_stop))

yn = input("upload the final saved model to S3? [y/n]\n")
if yn.upper() == "Y":
    tag = input("set a tag for this model: (use timestamp if left blank)\n")
    if tag == "":
        tag = "latest"
    state_dict = torch.load('{}/model-epoch{:03d}.pt'.format(model_dir, fse))
    tag = save_checkpoint(model_path_by(dataset, args.natural, mat=False), state_dict, tag=tag)

    mlflow.set_tag('tag', tag)

mlflow.end_run()
