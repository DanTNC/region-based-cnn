import argparse
import os
from heapq import heapify, heappop, heappush, heappushpop

import foolbox
import joblib
import matplotlib.pyplot as plt
import mlflow
import numpy as np
import torch
import torchvision.transforms as T
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import _hierarchical_fast as _hierarchical
from sklearn.metrics import roc_auc_score, roc_curve

import models
import utils.loss as custom_losses
from configs.arg_helper import (add_cnn_config_args, model_according_to,
                                model_path_by)
from configs.attack_config import ATTACK_CONFIGS
from configs.clustering_config import \
    load_config_into as load_clustering_config_into
from configs.cnn_config import DATASET_MODEL_MAPPING, load_config_into
from configs.constant import NUM_CLASSES
from data.dataset import get_dataset
from models.MAT import MAT
from utils.adversarial import create_attacks
from utils.cluster_helper import PseudoClusters, TorchClusterPredictor
from utils.data_helper import CountingBin, cast2numpy, channel_mean, t_l2_norm
from utils.function_helper import Cascade
from utils.mlflow_helper import get_tracking_uri
from utils.model_manager import load_checkpoint
from utils.path_helper import (PathWDefaults, joblib_exists, joblib_path,
                               output_exists, output_path)
from utils.torch_helper import (auto_device, auto_no_final_parallel,
                                auto_parallel)
from utils.training_helper import score

output_path_maker = PathWDefaults('clustering', '.pt')
skmodel_path_maker = PathWDefaults('clustering', '.joblib')

ATTACKS = ATTACK_CONFIGS['ALL']
NUM_CLASSES_ = 10
sigmoid = torch.sigmoid

def heterogeneity(x):
    """
    Compute the heterogeneity of a class mapping vector by
            sigma(x)
        ----------------  -  1
        sqrt(sigma(x^2))
    --------------------------------- = rescaled cosine ... [0, 1]
                sqrt(n) - 1
    Args:
        x [list]: class mapping vector
    Returns:
        [float]: heterogeneity
    """
    n = len(x)
    x = np.array(x)
    return ((x.sum() / ((x ** 2).sum() ** 0.5)) - 1) / (n ** 0.5 - 1)

def single_dominance(x):
    """
    Compute the single dominance of a class mapping vector.
    Args:
        x [list]: class mapping vector
    Returns:
        [float]: single dominance
    """
    x = list(sorted(x))
    S, M = x[-2:]
    return (M - S) / M

def create_mapping(cluster_lbls, class_lbls):
    """
    Create the class mapping vectors of a clustering result.
    Args:
        cluster_lbls [numpy.array]: cluster labels
        class_lbls [numpy.array]: class labels
    Returns:
        [dict]: class mapping vectors of all clusters
    """
    mapping = {}
    for cls, lbl in zip(cluster_lbls, class_lbls):
        if cls not in mapping:
            mapping[cls] = [0] * NUM_CLASSES_
        mapping[cls][lbl] += 1
    return mapping

def _hc_cut(n_clusters, children, n_leaves):
    """
    Note: this bit of code is from sklearn source.
    """
    nodes = [-(max(children[-1]) + 1)]
    for _ in range(n_clusters - 1):
        # As we have a heap, nodes[0] is the smallest element
        these_children = children[-nodes[0] - n_leaves]
        # Insert the 2 children and remove the largest node
        heappush(nodes, -these_children[0])
        heappushpop(nodes, -these_children[1])
    label = np.zeros(n_leaves, dtype=np.intp)
    for i, node in enumerate(nodes):
        label[_hierarchical._hc_get_descendent(-node, children, n_leaves)] = i
    return label

def get_clusters(clustering, preds, threshold):
    """
    Run clustering analysis.
    Note: this bit of code imitate how sklearn source code applies distance_threshold.
    Args:
        clustering [sklearn.cluster.AgglomerativeClustering]: sklearn clustering object
        preds [numpy.array]: model predictions
        threshold [float]: distance threshold
    Returns:
        [sklearn.cluster.AgglomerativeClustering], [dict]: sklearn clustering object, class mapping vectors
    """
    n_clusters = np.count_nonzero(clustering.distances_ >= threshold) + 1
    clustering.n_clusters_ = n_clusters
    clustering.labels_ = _hc_cut(n_clusters, clustering.children_, clustering.n_leaves_)
    mapping = create_mapping(clustering.labels_, preds)
    return clustering, mapping

def init_clusters(features):
    """
    Initialize clustering object.
    [sklearn.cluster.AgglomerativeClustering]: sklearn clustering object
    """
    ms = AgglomerativeClustering(n_clusters=None, distance_threshold=1)
    ms.fit(features)
    return ms

def find_good_threshold(features, preds, init_threshold, step, max_threshold, model_path):
    """
    Grid search-like algorithm for finding the maximum good threshold.
    Args:
        features [numpy.array]: features to run clustering
        preds [numpy.array]: model predictions
        init_threshold [float]: initial threshold of search
        step [float]: step size of search
        max_threshold [float]: maximum threshold of search
        model_path [str]: path of the clustering object
    Returns:
        [float]: maximum good threshold
    """
    h_bins = CountingBin([0.4, 0.7])
    s_bins = CountingBin([0.4, 0.7])
    first_good = init_threshold
    if os.path.isfile(model_path):
        clustering = load_sk_model(model_path)
    else:
        clustering = init_clusters(features)
        save_sk_model(clustering, model_path)
    clstrs, mapping = get_clusters(clustering, preds, first_good)
    good, rs = is_good(mapping, h_bins, s_bins)
    while not good:
        print(first_good, good, len(mapping), rs)
        first_good += step
        if first_good >= max_threshold:
            raise ValueError("The threshold in the range is never good")
        clstrs, mapping = get_clusters(clustering, preds, first_good)
        good, rs = is_good(mapping, h_bins, s_bins)
    first_bad = first_good
    while good:
        first_bad += step
        clstrs, mapping = get_clusters(clustering, preds, first_bad)
        good, rs = is_good(mapping, h_bins, s_bins)
        print(first_bad, good, len(mapping), rs)
    finer = first_bad - (step / 2)
    clstrs, mapping = get_clusters(clustering, preds, finer)
    good, rs = is_good(mapping, h_bins, s_bins)
    return finer if good else first_bad - step

def is_good(mapping, h_bins, s_bins, distance_threshold=None, i=None, alpha=0.7, beta=0.6):
    """
    Check if a threshold is good.
    Args:
        mapping [dict]: class mapping vectors
        h_bins [CountingBin]: histogram counting number of clusters with different level of heterogeneity
        s_bins [CountingBin]: histogram counting number of clusters with different level of single dominance
        distance_threshold [float]: distance threshold
        i [int]: index of mlflow logging
        alpha [float]: threshold of high SD percentage
        beta [float]: threshold of heterogeity for low SD clusters
    Returns:
        [bool], [list]: whether it is good, list of not good reasons
    """
    if not isinstance(h_bins, CountingBin):
        h_bins = CountingBin(h_bins)
    if not isinstance(s_bins, CountingBin):
        s_bins = CountingBin(s_bins)
    h_bins.clear()
    s_bins.clear()
    s_not_high_hs = []
    is_dom = [0] * NUM_CLASSES_
    to_log = distance_threshold is not None
    for cls, lbls in mapping.items():
        h = heterogeneity(lbls)
        s = single_dominance(lbls)
        h_bins.add(h)
        b = s_bins.add(s)
        if b != 2:
            s_not_high_hs.append(h)
        dom_cls = np.argmax(lbls)
        is_dom[dom_cls] += 1
        if to_log:
            mlflow.log_metric('heterogeneity_{}'.format(distance_threshold), h, step=cls)
            mlflow.log_metric('single_dominance_{}'.format(distance_threshold), s, step=cls)
    h_res = h_bins.percentages()
    s_res = s_bins.percentages()
    if to_log:
        mlflow.log_metric('h_low', h_res[0], step=i)
        mlflow.log_metric('h_medium', h_res[1], step=i)
        mlflow.log_metric('h_high', h_res[2], step=i)
        mlflow.log_metric('s_low', s_res[0], step=i)
        mlflow.log_metric('s_medium', s_res[1], step=i)
        mlflow.log_metric('s_high', s_res[2], step=i)

    reasons = []
    if not np.greater(is_dom, 0).all():
        reasons.append("not all dom")
    if not (s_res[2] > alpha):
        reasons.append("high SD only {}".format(s_res[2]))
    if not ((len(s_not_high_hs) == 0) or np.less(s_not_high_hs, beta).all()):
        reasons.append("not all low SD clusters have low H")
    return len(reasons) == 0, reasons

def save_sk_model(model, path):
    """
    Save sklearn clustering object.
    Args:
        model [sklearn.cluster.AgglomerativeClustering]: sklearn clustering object
        path [str]: path to save
    Returns:
        None
    """
    joblib.dump(model, path)

def load_sk_model(path):
    """
    Load sklearn clustering object.
    Args:
        path [str]: path to load
    Returns:
        model [sklearn.cluster.AgglomerativeClustering]: sklearn clustering object
    """
    return joblib.load(path)

def infer_features(model, device, kwargs, observe_testing, download=False):
    """
    Inference of the features of a certain dataset.
    Args:
        model [torch.nn.Module]: model for inference
        device [torch.device]: device of model
        kwargs [dict]: keyword parameters for the data loader
        observe_testing [bool]: whether to compute features of the testing datasets
    Returns:
        [torch.Tensor], [torch.Tensor], [torch.Tensor]: features, predictions, class labels
    """
    global transform_test
    data_loader = torch.utils.data.DataLoader(
        get_dataset(dataset, train=(not observe_testing), transform=transform_test, download=download),
        batch_size=torch.cuda.device_count() * 16, shuffle=False, **kwargs
    )

    print(score(model, device, data_loader))

    fmodel = auto_no_final_parallel(model).to(device)

    features = []
    preds = []
    labels = []

    with torch.no_grad():
        for data, target in data_loader:
            features.append(fmodel(data.to(device)))
            preds.append(model(data.to(device)).max(1)[1])
            labels.append(target)

    features = torch.cat(features, dim=0)
    preds = torch.cat(preds, dim=0)
    labels = torch.cat(labels, dim=0)
    return features, preds, labels

def save_features(features, preds, labels, model_name, tag, observe_testing):
    """
    Save inference outputs.
    Args:
        features [torch.Tensor]: features to save
        preds [torch.Tensor]: predictions to save
        labels [torch.Tensor]: class labels to save
        model_name [str]: model name
        tag [str]: tag
        observe_testing [bool]: whether the outputs are from testing dataset
    Returns:
        None
    """
    torch.save(features, output_path(output_path_maker, model_name, tag, observe_testing, 'features'))
    torch.save(preds, output_path(output_path_maker, model_name, tag, observe_testing, 'preds'))
    torch.save(labels, output_path(output_path_maker, model_name, tag, observe_testing, 'labels'))

def load_features(model_name, tag, observe_testing):
    """
    Load inference outputs.
    Args:
        model_name [str]: model name
        tag [str]: tag
        observe_testing [bool]: whether the outputs are from testing dataset
    Returns:
        
        [torch.Tensor], [torch.Tensor], [torch.Tensor]: loaded features, loaded predictions, loaded class labels
    """
    features = torch.load(output_path(output_path_maker, model_name, tag, observe_testing, 'features'))
    preds = torch.load(output_path(output_path_maker, model_name, tag, observe_testing, 'preds'))
    labels = torch.load(output_path(output_path_maker, model_name, tag, observe_testing, 'labels'))
    return features, preds, labels

def main():
    parser = argparse.ArgumentParser(description='Model Clustering Observation')

    add_cnn_config_args(parser)
    parser.add_argument('-a', '--attacks', type=str, default=None,
                        help='attack configs to use')
    parser.add_argument('-f', '--force-inference', default=False, action='store_true',
                        help='if still run inference when npy files exist')
    parser.add_argument('--split', type=str, default="test",
                        help='split of dataset to render')
    parser.add_argument('--grid-search', default=False, action='store_true',
                        help='if run grid search for threshold instead')
    parser.add_argument('--auto-search', default=False, action='store_true',
                        help='if run auto search for threshold instead')
    parser.add_argument('--threshold', default=None, type=float,
                        help='explicitly set threshold instead of searching')
    parser.add_argument('--run-bad', default=False, action='store_true',
                        help='if still run analysis even the threshold is not good')
    parser.add_argument('--adv', default=False, action='store_true',
                        help='if also observe adversarial examples')
    parser.add_argument('--metric', default='l2', type=str,
                        help='distance metric for clustering')
    parser.add_argument('--class-cluster', default=False, action='store_true',
                        help='whether to use classes directly as clusters')
    parser.add_argument('--conv-features', default=False, action='store_true',
                        help='whether to use convolutional features')
    parser.add_argument('-m', '--mat', default=False, action='store_true',
                        help='if using MAT model')
    parser.add_argument('--mm', default=False, action='store_true',
                        help='whether to use max-mahalanobis class centers')
    args = parser.parse_args()

    if args.attacks is not None:
        global ATTACKS
        ATTACKS = ATTACK_CONFIGS[args.attacks.upper()]

    observe_testing = args.split.lower() in ["test", "testing"]

    model_type = model_according_to(args.model, args.dataset)
    load_config_into(__import__(__name__), model_type, models, custom_losses, natural=args.natural)
    load_clustering_config_into(__import__(__name__), model_type, natural=args.natural)

    global NUM_CLASSES_
    NUM_CLASSES_ = NUM_CLASSES[dataset]

    model_name = model_path_by(dataset, args.natural, mat=args.mat)
    checkpoint, tag = load_checkpoint(model_name, tag=args.tag, return_tag=True)

    run_search = args.grid_search or args.auto_search

    run_inference = (args.force_inference) or ( \
                        (run_search and (not output_exists(output_path_maker, model_name, tag, observe_testing))) or \
                        ( \
                            not run_search and \
                            ( \
                                (not output_exists(output_path_maker, model_name, tag, True) or \
                                not output_exists(output_path_maker, model_name, tag, False)) \
                            ) \
                        ) \
                    )

    if run_inference:

        device, kwargs = auto_device()

        model = use_model()
        if args.mat:
            model = MAT(model, MM=args.mm)
        model = auto_parallel(model).to(device)
        model.load_state_dict(checkpoint)

        if run_search:
            features, preds, labels = infer_features(model, device, kwargs, observe_testing, download=args.download)
            save_features(features, preds, labels, model_name, tag, observe_testing)
            features = sigmoid(features)
        else:
            train_features, train_preds, train_labels = infer_features(model, device, kwargs, False, download=args.download)
            save_features(train_features, train_preds, train_labels, model_name, tag, False)
            train_features = sigmoid(train_features)
            test_features, test_preds, test_labels = infer_features(model, device, kwargs, True, download=args.download)
            save_features(test_features, test_preds, test_labels, model_name, tag, True)
            test_features = sigmoid(test_features)
    else:
        if run_search:
            features, preds, labels = load_features(model_name, tag, observe_testing)
            features = sigmoid(features)
        else:
            train_features, train_preds, train_labels = load_features(model_name, tag, False)
            train_features = sigmoid(train_features)
            test_features, test_preds, test_labels = load_features(model_name, tag, True)
            test_features = sigmoid(test_features)

    mlflow.set_tracking_uri(get_tracking_uri())

    if args.grid_search:
        mlflow.set_experiment('clustering')
        mlflow.start_run()

        mlflow.log_param('model_name', model_name)
        mlflow.log_param('tag', tag)

        h_bins = CountingBin([0.4, 0.7])
        s_bins = CountingBin([0.4, 0.7])
        
        threshold_range = range(*search_range)

        if args.threshold is not None:
            threshold_range = [args.threshold]

        npy_features = cast2numpy(features)
        npy_preds = cast2numpy(preds)
        clustering = init_clusters(npy_features)
        for i, distance_threshold in enumerate(threshold_range):
            clusters, mapping = get_clusters(clustering, npy_preds, distance_threshold)
            print(distance_threshold, clusters.n_clusters_)
            mlflow.log_metric('num_clusters', clusters.n_clusters_, step=i)
            mlflow.log_metric('thresholds', distance_threshold, step=i)
            good, rs = is_good(mapping, h_bins, s_bins, distance_threshold=distance_threshold, i=i)
            mlflow.log_metric('good', int(good), step=i)
            print("==============================")

        mlflow.end_run()

    elif args.auto_search:
        try:
            init_threshold, max_threshold, step = search_range
            model_path = joblib_path(skmodel_path_maker, model_name, tag, observe_testing)
            npy_features = cast2numpy(features)
            npy_preds = cast2numpy(preds)
            print(model_name, find_good_threshold(npy_features, npy_preds, init_threshold, step, max_threshold, model_path))
        except ValueError as e:
            print(str(e))
            print("Please run gird search instead.")

    elif args.conv_features:
        if not run_inference:
            device, kwargs = auto_device()

            model = use_model()
            model = auto_parallel(model).to(device)
            model.load_state_dict(checkpoint)

        model.eval()
        fmodel, attacks = create_attacks(model, requested_attacks=ATTACKS[dataset], mlflow_track=True)

        test_data_loader = torch.utils.data.DataLoader(
            get_dataset(dataset, train=False, transform=T.ToTensor(), download=args.download),
            batch_size=torch.cuda.device_count() * 16, shuffle=False, **kwargs
        )

        total = len(test_data_loader.dataset)

        with torch.no_grad():
            for b_idx, (data, target) in enumerate(test_data_loader):
                data, target = data.to(device), target.to(device)
                y = model(data)
                preds = y.max(1)[1]
                predict_correct = (preds == target)

                natural_features = channel_mean(model.intermediate_forward(data))
                natural_features = sigmoid(natural_features)

                if b_idx == 0:
                    Data = data
                    Labels = target
                    Features = natural_features
                    Logits = y
                    Preds = preds
                    AdvData = {}
                    AdvFeatures = {}
                    AdvLogits = {}
                    AdvPreds = {}
                else:
                    Data = torch.cat((Data, data), dim=0)
                    Labels = torch.cat((Labels, target), dim=0)
                    Features = torch.cat((Features, natural_features), dim=0)
                    Logits = torch.cat((Logits, y), dim=0)
                    Preds = torch.cat((Preds, preds), dim=0)

                for name, (success, clipped) in attacks.attack_all_safe(data, target):
                    success, clipped = success.to(device), clipped.to(device)
                    clipped[~success] = data[~success]
                    adv_logits = model(clipped) 
                    adv_preds = adv_logits.max(1)[1]
                    adv_features = channel_mean(model.intermediate_forward(clipped))
                    adv_features = sigmoid(adv_features)

                    if b_idx == 0:
                        AdvData[name] = clipped
                        AdvFeatures[name] = adv_features
                        AdvLogits[name] = adv_logits
                        AdvPreds[name] = adv_preds
                    else:
                        AdvData[name] = torch.cat((AdvData[name], clipped), dim=0)
                        AdvFeatures[name] = torch.cat((AdvFeatures[name], adv_features), dim=0)
                        AdvLogits[name] = torch.cat((AdvLogits[name], adv_logits), dim=0)
                        AdvPreds[name] = torch.cat((AdvPreds[name], adv_preds), dim=0)

                print("[{}/{}]".format(b_idx * target.shape[0], total), end="\r")

        path_maker = PathWDefaults('soft_reject', '.pt')
        for suffix, tensor_ in zip(['data', 'labels', 'logits', 'preds', 'features'], [Data, Labels, Logits, Preds, Features]):
            torch.save(tensor_, output_path(path_maker, model_name, tag, True, suffix + "_inter"))

        for name in attacks:
            prefix = "{}_adv_".format(name)
            for suffix, tensor_ in zip(['data', 'logits', 'preds', 'features'], [AdvData[name], AdvLogits[name], AdvPreds[name], AdvFeatures[name]]):
                torch.save(tensor_, output_path(path_maker, model_name, tag, True, prefix + suffix + "_inter"))

    else:
        mlflow.set_experiment('clustering_analysis')
        mlflow.start_run()

        mlflow.log_param('model_name', model_name)
        mlflow.log_param('tag', tag)

        use_threshold = best_threshold if args.threshold is None else args.threshold

        if not joblib_exists(skmodel_path_maker, model_name, tag, False):
            clusters, mapping = get_clusters(init_clusters(cast2numpy(train_features)), cast2numpy(train_preds), use_threshold)
            save_sk_model(clusters, joblib_path(skmodel_path_maker, model_name, tag, False))
        else:
            clusters = load_sk_model(joblib_path(skmodel_path_maker, model_name, tag, False))
            clusters, mapping = get_clusters(clusters, cast2numpy(train_preds), use_threshold)
        
        h_bins = CountingBin([0.4, 0.7])
        s_bins = CountingBin([0.4, 0.7])
        
        if args.run_bad or is_good(mapping, h_bins, s_bins)[0]:
            if args.metric == "CLWMd":
                clusters = PseudoClusters(cast2numpy(train_labels))
                mapping = clusters.get_mapping()
                cluster_predictor = TorchClusterPredictor(clusters, train_features, train_labels, mapping, dist=args.metric)
            else:
                cluster_predictor = TorchClusterPredictor(clusters, train_features, train_preds, mapping, dist=args.metric)
            mlflow.log_param('dist_metric', args.metric)

            global transform_test

            if args.adv:

                if not run_inference:
                    device, kwargs = auto_device()

                    model = use_model()
                    if args.mat:
                        model = MAT(model, MM=args.mm)
                    model = auto_parallel(model).to(device)
                    model.load_state_dict(checkpoint)

                if args.mat:
                    model = auto_parallel(model.for_attack_version()).to(device)
                model.eval()
                fmodel, attacks = create_attacks(model, requested_attacks=ATTACKS[dataset], mlflow_track=True)

                feature_model = auto_no_final_parallel(model).to(device)
                feature_model.eval()

                test_data_loader = torch.utils.data.DataLoader(
                    get_dataset(dataset, train=False, transform=transform_test, download=args.download),
                    batch_size=torch.cuda.device_count() * 16, shuffle=False, **kwargs
                )

                natural_matches = 0
                natural_total_distance = 0
                correct = 0
                rejection = 0
                natural_saved = 0
                adv_matcheses = {}
                adv_successes = {}
                natural_kds = []
                adv_kds = {}
                robust_saved = {}
                robust_acc = {}
                total = len(test_data_loader.dataset)

                with torch.no_grad():
                    for b_idx, (data, target) in enumerate(test_data_loader):
                        data = data.to(device)
                        y = model(data).cpu()
                        preds = y.max(1)[1]
                        predict_correct = (preds == target)

                        natural_features = feature_model(data).cpu()
                        natural_features = sigmoid(natural_features)
                        natural_match_classes, natural_min_ds = cluster_predictor.predict_and_match(natural_features)
                        natural_kds += natural_min_ds.tolist()

                        correct += predict_correct.sum().item()
                        natural_matches += (natural_match_classes == preds).sum().item()
                        natural_total_distance += natural_min_ds.sum().item()
                        rejection += (natural_match_classes == -1).sum().item()
                        natural_saved += (natural_match_classes == target).sum().item()

                        data = data.cpu()

                        if b_idx == 0:
                            Data = data
                            Labels = target
                            Features = natural_features
                            Logits = y
                            Preds = preds
                            AdvData = {}
                            AdvFeatures = {}
                            AdvLogits = {}
                            AdvPreds = {}
                        else:
                            Data = torch.cat((Data, data), dim=0)
                            Labels = torch.cat((Labels, target), dim=0)
                            Features = torch.cat((Features, natural_features), dim=0)
                            Logits = torch.cat((Logits, y), dim=0)
                            Preds = torch.cat((Preds, preds), dim=0)

                        for name, (success, clipped) in attacks.attack_all_safe(data, target):
                            success, clipped = success.to(device), clipped.to(device)
                            clipped[~success] = data.to(device)[~success]
                            adv_y = model(clipped).cpu()
                            adv_preds = adv_y.max(1)[1]
                            adv_features = feature_model(clipped).cpu()
                            adv_features = sigmoid(adv_features)
                            adv_match_classes, adv_min_ds = cluster_predictor.predict_and_match(adv_features)
                            success = success.cpu()
                            success = success & predict_correct
                            clipped = clipped.cpu()

                            if name not in robust_acc:
                                adv_matcheses[name] = 0
                                adv_successes[name] = 0
                                robust_acc[name] = 0
                                robust_saved[name] = 0
                                adv_kds[name] = []

                            adv_successes[name] += success.sum().item()
                            if success.sum() != 0:
                                adv_matcheses[name] += ((adv_match_classes == adv_preds)[success]).sum().item()
                            robust_acc[name] += (adv_preds == target).sum().item()
                            robust_saved[name] += (adv_match_classes == target).sum().item()
                            adv_kds[name] += adv_min_ds[success].tolist()

                            if b_idx == 0:
                                AdvData[name] = clipped
                                AdvFeatures[name] = adv_features
                                AdvLogits[name] = adv_y
                                AdvPreds[name] = adv_preds
                            else:
                                AdvData[name] = torch.cat((AdvData[name], clipped), dim=0)
                                AdvFeatures[name] = torch.cat((AdvFeatures[name], adv_features), dim=0)
                                AdvLogits[name] = torch.cat((AdvLogits[name], adv_y), dim=0)
                                AdvPreds[name] = torch.cat((AdvPreds[name], adv_preds), dim=0)

                        print("[{}/{}]".format(b_idx * target.shape[0], total), end="\r")

                path_maker = PathWDefaults('soft_reject', '.pt')
                for suffix, tensor_ in zip(['data', 'labels', 'logits', 'preds', 'features'], [Data, Labels, Logits, Preds, Features]):
                    torch.save(tensor_, output_path(path_maker, model_name, tag, True, suffix))

                for name in attacks:
                    prefix = "{}_adv_".format(name)
                    for suffix, tensor_ in zip(['data', 'logits', 'preds', 'features'], [AdvData[name], AdvLogits[name], AdvPreds[name], AdvFeatures[name]]):
                        torch.save(tensor_, output_path(path_maker, model_name, tag, True, prefix + suffix))

                mlflow.log_metric('natural_perc_match', natural_matches / total)
                mlflow.log_metric('natural_mean_d', natural_total_distance / total)
                mlflow.log_metric('natural_acc', correct / total)
                mlflow.log_metric('natural_rej', rejection / total)
                mlflow.log_metric('natural_saved', natural_saved / total)

                n_trues = np.zeros_like(natural_kds)

                for name in attacks:
                    success = adv_successes[name]
                    mlflow.log_metric('adv_perc_match_{}'.format(name), adv_matcheses[name] / success)
                    mlflow.log_metric('robust_acc_{}'.format(name), robust_acc[name] / total)
                    mlflow.log_metric('robust_saved_{}'.format(name), robust_saved[name] / total)
                    a_trues = np.ones_like(adv_kds[name])
                    y_true = np.concatenate([n_trues, a_trues], axis=0)
                    y_score = np.concatenate([natural_kds, adv_kds[name]], axis=0)
                    mlflow.log_metric('{}_AUC'.format(name), roc_auc_score(y_true, y_score))

                    fpr, tpr, thresholds = roc_curve(y_true, y_score)
                    for i, fprr in enumerate(fpr):
                        if fprr > 0.001 and i > 0:
                            mlflow.log_metric('threshold', thresholds[i-1])

            else:
                seq = Cascade() \
                    .append("unsqueeze", 0) \
                    .append("__sub__", cluster_predictor.cluster_centers) \
                    .append(t_l2_norm, dim=1) \
                    .append("min", 0) \
                    .append('__getitem__', 1) \
                    .append(cluster_predictor.dom_classes.__getitem__)
                # Equivalent to:
                #     distances = t_l2_norm((test_feature.unsqueeze(0) - cluster_predictor.cluster_centers), dim=1)
                #     min_d_cluster = distances.min(0)[1]
                #     cluster_predictor.dom_classes[min_d_cluster]

                min_d_dom_classes = [seq(test_feature) for test_feature in test_features]
                perc_match = (min_d_dom_classes == test_preds).mean().item()
                print("Percent of matches: {:.5f}".format(perc_match))
                mlflow.log_metric('perc_match', perc_match)

        else:
            print("this threshold is not good")

        mlflow.end_run()

if __name__ == "__main__":
    main()
