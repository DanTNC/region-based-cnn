import argparse
import os

import joblib
import matplotlib.pyplot as plt
import mlflow
import numpy as np
import torch
import torchvision.transforms as T
from scipy.special import softmax
from sklearn.manifold import TSNE
from sklearn.metrics import roc_auc_score, roc_curve

import models
import utils.loss as custom_losses
from clustering import (create_mapping, get_clusters, init_clusters,
                        load_features, load_sk_model, save_features,
                        save_sk_model, sigmoid, skmodel_path_maker)
from configs.arg_helper import (add_cnn_config_args, model_according_to,
                                model_path_by)
from configs.clustering_config import \
    load_config_into as load_clustering_config_into
from configs.cnn_config import MODEL_DATASET_MAPPING
from configs.constant import NUM_CLASSES
from data.dataset import get_dataset
from utils.cluster_helper import (DIST_MAPPING, ClusterPredictor, KNNPredictor,
                                  PseudoClusters, TorchClusterPredictor)
from utils.data_helper import cast2numpy, cast2tensor, l2_norm
from utils.mlflow_helper import get_tracking_uri
from utils.model_manager import load_checkpoint
from utils.path_helper import (PathWDefaults, joblib_exists, joblib_path,
                               output_exists, output_path)
from utils.torch_helper import (auto_device, auto_no_final_parallel,
                                auto_parallel)
from utils.training_helper import score

path_maker = PathWDefaults("soft_reject", ".pt")
embed_path_maker = PathWDefaults("soft_reject", ".npy")

NUM_CLASSES_ = 10
DIST_MAPPING_T = DIST_MAPPING['torch']

def input_path(model_name, tag, suffix):
    """
    Construct path of input information.
    Args:
        model_name [str]: model name
        tag [str]: tag
        suffix [str]: suffix of the file name
    Returns:
        [str]: path
    """
    return output_path(path_maker, model_name, tag, True, suffix)

def load_naturals(model_name, tag, conv=False):
    """
    Load information of natural data.
    Args:
        model_name [str]: model name
        tag [str]: tag
        conv [bool]: whether to use features of the last convolutional layer
    Returns:
        [numpy.array], [numpy.array], [numpy.array], [numpy.array]: images, features, class labels, predictions
    """
    ext = "_inter" if conv else ""
    data = cast2numpy(torch.load(input_path(model_name, tag, "data" + ext)))
    features = cast2numpy(torch.load(input_path(model_name, tag, "features" + ext)))
    labels = cast2numpy(torch.load(input_path(model_name, tag, "labels" + ext)))
    preds = cast2numpy(torch.load(input_path(model_name, tag, "preds" + ext)))
    return data, features, labels, preds

def load_advs(model_name, tag, name, conv=False):
    """
    Load information of adversarial data.
    Args:
        model_name [str]: model name
        tag [str]: tag
        conv [bool]: whether to use features of the last convolutional layer
    Returns:
        [numpy.array], [numpy.array], [numpy.array]: images, features, predictions
    """
    ext = "_inter" if conv else ""
    adv_data = cast2numpy(torch.load(input_path(model_name, tag, "{}_adv_data".format(name) + ext)))
    adv_features = cast2numpy(torch.load(input_path(model_name, tag, "{}_adv_features".format(name) + ext)))
    adv_preds = cast2numpy(torch.load(input_path(model_name, tag, "{}_adv_preds".format(name) + ext)))
    return adv_data, adv_features, adv_preds

def embeddings_exists(model_name, tag, name, conv=False):
    """
    Check if all embedding files exist.
    Args:
        model_name [str]: model name
        tag [str]: tag
        name [str]: attack name
        conv [bool]: whether to use features of the last convolutional layer
    Returns:
        [bool]: whether they all exist
    """
    if conv:
        return output_exists(embed_path_maker, model_name, tag, True, args_list=[["{}_embeddings".format(name), "inter"], ["{}_n_len".format(name), "inter"]])
    else:
        return output_exists(embed_path_maker, model_name, tag, True, args_list=[["{}_embeddings".format(name)], ["{}_n_len".format(name)]])

def save_embeddings(model_name, tag, name, embeddings, n_len, conv=False):
    """
    Save embeddings as files.
    Args:
        model_name [str]: model name
        tag [str]: tag
        name [str]: attack name
        embeddings [numpy.array]: embeddings
        n_len [int]: number of natural data instances
        conv [bool]: whether to use features of the last convolutional layer
    Returns:
        None
    """
    if conv:
        args = ["inter"]
    else:
        args = []
    np.save(output_path(embed_path_maker, model_name, tag, True, "{}_embeddings".format(name), *args), embeddings)
    np.save(output_path(embed_path_maker, model_name, tag, True, "{}_n_len".format(name), *args), n_len)

def load_embeddings(model_name, tag, name, conv=False):
    """
    Load embeddings from files.
    Args:
        model_name [str]: model name
        tag [str]: tag
        name [str]: attack name
        conv [bool]: whether to use features of the last convolutional layer
    Returns:
        [numpy.array], [int]: embeddings, number of natural data instances
    """
    if conv:
        args = ["inter"]
    else:
        args = []
    embeddings = np.load(output_path(embed_path_maker, model_name, tag, True, "{}_embeddings".format(name), *args))
    n_len = np.load(output_path(embed_path_maker, model_name, tag, True, "{}_n_len".format(name), *args))
    return embeddings, n_len

def split_embedding(embed, n_len, with_class_mean=True):
    """
    Split embedding array into natural part, adversarial part (, and class centers).
    Args:
        embed [numpy.array]: all embeddings
        n_len [int]: number of natural data instances
        with_class_mean [bool]: whether the array contains class centers
    Returns:
        If with_class_mean is True:
            [numpy.array], [numpy.array], [numpy.array]: natural embeddings, adversarial embeddings, class centers
        Else:
            [numpy.array], [numpy.array]: natural embeddings, adversarial embeddings
    """
    n_len = int(n_len)
    n_tol = embed.shape[0]
    if with_class_mean:
        return embed[:n_len], embed[n_len:n_tol-NUM_CLASSES_], embed[n_tol-NUM_CLASSES_:]
    else:
        return embed[:n_len], embed[n_len:]

def plot2d(n_e, a_e, n_lbl, lbl2, view, prefix, postfix, ext):
    """
    Plot TSNE embeddings in a 2D map.
    Args:
        n_e [numpy.array]: natural embeddings
        a_e [numpy.array]: adversarial embeddings
        n_lbl [numpy.array]: natural labels
        lbl2 [numpy.array]: labels for adversarial embeddings (can be real labels or model predictions)
        view [str]: from which view to color the adversarial embeddings ("human" or "model")
        Note: if view is "human", lbl2 should be real labels, otherwise it should be model predictions.
        prefix [str]: prefix of the plot figure name
        postfix [str]: postfix of the plot figure name
        ext [str]: additional information to be put in the plot figure name
    Returns:
        None
    """
    if not os.path.exists(view):
        os.makedirs(view, exist_ok=True)
    plt.clf()
    cmap = "tab10"
    scatter = plt.scatter(n_e[:, 0], n_e[:, 1], s=2, c=n_lbl.astype(float), cmap=cmap)
    plt.legend(*scatter.legend_elements())
    plt.scatter(a_e[:, 0], a_e[:, 1], s=50, c=lbl2.astype(float), cmap=cmap, marker="^", linewidths=0.5, edgecolors="k")
    plt.savefig("{}/{}-{}{}.png".format(view, prefix, postfix, ext))

def plot2d_natural(n_e, n_lbl, view, prefix, postfix, ext):
    """
    Plot only natural TSNE embeddings in a 2D map.
    Args:
        n_e [numpy.array]: natural embeddings
        n_lbl [numpy.array]: natural labels
        view [str]: from which view to color the adversarial embeddings ("human" or "model")
        Note: this argument does not affect the plot result but the directory to save the figure file.
        prefix [str]: prefix of the plot figure name
        postfix [str]: postfix of the plot figure name
        ext [str]: additional information to be put in the plot figure name
    Returns:
        None
    """
    if not os.path.exists(view):
        os.makedirs(view, exist_ok=True)
    plt.clf()
    cmap = "tab10"
    scatter = plt.scatter(n_e[:, 0], n_e[:, 1], s=2, c=n_lbl.astype(float), cmap=cmap)
    plt.legend(*scatter.legend_elements())
    plt.savefig("{}/{}-{}{}.png".format(view, prefix, postfix, ext + '_all_n'))

def plot2d_wrong(n_e, n_lbl, view, prefix, postfix, ext, preds):
    """
    Plot only natural TSNE embeddings in a 2D map and color misclassified samples in a different color.
    Args:
        n_e [numpy.array]: natural embeddings
        n_lbl [numpy.array]: natural labels
        view [str]: from which view to color the adversarial embeddings ("human" or "model")
        Note: this argument does not affect the plot result but the directory to save the figure file.
        prefix [str]: prefix of the plot figure name
        postfix [str]: postfix of the plot figure name
        ext [str]: additional information to be put in the plot figure name
        preds [numpy.array]: model predictions
    Returns:
        None
    """
    if not os.path.exists(view):
        os.makedirs(view, exist_ok=True)
    plt.clf()
    cmap = "tab10"
    correct = n_lbl == preds
    c_e = n_e[correct]
    c_lbl = n_lbl[correct]
    w_e = n_e[~correct]
    w_lbl = n_lbl[~correct]
    scatter = plt.scatter(c_e[:, 0], c_e[:, 1], s=2, c=c_lbl.astype(float), cmap=cmap)
    plt.legend(*scatter.legend_elements())
    scatter = plt.scatter(w_e[:, 0], w_e[:, 1], s=1.5, c=w_lbl.astype(float), cmap=cmap, linewidths=0.5, edgecolors="k")
    plt.savefig("{}/{}-{}{}.png".format(view, prefix, postfix, ext + '_wrong'))

def main():
    parser = argparse.ArgumentParser(description='Model Soft Rejection Experiments')
    add_cnn_config_args(parser)
    parser.add_argument('--tsne', default=False, action='store_true',
                        help='if run TSNE analysis')
    parser.add_argument('--metric', default='l2', type=str,
                        help='distance metric for clustering')
    parser.add_argument('--class-cluster', default=False, action='store_true',
                        help='whether to use classes directly as clusters')
    parser.add_argument('--variant', default=None, type=str,
                        help='which variant to use for TSNE plotting')
    parser.add_argument('--no-remove', default=False, action='store_true',
                        help='do not remove outliers while computing cluster characteristics')
    parser.add_argument('--magnitude', default=0.01, type=float,
                        help='magnitude of calibration noise')
    parser.add_argument('--conv-features', default=False, action='store_true',
                        help='if use convolutional features instead')
    parser.add_argument('-f', '--force-inference', default=False, action='store_true',
                        help='if force running embeddings')
    parser.add_argument('-a', '--attacks', default='LinfBIM', type=str,
                        help='comma separated attack names to be considered')
    args = parser.parse_args()

    model_type = model_according_to(args.model, args.dataset)
    dataset = args.dataset if args.dataset is not None else MODEL_DATASET_MAPPING[model_type]
    model_name = model_path_by(dataset, args.natural, mat=False)

    load_clustering_config_into(__import__(__name__), model_type, natural=args.natural)
    global NUM_CLASSES_
    NUM_CLASSES_ = NUM_CLASSES[dataset]

    if args.tsne:
        data, features, labels, preds = load_naturals(model_name, args.tag, conv=args.conv_features)
        name = 'LinfBIM'
        adv_data, adv_features, adv_preds = load_advs(model_name, args.tag, name, conv=args.conv_features)

        condition = (preds == labels) & (adv_preds != labels)
        adv_features = adv_features[condition]
        a_labels = labels[condition]
        m_labels = adv_preds[condition]
        adv_preds = adv_preds[condition]

        n_len = np.array(features.shape[0])

        total_features = np.concatenate((features, adv_features), axis=0)

        if args.force_inference or not embeddings_exists(model_name, args.tag, name, conv=args.conv_features):
            embeddings = TSNE(n_components=2).fit_transform(total_features)
            save_embeddings(model_name, args.tag, name, embeddings, n_len, conv=args.conv_features)
        else:
            embeddings, n_len = load_embeddings(model_name, args.tag, name, conv=args.conv_features)

        n_e, a_e = split_embedding(embeddings, n_len, with_class_mean=False)

        prefix = "{}-{}".format(model_name, args.tag)

        ext = 'cf' if args.conv_features else ''

        if args.variant is None:
            for lbl2, postfix in ((a_labels, 'a_labels'), (m_labels, 'm_labels')):
                plot2d(n_e, a_e, labels, lbl2, 'human', prefix, postfix, ext)
                plot2d(n_e, a_e, preds, lbl2, 'model', prefix, postfix, ext)
        elif args.variant == 'all_n':
            plot2d_natural(n_e, labels, 'human', prefix, '', ext)
            plot2d_natural(n_e, preds, 'model', prefix, '', ext)
        elif args.variant == 'wrong':
            plot2d_wrong(n_e, labels, 'human', prefix, '', ext, preds)

    else:
        train_features, train_preds, train_labels = load_features(model_name, args.tag, False)
        train_features = sigmoid(train_features)
        train_features, train_preds, train_labels = cast2numpy(train_features, train_preds, train_labels)

        mlflow.set_tracking_uri(get_tracking_uri())

        mlflow.set_experiment('soft_rejection_analysis')
        mlflow.start_run()

        mlflow.log_param('model_name', model_name)
        mlflow.log_param('tag', args.tag)

        clusters = load_sk_model(joblib_path(skmodel_path_maker, model_name, args.tag, False))
        clusters, mapping = get_clusters(clusters, train_preds, best_threshold)

        if args.metric == 'CLWMd':
            clusters = PseudoClusters(train_labels)
            mapping = clusters.get_mapping()
            cluster_predictor = ClusterPredictor(clusters, train_features, train_labels, mapping, dist=args.metric, remove_outlier=not args.no_remove)
        elif args.metric == 'KNN':
            cluster_predictor = KNNPredictor(clusters, train_features, train_labels, mapping, dist=args.metric, remove_outlier=not args.no_remove)
        else:
            cluster_predictor = ClusterPredictor(clusters, train_features, train_preds, mapping, dist=args.metric, remove_outlier=not args.no_remove)
        mlflow.log_param('dist_metric', args.metric)

        attacks = args.attacks.split(",")

        data, features, labels, preds = load_naturals(model_name, args.tag)
        natural_correct = preds == labels
        mlflow.log_metric("natural_acc", natural_correct.mean())
        natural_mcs, natural_ds = cluster_predictor.predict_and_match(features)
        mlflow.log_metric("natural_saved", (natural_mcs == labels).mean())
        mlflow.log_metric("natural_match_rate", (natural_mcs == preds).mean())
        natural_trues = np.zeros_like(natural_ds)
        for name in attacks:
            adv_data, adv_features, adv_preds = load_advs(model_name, args.tag, name)
            success = natural_correct & (adv_preds != labels)
            mlflow.log_metric(name + " robust_acc", (adv_preds == labels).mean())
            adv_mcs, adv_ds = cluster_predictor.predict_and_match(adv_features)
            mlflow.log_metric(name + " robust_saved", (adv_mcs == labels).mean())
            mlflow.log_metric(name + " robust_match_rate", (adv_mcs == adv_preds).mean())
            pure_adv_ds = adv_ds[success]
            adv_trues = np.ones_like(pure_adv_ds)
            y_trues = np.concatenate((natural_trues, adv_trues), axis=0)
            y_scores = np.concatenate((natural_ds, pure_adv_ds), axis=0)
            fpr, tpr, thresholds = roc_curve(y_trues, y_scores)
            mlflow.log_metric(name + " AUC", roc_auc_score(y_trues, y_scores))

            overall_preds = np.concatenate((preds, adv_preds[success]), axis=0)
            overall_mcs = np.concatenate((natural_mcs, adv_mcs[success]), axis=0)
            overall_labels = np.concatenate((labels, labels[success]), axis=0)

            mlflow.log_metric(name + " overall_softmax_acc", (overall_preds == overall_labels).mean())
            mlflow.log_metric(name + " overall_map_acc", (overall_mcs == overall_labels).mean())

        mlflow.end_run()

if __name__ == "__main__":
    main()
