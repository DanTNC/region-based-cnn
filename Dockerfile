FROM nvcr.io/nvidia/pytorch:19.11-py3
COPY . .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
CMD python ManifoldAwareTraining.py