# Analysis on adversarial attacks and nature of CNN  
**And proposal of Manifold-Aware Training (MAT) for robustness**

## Setup
### Environment
```
git clone <url-to-repo>
cd region-based-cnn
pip install -r requirements.txt
```

### TWCC S3 connection (for model weights)
Create a json file in root directory and name it ```secret.json```.  
Put the following content in it:  
```json
{
    "ACCESS_KEY": <ACCESS_KEY>,
    "SECRET_KEY": <SECRET_KEY>
}
```
Find your ```ACCESS_KEY``` & ```SECRET_KEY```:  
https://www.twcc.ai/user/bucket/tool

### mlflow path and other storage path settings (for experiments)
Create a json file in root directory and name it ```path_setting.json```.  
Put the following content in it:  
```json
{
    "STORAGE_ROOT": <PATH_TO_STORE_TRAINED_WEIGHTS_OR_OTHER_LARGE_FILES>,
    "MLFLOW_TRACKING_URI": <PATH_TO_STORE_EXPERIMENT_LOGS>
}
```
For ```MLFLOW_TRACKING_URI```:  
Start with ```"file://"``` if using a local path.  
e.g. ```"file:///work/fad11204/research/mlruns"```


To see the experiment results logged by mlflow:  
```
cd <PATH_TO_STORE_EXPERIMENT_LOGS_WITHOUT_'/mlruns'>
# e.g. cd /work/fad11204/research
mlflow ui -h 0.0.0.0
# If using TWCC, you need to '關聯 服務埠 5000' on the container web page first
# After that, on the left side ('協定埠') you will see something like:
# 目標埠: 5000 (userdefine1)
# 對外埠: <SOME_PORT>

# In your browser, navigate to 'https://<PUBLIC_IP>:<SOME_PORT>'
# Then you will see the mlflow ui web page
```

## Introduction of codebase
### Directories
| Name | Description |
| -------- | -------- |
| configs     | helpers for configurations of command-line arguments and model settings ([details](configs/README.md))   |
| data     | helpers for dataset loading (Dataset and DataLoader) ([details](data/README.md))   |
| models     | architectures or wrappers of Pytorch models ([details](models/README.md))   |
| utils     | commonly-used functions across all python scripts ([details](utils/README.md))   |

### Scripts
**NOTE**: use the following command to show script options.
```python
    python <SCRIPTNAME> -h
```

* **curve.py**  
training script for plotting robust accuracy vs. perturbation strength curve  

* **train_CNN.py**  
training script for CNN models  

* **train_MAT.py**  
training script for CNN models using MAT  

* **clustering.py**  
script for conducting clustering of features  

* **eval_robustness.py**  
script for observing natural and robust accuracy of CNNs  

* **model_versions.py**  
script for showing model versions on S3  

* **upload.py**  
script for saving model weights to S3  

* **soft_rejection.py**  
script for running MAP or plotting TSNE visualization of traditional CNNs  