# for adapter
#from abstractalgorithmserver.abstractalgorithmserver import AbstractAlgorithmServer
import os

import mlflow

from models.MAT import MAT
from utils.torch_helper import auto_parallel
from utils.training_helper import training_loop
from utils.criteria import Scheduler, make_criteria
from utils.loss import bibo_loss

#class AlgorithmName(AbstractAlgorithmServer):
class ManifoldAwareTraining():
    REQUIRED_METHODS = [
        "out_layer",
        "no_final_version"
    ]
    def __init__(self):
        super().__init__()
        # initialize parameter=
        
    def init_parameters(self, model, data_loader, optimizer,
                optimizer_kwargs, early_stop_kwargs, scheduler_kwargs,
                device, print_interval=10, test_data_loader=None,
                model_directory="./", mlflow_track=False, mlflow_exp_name=None,
                before_iter=None, after_iter=None, after_epoch=None,
                MMD=True, SO=True, BIBO=True, bibo_kwargs=None):
        """
        Args:
            model [torch.nn.Module]: model to be trained
            data_loader [torch.utils.data.DataLoader]: data loader of training dataset
            optimizer [type]: optimizer class
            optimizer_kwargs [dict]: optimizer configurations
            early_stop_kwargs [dict]: stopping criteria configurations
            scheduler_kwargs [dict]: scheduler configurations
            device [torch.device]: device of model
            print_interval [int]: interval of epochs for printing training status
            test_data_loader [torch.utils.data.DataLoader]: data loader of testing dataset
            model_directory [str]: model directory to save weights
            mlflow_track [bool]: whether to use mlflow to track training status
            mlflow_exp_name [str]: name of the experiment in mlflow
            before_iter [Callable]: function that is called before each iteration
            after_iter [Callable]: function that is called after each iteration
            after_epoch [Callable]: function that is called after each epoch
            MMD [bool]: whether to use MMD centers
            SO [bool]: whether to use SO loss
            BIBO [bool]: whether to use BIBO loss
            bibo_kwargs [dict]: parameters for the BIBO loss
        """
        self.progress = 0
        self.model = model
        self.train_data_loader = data_loader
        self.optimizer = optimizer
        self.optimizer_kwargs = optimizer_kwargs
        self.early_stop_kwargs = early_stop_kwargs
        self.scheduler_kwargs = scheduler_kwargs
        self.device = device
        self.print_interval = print_interval
        self.test_data_loader = test_data_loader
        self.model_directory = model_directory
        self.mlflow_track = mlflow_track
        self.mlflow_experiment_name = mlflow_exp_name
        self.before_iteration = before_iter
        self.after_iteration = after_iter
        self.after_epoch = after_epoch
        self.MMD = MMD
        self.SO = SO
        self.BIBO = BIBO
        self.bibo_kwargs = bibo_kwargs
        if MMD:
            self.REQUIRED_METHODS[1] = "no_final_block_version"
        if not os.path.exists(model_directory):
            os.makedirs(model_directory)
        
    def progress(self):
        # return algorithm progress (0-100)
        return self.progress

    def check_model_methods(self):
        missing_methods = [required_method for required_method in self.REQUIRED_METHODS if not hasattr(self.model, required_method)]
        if len(missing_methods) != 0:
            raise ValueError("Models using ManifoldAwareTraining need to have the following methods: {}. {} is/are missing.".format(", ".join(self.REQUIRED_METHODS), ", ".join(missing_methods)))

    def execute(self):
        # algorithm executing here
        self.check_model_methods()
        model = MAT(self.model, MM=self.MMD)
        model = auto_parallel(model, verbose=False).to(self.device)

        optimizer = self.optimizer(model.parameters(), **self.optimizer_kwargs)

        if self.scheduler_kwargs:
            if "trigger_epochs" in self.early_stop_kwargs:
                lr_decay_max_times = len(self.early_stop_kwargs["trigger_epochs"])
            else:
                lr_decay_max_times = self.scheduler_kwargs["lr_decay_max_times"]

            scheduler = Scheduler(self.optimizer_kwargs["lr"], optimizer, 
                                self.scheduler_kwargs["lr_decay_rate"],
                                lr_decay_max_times, mlflow=mlflow)

            self.early_stop_kwargs["scheduler"] = scheduler

        early_stop = make_criteria(**self.early_stop_kwargs)

        print(early_stop.profile())

        variant = "so" if self.SO else "cosine"

        if self.BIBO:
            Loss = lambda y, y_star, x: bibo_loss(model=model,
                            x_natural=x,
                            y=y_star,
                            optimizer=optimizer,
                            loss_func=lambda y, y_star: model.get_loss(y, y_star, self.device, variant=variant),
                            **self.bibo_kwargs)
        else:
            Loss = lambda y, y_star, x: model.get_loss(y, y_star, self.device, variant=variant)

        e_loss = lambda y, y_star, x: model.get_loss(y, y_star, self.device, variant=variant)

        save_func = lambda model, epoch: (
                    torch.save(model.state_dict(), '{}/model-epoch{:03d}.pt'.format(self.model_directory, epoch)),
                    mlflow.log_artifact('{}/model-epoch{:03d}.pt'.format(self.model_directory, epoch)) if self.mlflow_track else None,
                    print("save model weights")
                )

        pred_func = lambda y: model.module.predict_by_features(y, self.device)

        if self.mlflow_track:
            if self.mlflow_experiment_name is not None:
                mlflow.set_experiment(self.mlflow_experiment_name)
            mlflow.start_run()

            mlflow.log_param('opt', str(optimizer).replace("\n", "&"))
            mlflow.log_param('learning rate', optimizer.param_groups[0]['lr'])
            mlflow.log_param('epsilon', self.bibo_kwargs['epsilon'])
            mlflow.log_param('perturb_steps', self.bibo_kwargs['perturb_steps'])
            mlflow.log_param('step_size', self.bibo_kwargs['step_size'])
            mlflow.log_param('beta', self.bibo_kwargs['beta'])
            mlflow.log_param('model directory', self.model_directory)
            mlflow.log_param('early_stop', str(early_stop.profile()))
            mlflow.log_param('MMD', str(self.MMD))
            mlflow.log_param('SO', str(self.SO))
            mlflow.log_param('BIBO', str(self.BIBO))

        def after_epoch(model, device, epoch):
            self.progress = (epoch / early_stop.profile()['max_epochs']) * 100
            print(self.progress)
            self.after_epoch

        model, epoch = training_loop(model, self.train_data_loader, Loss, optimizer, early_stop, self.device,
                print_interval=self.print_interval,
                test_data_loader=self.test_data_loader, save_func=save_func, mlflow_track=self.mlflow_track,
                before_iter=self.before_iteration, after_iter=self.after_iteration, after_epoch=after_epoch,
                pred_func=pred_func, e_loss=e_loss)

        if self.mlflow_track:
            mlflow.end_run()

        print("Final saved epoch: {}".format(early_stop.final_saved_epoch))
    
#Algorithm_runner = ImgRegWSI()
#Algorithm_runner.start_server()

if __name__ == "__main__":
    import torch

    from models.wideresnet import WideResNet
    from data.dataset import get_data_loaders
    from utils.torch_helper import auto_device

    model = WideResNet()
    device, kwargs = auto_device()
    train_loader, test_loader = get_data_loaders(128, "cifar", kwargs, download=True)
    optimizer = torch.optim.SGD

    class ExamplePrintingChecker:
        def __init__(self):
            self.before_iter_printed = False
            self.after_iter_printed = False
            self.after_epoch_printed = False

        def print_before_iter(self):
            if not self.before_iter_printed:
                print("before_iter function executed here")
                self.before_iter_printed = True
            
        def print_after_iter(self):
            if not self.after_iter_printed:
                print("after_iter function executed here")
                self.after_iter_printed = True

        def print_after_epoch(self):
            if not self.after_epoch_printed:
                print("after_epoch function executed here")
                self.after_epoch_printed = True
    
    epc = ExamplePrintingChecker()

    ManifoldAwareTraining_runner = ManifoldAwareTraining()
    ManifoldAwareTraining_runner.init_parameters(model, train_loader, optimizer, # optimizer is a class, not an object instance.
                                {'lr': 0.01, 'momentum': 0.9, 'weight_decay': 2e-4}, # optimizer keyword arguments. Please see PyTorch documentation for details.
                                {'strategy': 'base', 'MAX_EPOCHS': 3, 'log_interval': 1, 'trigger_epochs': [1, 2]},
                                # strategy='best': 'base' or 'best'.
                                #   'base' saves weights periodically and stops when MAX_EPOCHS is reached.
                                #   'best' saves weights when loss is reduced and stops when the loss doesn't become lower for CONVERGE_EPOCHS epochs or when MAX_EPOCHS is reached.
                                # MAX_EPOCHS=500: maximum number of epochs to train.
                                # CONVERGE_EPOCHS=5: number of epochs of steady loss defined as converge.
                                # log_interval=5: period of weight saving (in epochs).
                                # trigger_epochs=None: predefined n-th epochs for learning rate adjustment.
                                {'lr_decay_rate': 0.1, 'lr_decay_max_times': 2},
                                # lr_decay_rate: learning rate adjustment factor.
                                # lr_decay_max_times: maximum number of times of adjustment.
                                # Note: If scheduler_kwargs is not None. the above two parameters are required.
                                device, test_data_loader=test_loader, model_directory="test/", mlflow_track=True,
                                mlflow_exp_name="ALOVAS", before_iter=lambda model, data, target, device, epoch: epc.print_before_iter(),
                                after_iter=lambda model, data, target, device, epoch: epc.print_after_iter(),
                                after_epoch=lambda model, device, epoch: epc.print_after_epoch(),
                                bibo_kwargs={'epsilon': 0.031, 'perturb_steps': 10, 'step_size': 0.007, 'beta': 3.0},
                                # epsilon=0.031: perturbation strength.
                                # perturb_steps=10: number of steps for PGD optimization.
                                # step_size=0.003: step size of PGD optimization.
                                # beta=1.0: weight of the second term in the TRADES loss.
                                BIBO=False, print_interval=100) # set BIBO as False to speed up example.
    ManifoldAwareTraining_runner.execute()
