import unittest

import configs.test as config_test
import utils.test as utils_test


def get_suite():
    test_loader = unittest.defaultTestLoader
    suite = unittest.TestSuite()
    suite.addTests(*test_loader.loadTestsFromModule(config_test))
    suite.addTests(*test_loader.loadTestsFromModule(utils_test))
    return suite

if __name__ == "__main__":
    runner = unittest.TextTestRunner()
    runner.run(get_suite())
