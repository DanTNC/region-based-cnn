import argparse
from datetime import datetime

from configs.arg_helper import (add_cnn_config_args, model_according_to,
                                model_path_by)
from configs.cnn_config import MODEL_DATASET_MAPPING
from utils.model_manager import get_version_controller

parser = argparse.ArgumentParser(description='Model Versions Viewer')
add_cnn_config_args(parser)
parser.add_argument('-m', '--mat', default=False, action='store_true',
                    help='if it is MAT model')

args = parser.parse_args()

version_controller = get_version_controller()
model_type = model_according_to(args.model, args.dataset)
dataset = MODEL_DATASET_MAPPING[model_type]
model_name = model_path_by(dataset, args.natural, mat=args.mat)

versions = version_controller[model_name]
for time, tag in versions.items():
    print('{}:\t{}'.format(datetime.fromtimestamp(float(time)), tag))
