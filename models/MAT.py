import copy
from collections import OrderedDict
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from utils.training_helper import to_1_hot
from utils.data_helper import cast2numpy, cast2tensor, t_s_l2_norm, t_l2_norm

def gen_class_centers_orthogonal(norm, num_features, num_labels):
    """
    Generate orthogonal class centers.
    Args:
        norm [float/int]: l2 norm of the class centers
        num_features [int]: number of features
        num_labels [int]: number of classes
    Returns:
        [torch.Tensor]: class centers
    """
    assert num_features >= num_labels, "feature dimensions must be greater than labels."
    valid_num = num_features
    if num_features % num_labels != 0:
        valid_num = (num_features // num_labels) * num_labels
    dim_entries = torch.randperm(num_features)[:valid_num].reshape(num_labels, -1)
    current_norm = (num_features // num_labels)**0.5
    return torch.zeros(num_labels, num_features).scatter_(1, dim_entries, 1) * (norm / current_norm)

def gen_class_centers_MM(norm, num_features, num_labels, random_rotate=True):
    """
    Generate MMD class centers.
    Args:
        norm [float/int]: l2 norm of the class centers
        num_features [int]: number of features
        num_labels [int]: number of classes
        random_rotate [bool]: whether to randomly rotate the class centers
    Returns:
        [torch.Tensor]: class centers
    """
    assert num_features >= num_labels, "feature dimensions must be greater than labels."
    class_centers = torch.zeros(num_labels, num_features)
    class_centers[0, 0] = 1
    L_m1 = num_labels - 1
    for i in range(1, num_labels):
        for j in range(i):
            class_centers[i, j] = -(1 + class_centers[i].dot(class_centers[j]) * L_m1)/(class_centers[j, j] * L_m1)
        class_centers[i, i] = torch.relu(1 - t_s_l2_norm(class_centers[i], dim=-1)) ** 0.5
    if random_rotate:
        class_centers = random_rotate_vectors(class_centers)
    class_centers = class_centers * norm
    return class_centers

def get_rotation_matrix(reference, target):
    """
    Compute a rotation matrix that rotates the reference vector to the target vector.
    Args:
        reference [torch.Tensor]: reference vector
        target [torch.Tensor]: target vector
    Returns:
        [torch.Tensor]: rotation matrix
    """
    nr = reference / t_l2_norm(reference, dim=-1)
    orthog = target - nr.dot(target) * nr
    nt = orthog / t_l2_norm(orthog, dim=-1)
    a = torch.acos(reference.dot(target)/t_l2_norm(reference, dim=-1)/t_l2_norm(target, dim=-1))
    R = torch.eye(reference.shape[0]) + (torch.ger(nt,nr) - torch.ger(nr,nt)) * torch.sin(a) + (torch.ger(nr,nr) + torch.ger(nt,nt)) * (torch.cos(a)-1)
    return R

def random_rotate_vectors(vectors):
    """
    Randomly generate a rotation matrix and rotate all vectors with it.
    Args:
        vectors [torch.Tensor]: vectors to rotate
    Returns:
        [torch.Tensor]: rotated vectors
    """
    target = torch.rand(vectors.shape[1])
    target = target / t_l2_norm(target, dim=-1)
    R = get_rotation_matrix(vectors[0], target)
    return vectors.mm(R.T)

class MAT(nn.Module):
    """
    A model wrapper to train the inner model to output compact features.
    Hidden parameters: class centers
    Output: features
    Predict by:
        class of the closest feature center
    """

    def __init__(self, inner_model, MM=False, RR=True, alpha=0.5):
        """
        Args:
            inner_model [torch.nn.Module]: the CNN model to be trained
            MM [bool]: whether to use MMD centers
            RR [bool]: whether to apply random rotation
            alpha [float]: the weight of the SO loss
        """
        super().__init__()

        self.alpha = alpha
        self.gamma = 2

        self.initialize_inner_model(inner_model, MM=MM)

        self.cosloss = torch.nn.CosineEmbeddingLoss()
        self.celoss = torch.nn.CrossEntropyLoss()
        self.cos = torch.nn.CosineSimilarity(dim=-1)
        self.sf = torch.nn.Softmax(dim=-1)

        self.init_class_centers(MM=MM, RR=RR)

    def initialize_inner_model(self, inner_model, MM=False):
        """
        Remove the last layer from the inner model and record the dimensions.
        Args:
            inner_model [torch.nn.Module]: the inner model
            MM [bool]: whether to use MMD centers
        Returns:
            None
        """
        out_layer = inner_model.out_layer()
        self.num_labels = out_layer.out_features
        self.num_features = out_layer.in_features
        if MM:
            self.inner_model = inner_model.no_final_block_version()
        else:
            self.inner_model = inner_model.no_final_version()

    def init_class_centers(self, MM=False, RR=True):
        """
        Initialize class centers as Pytorch module parameters.
        Args:
            MM [bool]: whether to use MMD centers
            RR [bool]: whether to apply random rotation
        Returns:
            None
        """
        if MM:
            self.class_centers = nn.Parameter(gen_class_centers_MM(100, self.num_features, self.num_labels, random_rotate=RR), requires_grad=False)
        else:
            self.class_centers = nn.Parameter(gen_class_centers_orthogonal(1, self.num_features, self.num_labels), requires_grad=False)

    def forward(self, input):
        """
        Model Inference. (Required for Pytorch Module)
        Args:
            input [torch.Tensor]: input image
        Returns:
            [torch.Tensor]: features
        """
        return self.inner_model(input)

    def get_loss(self, features, labels, device, variant='cosine'):
        """
        Compute MAT loss (as requested variant).
        """
        if variant == 'cosine':
            # FTC loss
            # 1 - cos(phi, mu)
            corr_centers = self.class_centers[labels].to(device)
            sim_loss = self.cosloss(features, corr_centers, torch.ones(features.shape[0]).to(device))
            return sim_loss
        elif variant == 'push':
            # gamma * (1 - cos(phi, mu)) + (1 - cos(phi, mu'))
            corr_centers = self.class_centers[labels].to(device)
            sim_loss = self.cosloss(features, corr_centers, torch.ones(features.shape[0]).to(device))
            features = features.unsqueeze(1) # bx1xf
            cos_sim = self.cos(features, self.class_centers.to(device)) # bxcls
            cos_sim[labels] = 0 # set ground truth to 0 to avoid found by max
            nearest_wrong = cos_sim.max(1)[1]
            no_corr_centers = self.class_centers[nearest_wrong].to(device)
            dissim_loss = self.cosloss(features.squeeze(1), no_corr_centers, -torch.ones(features.shape[0]).to(device))
            return self.gamma * sim_loss + dissim_loss
        elif variant == 'ce':
            # cross_entropy(phi Mu, y)
            self.class_centers.to(device)
            logits = features.mm(self.class_centers.T)
            return self.celoss(logits, labels)
        elif variant == 'so':
            # FTC loss + alpha * SO loss
            # (1 - cos(phi, mu)) + alpha * grad
            corr_centers = self.class_centers[labels].to(device)
            sim_loss = self.cosloss(features, corr_centers, torch.ones(features.shape[0]).to(device))
            cos_sim = self.cos(features, corr_centers).unsqueeze(1) # bx1
            features_norm = features.norm(2, dim=-1).unsqueeze(1) # bx1
            grad = (corr_centers / corr_centers.norm(2, dim=-1).unsqueeze(1) - cos_sim * features / features_norm) / features_norm # bxf
            grad_loss = t_s_l2_norm(grad, dim=-1).mean() # bxf => b => 0
            return sim_loss + self.alpha * grad_loss
        else:
            raise ValueError("variant not supported")

    def feature_sims(self, features, device):
        """
        Compute the cosine similarity between features and each class center.
        Args:
            features [torch.Tensor]: feature vector (model output)
            device [torch.device]: device of @features
            TODO: use feature.device instead of passing device as argument
        Returns:
            [torch.Tensor]: cosine similarity
        """
        features = features.unsqueeze(1) # bx1xf
        cos_sim = self.cos(features, self.class_centers.to(device)) # bxcls
        return cos_sim

    def predict_by_features(self, features, device):
        """
        Predict classes based on cosine similarity.
        Args:
            features [torch.Tensor]: feature vector (model output)
            device [torch.device]: device of @features
            TODO: use feature.device instead of passing device as argument
        Returns:
            [torch.Tensor]: predictions
        """
        cos_sim = self.feature_sims(features, device)
        return cos_sim.max(1)[1]

    def get_class_centers(self):
        """
        Getter for self.class_centers.
        Note: 
            utils.function_helper.Delegator turns every member of an object into wrapped funciton,
            and thus makes getting member properties impossible and requires getter-methods accordingly.
        Returns:
            [torch.Tensor]: class centers
        """
        return self.class_centers

    def for_attack_version(self):
        """
        Transform this model into a traditional CNN model (with only regular operation layers).
        Note: see Section 3.6 in the Thesis for details.
        Returns:
            [torch.nn.Module]: transformed model
        """
        return FMAT(self)

class FMAT(MAT):
    def __init__(self, wrapped_model):
        """
        Args:
            wrapped_model [torch.nn.Module]: model to be transformed
        """
        self.get_dimensions(wrapped_model)
        super().__init__(wrapped_model.inner_model)
        st = wrapped_model.state_dict()
        self.load_state_dict(st)
        self.cos = torch.nn.CosineSimilarity(dim=-1)

    def get_dimensions(self, wrapped_model):
        """
        Record dimensions.
        Args:
            wrapped_model [torch.nn.Module]: model to be transformed
        Returns:
            None
        """
        self.num_labels, self.num_features = wrapped_model.class_centers.shape

    def initialize_inner_model(self, inner_model, MM=False, RR=True):
        """
        Remove the last layer from the inner model and record the dimensions.
        Args:
            inner_model [torch.nn.Module]: the inner model
            MM [bool]: whether to use MMD centers
            RR [bool]: whether to apply random rotation
        Returns:
            None
        """
        self.inner_model = inner_model

    def forward(self, input):
        """
        Model Inference. (Required for Pytorch Module)
        Args:
            input [torch.Tensor]: input image
        Returns:
            [torch.Tensor]: logits
        """
        features = super().forward(input)
        return features.mm(self.class_centers.T)