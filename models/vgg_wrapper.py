import torch.nn as nn
from torchvision.models.vgg import VGG, cfgs, make_layers, model_urls

try:
    from torch.hub import load_state_dict_from_url
except ImportError:
    from torch.utils.model_zoo import load_url as load_state_dict_from_url

class inner_VGG(VGG):
    def __init__(self, arch, cfg, batch_norm, pretrained, progress, gray_level=False, **kwargs):
        """
        Args:
            Original arguments of torchvision.models.resnet.ResNet:
                arch
                cfg
                batch_norm
                kwargs
            
            pretrained [bool]: whether to load pretrained weights
            progress [bool]: whether to show progress bar when loading pretrained weights
            gray_level [bool]: whether to take 1-channel input
        """
        self.model_init_kwargs = {"gray_level": gray_level, **kwargs}
        if pretrained:
            kwargs['init_weights'] = False
            state_dict = load_state_dict_from_url(model_urls[arch],
                                                progress=progress)
            num_classes = kwargs['num_classes']
            if num_classes != 1000:
                del kwargs['num_classes']
                super().__init__(make_layers(cfgs[cfg], batch_norm=batch_norm), **kwargs)
                self.load_state_dict(state_dict)
                self.classifier[-1] = nn.Linear(4096, num_classes)
            else:
                super().__init__(make_layers(cfgs[cfg], batch_norm=batch_norm), **kwargs)
                self.load_state_dict(state_dict)
        else:
            super().__init__(make_layers(cfgs[cfg], batch_norm=batch_norm), **kwargs)
        if gray_level:
            old_1st = self.features[0]
            self.features[0] = nn.Conv2d(1, old_1st.out_channels, kernel_size=old_1st.kernel_size, padding=old_1st.padding, stride=old_1st.stride)

    def get_model_init_kwargs(self):
        """
        Getter for self.model_init_kwargs.
        Note: 
            utils.function_helper.Delegator turns every member of an object into wrapped funciton,
            and thus makes getting member properties impossible and requires getter-methods accordingly.
        Returns:
            [torch.Tensor]: model initialization kwargs
        """
        return self.model_init_kwargs

class vgg_common:
    def out_layer(self):
        """
        Return the output layer.
        """
        return self.classifier[-1]

    def no_final_version(self):
        """
        Return a copy of this model with the final layer removed.
        """
        model = type(self)(**self.get_model_init_kwargs())
        model.load_state_dict(self.state_dict())
        new_classifier = nn.Sequential(*list(model.classifier.children())[:-2]) # take out Dropout(-2) and Linear(-1)
        model.classifier = new_classifier
        return model

    def no_final_block_version(self):
        """
        Return a copy of this model with the final layer and the previous ReLU layer (if any) removed.
        """
        model = type(self)(**self.get_model_init_kwargs())
        model.load_state_dict(self.state_dict())
        new_classifier = nn.Sequential(*list(model.classifier.children())[:-3]) # take out ReLU(-3), Dropout(-2), and Linear(-1)
        model.classifier = new_classifier
        return model

"""
The code below are modified from torchvision.models.vgg
"""

class vgg11(inner_VGG, vgg_common):
    r"""VGG 11-layer model (configuration "A") from
    `"Very Deep Convolutional Networks For Large-Scale Image Recognition" <https://arxiv.org/pdf/1409.1556.pdf>`_
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    def __init__(self, pretrained=False, progress=True, num_classes=10, **kwargs):
        super().__init__('vgg11', 'A', False, pretrained, progress, num_classes=num_classes, **kwargs)

class vgg11_bn(inner_VGG, vgg_common):
    r"""VGG 11-layer model (configuration "A") with batch normalization
    `"Very Deep Convolutional Networks For Large-Scale Image Recognition" <https://arxiv.org/pdf/1409.1556.pdf>`_
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    def __init__(self, pretrained=False, progress=True, num_classes=10, **kwargs):
        super().__init__('vgg11_bn', 'A', True, pretrained, progress, num_classes=num_classes, **kwargs)


class vgg13(inner_VGG, vgg_common):
    r"""VGG 13-layer model (configuration "B")
    `"Very Deep Convolutional Networks For Large-Scale Image Recognition" <https://arxiv.org/pdf/1409.1556.pdf>`_
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    def __init__(self, pretrained=False, progress=True, num_classes=10, **kwargs):
        super().__init__('vgg13', 'B', False, pretrained, progress, num_classes=num_classes, **kwargs)


class vgg13_bn(inner_VGG, vgg_common):
    r"""VGG 13-layer model (configuration "B") with batch normalization
    `"Very Deep Convolutional Networks For Large-Scale Image Recognition" <https://arxiv.org/pdf/1409.1556.pdf>`_
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    def __init__(self, pretrained=False, progress=True, num_classes=10, **kwargs):
        super().__init__('vgg13_bn', 'B', True, pretrained, progress, num_classes=num_classes, **kwargs)


class vgg16(inner_VGG, vgg_common):
    r"""VGG 16-layer model (configuration "D")
    `"Very Deep Convolutional Networks For Large-Scale Image Recognition" <https://arxiv.org/pdf/1409.1556.pdf>`_
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    def __init__(self, pretrained=False, progress=True, num_classes=10, **kwargs):
        super().__init__('vgg16', 'D', False, pretrained, progress, num_classes=num_classes, **kwargs)


class vgg16_bn(inner_VGG, vgg_common):
    r"""VGG 16-layer model (configuration "D") with batch normalization
    `"Very Deep Convolutional Networks For Large-Scale Image Recognition" <https://arxiv.org/pdf/1409.1556.pdf>`_
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    def __init__(self, pretrained=False, progress=True, num_classes=10, **kwargs):
        super().__init__('vgg16_bn', 'D', True, pretrained, progress, num_classes=num_classes, **kwargs)


class vgg19(inner_VGG, vgg_common):
    r"""VGG 19-layer model (configuration "E")
    `"Very Deep Convolutional Networks For Large-Scale Image Recognition" <https://arxiv.org/pdf/1409.1556.pdf>`_
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    def __init__(self, pretrained=False, progress=True, num_classes=10, **kwargs):
        super().__init__('vgg19', 'E', False, pretrained, progress, num_classes=num_classes, **kwargs)


class vgg19_bn(inner_VGG, vgg_common):
    r"""VGG 19-layer model (configuration 'E') with batch normalization
    `"Very Deep Convolutional Networks For Large-Scale Image Recognition" <https://arxiv.org/pdf/1409.1556.pdf>`_
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    def __init__(self, pretrained=False, progress=True, num_classes=10, **kwargs):
        super().__init__('vgg19_bn', 'E', True, pretrained, progress, num_classes=num_classes, **kwargs)
