# architectures or wrappers of Pytorch models

## Remarks  
all the models have three additional functions for further experiments  
1. ```out_layer```: return the output layer
2. ```no_final_version```: return a copied model whose output layer is removed
3. ```no_final_block_version```: return a copied model whose output layer and preceeding ReLU layer are removed

## Introduction of codebase  

### Modules  
* **small_cnn.py**  
simple shallow CNN for classification (used in the TRADES paper)  
**NOTE**: code is modified from TRADES released repo [GitHub](https://github.com/yaodongyu/TRADES)

* **vgg_wrapper.py**  
wrappers for VGG models from torchvision  

* **wideresnet.py**  
wide versoin of ResNet (used in the TRADES paper)  
**NOTE**: code is modified from TRADES released repo [GitHub](https://github.com/yaodongyu/TRADES)

* **resnet_wrapper.py**  
wrappers for ResNet and ResNext models from torchvision  
the ResNet and ResNext provided by torchvision are functions instead of classes  
so it is troublesome to add the two additional functions to those models  
this file provides class version of those models

* **MAT.py**  
wrapper for changing a traditional CNN into a model w/ feature clustering  
can be used on all models once they have methods: ```out_layer```, ```no_final_version```, and ```no_final_block_version```  