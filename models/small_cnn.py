from collections import OrderedDict

import torch.nn as nn

class SmallCNN(nn.Module):
    """
    Implementation of the CNN architecture used in TRADES.
    Reference: https://github.com/yaodongyu/TRADES
    """
    def __init__(self, drop=0.5, gray_level=True, num_classes=10):
        """
        Args:
            drop [float]: dropout probability
            gray_level [bool]: whether to take 1-channel input
            num_classes [int]: number of classes
        """
        super(SmallCNN, self).__init__()
        self.model_init_kwargs = {'drop': drop, 'gray_level': gray_level, 'num_classes': num_classes}

        self.num_channels = 1 if gray_level else 3
        self.num_labels = num_classes

        activ = nn.ReLU

        self.feature_extractor = nn.Sequential(OrderedDict([
            ('conv1', nn.Conv2d(self.num_channels, 32, 3)),
            ('relu1', activ(True)),
            ('conv2', nn.Conv2d(32, 32, 3)),
            ('relu2', activ(True)),
            ('maxpool1', nn.MaxPool2d(2, 2)),
            ('conv3', nn.Conv2d(32, 64, 3)),
            ('relu3', activ(True)),
            ('conv4', nn.Conv2d(64, 64, 3)),
            ('relu4', activ(True)),
            ('maxpool2', nn.MaxPool2d(2, 2)),
        ]))

        self.classifier = nn.Sequential(OrderedDict([
            ('fc1', nn.Linear(64 * 4 * 4, 200)),
            ('relu1', activ(True)),
            ('drop', nn.Dropout(drop)),
            ('fc2', nn.Linear(200, 200)),
            ('relu2', activ(True)),
            ('fc3', nn.Linear(200, self.num_labels)),
        ]))

        for m in self.modules():
            if isinstance(m, (nn.Conv2d)):
                nn.init.kaiming_normal_(m.weight)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
        nn.init.constant_(self.classifier.fc3.weight, 0)
        nn.init.constant_(self.classifier.fc3.bias, 0)

    def get_model_init_kwargs(self):
        """
        Getter for self.model_init_kwargs.
        Note: 
            utils.function_helper.Delegator turns every member of an object into wrapped funciton,
            and thus makes getting member properties impossible and requires getter-methods accordingly.
        Returns:
            [torch.Tensor]: model initialization kwargs
        """
        return self.model_init_kwargs

    def forward(self, input):
        """
        Model Inference. (Required for Pytorch Module)
        Args:
            input [torch.Tensor]: input image
        Returns:
            [torch.Tensor]: logits
        """
        features = self.feature_extractor(input)
        logits = self.classifier(features.view(-1, 64 * 4 * 4))
        return logits
    
    def out_layer(self):
        """
        Return the output layer.
        """
        return self.classifier[-1]
    
    def no_final_version(self):
        """
        Return a copy of this model with the final layer removed.
        """
        model = type(self)(**self.get_model_init_kwargs())
        model.load_state_dict(self.state_dict())
        new_classifier = nn.Sequential(*list(model.classifier.children())[:-1])
        model.classifier = new_classifier
        return model

    def no_final_block_version(self):
        """
        Return a copy of this model with the final layer and the previous ReLU layer (if any) removed.
        """
        model = type(self)(**self.get_model_init_kwargs())
        model.load_state_dict(self.state_dict())
        new_classifier = nn.Sequential(*list(model.classifier.children())[:-2])
        model.classifier = new_classifier
        return model

    def intermediate_forward(self, input):
        """
        Compute output of the last convolutional layer.
        Args:
            input [torch.Tensor]: input image
        Returns:
            [torch.Tensor]: feature map
        """
        return self.feature_extractor(input)
