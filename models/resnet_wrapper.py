import torch.nn as nn
from torchvision.models.resnet import (BasicBlock, Bottleneck, ResNet,
                                       model_urls)

try:
    from torch.hub import load_state_dict_from_url
except ImportError:
    from torch.utils.model_zoo import load_url as load_state_dict_from_url

class inner_resnet(ResNet):
    """
    A ResNet wrapper.
    """
    def __init__(self, arch, block, layers, pretrained=False, progress=True, gray_level=False, **kwargs):
        """
        Args:
            Original arguments of torchvision.models.resnet.ResNet:
                arch
                block
                layers
                kwargs
            
            pretrained [bool]: whether to load pretrained weights
            progress [bool]: whether to show progress bar when loading pretrained weights
            gray_level [bool]: whether to take 1-channel input
        """
        self.model_init_kwargs = {"gray_level": gray_level, **kwargs}
        if pretrained:
            state_dict = load_state_dict_from_url(model_urls[arch],
                                                progress=progress)
            num_classes = kwargs['num_classes']
            if num_classes != 1000:
                del kwargs['num_classes']
                super().__init__(block, layers, **kwargs)
                self.load_state_dict(state_dict)
                self.fc = nn.Linear(512 * block.expansion, num_classes)
            else:
                super().__init__(block, layers, **kwargs)
                self.load_state_dict(state_dict)
        else:
            super().__init__(block, layers, **kwargs)
        if gray_level:
            old_1st = self.conv1
            self.conv1 = nn.Conv2d(1, old_1st.out_channels, kernel_size=old_1st.kernel_size, padding=old_1st.padding, stride=old_1st.stride, bias=old_1st.bias)

    def get_model_init_kwargs(self):
        """
        Getter for self.model_init_kwargs.
        Note: 
            utils.function_helper.Delegator turns every member of an object into wrapped funciton,
            and thus makes getting member properties impossible and requires getter-methods accordingly.
        Returns:
            [torch.Tensor]: model initialization kwargs
        """
        return self.model_init_kwargs

class resnet_common:
    def out_layer(self):
        """
        Return the output layer.
        """
        return self.fc
        
    def no_final_version(self):
        """
        Return a copy of this model with the final layer removed.
        """
        model = type(self)(**self.get_model_init_kwargs())
        model.load_state_dict(self.state_dict())
        model = nn.Sequential(*list(model.children())[:-1])
        return one_seq_model(model)

    def no_final_block_version(self):
        """
        Return a copy of this model with the final layer and the previous ReLU layer (if any) removed.
        """
        return self.no_final_version() # resnet has no ReLU after the penultimate layer

class resnet18(inner_resnet, resnet_common):
    def __init__(self, *args, num_classes=10, **kwargs):
        super().__init__('resnet18', BasicBlock, [2, 2, 2, 2], *args, num_classes=num_classes, **kwargs)

class resnet34(inner_resnet, resnet_common):
    def __init__(self, *args, num_classes=10, **kwargs):
        super().__init__('resnet34', BasicBlock, [3, 4, 6, 3], *args, num_classes=num_classes, **kwargs)

class resnet50(inner_resnet, resnet_common):
    def __init__(self, *args, num_classes=10, **kwargs):
        super().__init__('resnet50', Bottleneck, [3, 4, 6, 3], *args, num_classes=num_classes, **kwargs)

class resnet101(inner_resnet, resnet_common):
    def __init__(self, *args, num_classes=10, **kwargs):
        super().__init__('resnet101', Bottleneck, [3, 4, 23, 3], *args, num_classes=num_classes, **kwargs)

class resnet152(inner_resnet, resnet_common):
    def __init__(self, *args, num_classes=10, **kwargs):
        super().__init__('resnet152', Bottleneck, [3, 8, 36, 3], *args, num_classes=num_classes, **kwargs)

class resnext50(inner_resnet, resnet_common):
    def __init__(self, *args, num_classes=10, **kwargs):
        super().__init__('resnext50_32x4d', Bottleneck, [3, 4, 6, 3], *args, num_classes=num_classes, groups=32, width_per_group=4, **kwargs)

class resnext101(inner_resnet, resnet_common):
    def __init__(self, *args, num_classes=10, **kwargs):
        super().__init__('resnext101_32x8d', Bottleneck, [3, 4, 23, 3], *args, num_classes=num_classes, groups=32, width_per_group=8, **kwargs)

class wide_resnet50(inner_resnet, resnet_common):
    def __init__(self, *args, num_classes=10, **kwargs):
        super().__init__('wide_resnet50_2', Bottleneck, [3, 4, 6, 3], *args, num_classes=num_classes, width_per_group=64 * 2, **kwargs)

class wide_resnet101(inner_resnet, resnet_common):
    def __init__(self, *args, num_classes=10, **kwargs):
        super().__init__('wide_resnet101_2', Bottleneck, [3, 4, 23, 3], *args, num_classes=num_classes, width_per_group=64 * 2, **kwargs)

class one_seq_model(nn.Module):
    """
    A module with only one torch.nn.Sequential object.
    """
    def __init__(self, sequential):
        super().__init__()
        self.sequential = sequential
        
    def forward(self, input_):
        return self.sequential(input_)[..., 0, 0]
