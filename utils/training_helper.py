import mlflow
import torch

from utils.criteria import INIT_ACC, INIT_CALLBACK, INIT_LOSS, is_with_acc


def to_1_hot(labels, classes):
    """
    Encode integer class labels as one-hot vectors.
    Args:
        labels [torch.Tensor]: integer labels
        classes [int]: total number of classes
    Returns:
        [torch.Tensor]: encoded labels
    """
    return torch.zeros(labels.shape[0], classes).to(labels.device).scatter_(1, labels.unsqueeze(-1), 1)

def iter_dataloader(model, device, data_loader, Loss=None, acc=True, pred_func=None):
    """
    Generator that iterates over a data loader and returns requested values.
    Args:
        model [torch.nn.Module]: model for inference
        device [torch.device]: device
        data_loader [torch.utils.data.DataLoader]: data loader
        Loss [torch.nn.Module]: loss function for computing loss value
        acc [bool]: whether to compute accuracy
        pred_func [Callable]: function that maps model output to predictions (for computing accuracy)
    Yields:
        [int]: batch size[, [float]: correct count][, [torch.Tensor]: loss value sum]
    """
    if pred_func is None:
        pred_func = lambda y: y.max(1)[1]
    model.eval()
    with torch.no_grad():
        for data, target in data_loader:
            data, target = data.to(device), target.to(device)
            y = model(data)
            res = [data.shape[0]]
            if acc:
                res.append((pred_func(y) == target).sum().item())
            if Loss is not None:
                res.append(Loss(y, target, data).item()*res[0])
            yield res

def score(model, device, data_loader, pred_func=None):
    """
    Compute the accuracy of a data loader.
    Args:
        model [torch.nn.Module]: model for inference
        device [torch.device]: device
        data_loader [torch.utils.data.DataLoader]: data loader
        pred_func [Callable]: function that maps model output to predictions
    Returns:
        [float]: accuracy
    """
    total = 0
    correct = 0
    for num, correct_ in iter_dataloader(model, device, data_loader, pred_func=pred_func):
        total += num
        correct += correct_
    return correct / total

def mean_loss(model, device, data_loader, Loss, pred_func=None):
    """
    Compute the accuracy of a data loader.
    Args:
        model [torch.nn.Module]: model for inference
        device [torch.device]: device
        data_loader [torch.utils.data.DataLoader]: data loader
        Loss [Callable]: loss function
        pred_func [Callable]: function that maps model output to predictions
    Returns:
        [float]: mean loss value
    """
    total = 0
    loss_val = 0.
    for num, loss in iter_dataloader(model, device, data_loader, Loss=Loss, acc=False, pred_func=pred_func):
        total += num
        loss_val += loss
    return loss_val / total

def stats(model, device, data_loader, Loss, pred_func=None):
    """
    Compute the accuracy of a data loader.
    Args:
        model [torch.nn.Module]: model for inference
        device [torch.device]: device
        data_loader [torch.utils.data.DataLoader]: data loader
        Loss [Callable]: loss function
        pred_func [Callable]: function that maps model output to predictions
    Returns:
        [float], [float]: mean loss value, accuracy
    """
    total = 0
    loss_val = 0.
    correct = 0
    for num, correct_, loss in iter_dataloader(model, device, data_loader, Loss=Loss, pred_func=pred_func):
        total += num
        loss_val += loss
        correct += correct_
    return loss_val / total, correct / total

def training_loop(model, data_loader, Loss, optimizer, early_stop, device,
                print_interval=10, test_data_loader=None,
                save_func=None, mlflow_track=False,
                before_iter=None, after_iter=None, pred_func=None, start_epoch=0,
                after_epoch=None, e_loss=None):
    """
    Training loop.
    Args:
        model [torch.nn.Module]: model to be trained
        data_loader [torch.utils.data.DataLoader]: data loader of training dataset
        Loss [Callable]: loss function
        optimizer [torch.optim.optimizer.Optimizer]: optimizer for updating weights
        early_stop [(criteria-related types)]: stopping criteria object
        device [torch.device]: device of model
        print_interval [int]: interval of epochs for printing training status
        test_data_loader [torch.utils.data.DataLoader]: data loader of testing dataset
        save_func [Callable]: function for saving model weights
        mlflow_track [bool]: whether to use mlflow to track training status
        before_iter [Callable]: function that is called before each iteration
        after_iter [Callable]: function that is called after each iteration
        pred_func [Callable]: function that maps model output to predictions
        start_epoch [int]: initial epoch number
        after_epoch [Callable]: function that is called after each epoch
        e_loss [Callable]: evaluation loss function that is used for computing the mean loss across the whole dataset for each epoch
    Returns:
        [torch.nn.Module], [int]: trained model, final epoch number
    """
    loss_val = None
    acc = INIT_ACC
    epoch = start_epoch
    with_acc = is_with_acc(early_stop)
    loader = test_data_loader if test_data_loader is not None else data_loader
    def proxy_loss(y, target, data):
        try:
            loss = Loss(y, target.long().squeeze(), data)
        except TypeError as e:
            if 'takes 2 positional arguments but 3 were given' in e.args[0]:
                loss = Loss(y, target.long().squeeze())
            else:
                raise e
        return loss
    try:
        while not (early_stop(loss_val, epoch - start_epoch, acc=acc) if with_acc else early_stop(loss_val, epoch - start_epoch)):
            model.train()
            print("============================================================")
            for batch_idx, (data, target) in enumerate(data_loader):
                data, target = data.to(device), target.to(device)
                optimizer.zero_grad()
                if before_iter is not None:
                    before_iter(model, data, target, device, epoch)
                y = model(data)
                loss = proxy_loss(y, target, data)
                loss.backward()
                torch.nn.utils.clip_grad_norm_(model.parameters(), 5)
                optimizer.step()
                if after_iter is not None:
                    after_iter(model, data, target, device, epoch)

                loss_val = loss.item()

                if batch_idx % print_interval == 0:
                    print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                        epoch, batch_idx * len(data), len(data_loader.dataset),
                            100. * batch_idx / len(data_loader), loss_val))

                if torch.isnan(loss).any():
                    print("Nan occurs in loss")
                    return model, epoch

            if save_func is not None:
                early_stop.update_callback(lambda: save_func(model, epoch))
            
            epoch += 1

            if after_epoch is not None:
                after_epoch(model, device, epoch)

            print("============================================================")
            e_loss_ = proxy_loss if e_loss is None else e_loss
            training_loss, acc = stats(model, device, data_loader, e_loss_, pred_func=pred_func)
            loss_val = training_loss
            print("Training loss: {}, accuracy: {}".format(training_loss, acc))
            if mlflow_track:
                mlflow.log_metric('train_loss', training_loss, step=epoch)
                mlflow.log_metric('train_acc', acc, step=epoch)
            if test_data_loader is not None:
                validation_loss, acc = stats(model, device, test_data_loader, e_loss_, pred_func=pred_func)
                print("Testing loss: {}, accuracy: {}".format(validation_loss, acc))
                loss_val = validation_loss
                if mlflow_track:
                    mlflow.log_metric('test_loss', validation_loss, step=epoch)
                    mlflow.log_metric('test_acc', acc, step=epoch)
            
        else:
            print("Stop training because {}".format(early_stop))
    except KeyboardInterrupt:
        print("Stop training because user requested by 'CTRL + C'")
        yn = input("Do you want to save the last model? [y/n]")
        if yn.upper() == "Y" and save_func is not None:
            save_func(model, epoch)
    return model, epoch
