from time import time, sleep

class OutTime:
    """
    Tool class to print execution time used by a piece of code.
    Usage:
        with OutTime():
            # code to execute...

    Note: one can create custom timing class by inheriting from this class and overriding onEnter and/or onExit methods.
    """
    switchOff = False
    DEFAULT_LOG_FUNCTION = lambda self, t: print("time passed: %5f s" % t)
    # set to True if you want to disable this tool.

    def __init__(self, log_function=None):
        """
        Args:
            log_function [Callable]: function to log passed time
        """
        if log_function is None:
            self.log_function = self.DEFAULT_LOG_FUNCTION
        else:
            self.log_function = log_function

    def __enter__(self):
        """
        Record start time when scope is opened.
        Returns:
            None
        """
        if self.switchOff: return
        self.st = time()
        self.onEnter(self.st)
    
    def __exit__(self, type_, value, traceback):
        """
        Print execution time when scope is closed.
        Returns:
            None
        """
        if self.switchOff: return
        t = time() - self.st
        try:
            self.log_function(t)
        except Exception as e:
            print(f"Custom log function threw an exception\n {type(e).__name__}: {e}")
            print("Roll back to default log function")
            self.DEFAULT_LOG_FUNCTION(t)
        self.onExit(t)
    
    def onEnter(self, starttime):
        """
        Callback function executed after the start time is recorded.
        Args:
            starttime [float]: the start time.
        """
        pass

    def onExit(self, time):
        """
        Callback function executed after the execution time is printed.
        Args:
            time [float]: the execution time.

        Note: one can access the start time by self.st.
        """
        pass

class FlagRecorder:
    """
    Print passed time between flags.
    Usage:
        fr = FlagRecorder()
        sleep(1)
        fr()
        sleep(0.5)
        fr("Step1")
    """
    def __init__(self):
        self.st = time()

    def __call__(self, message=""):
        """
        Print time passed since last call.
        Args:
            message [str]: additional message
        Returns:
            None
        """
        print(message + "::time passed: %5f s" % (time() - self.st))
        self.st = time()

def test():
    i = 1
    with OutTime():
        i += 1

    with OutTime(lambda t: print(t)):
        i += 1

    with OutTime(lambda a, b: print(a, b)):
        i += 1

    fr = FlagRecorder()
    sleep(1)
    fr()
    sleep(0.5)
    fr("Step1")

if __name__ == "__main__":
    test()