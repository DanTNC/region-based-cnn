import torch
from models.MAT import MAT

from utils.function_helper import DelegatorWDefault


def auto_device():
    """
    Create device based on whether a GPU is available.
    Returns:
        [torch.device], [dict]: device, keyword arguments for a data loader.
    """
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")
    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    return device, kwargs

class DelegatorDataParallel(DelegatorWDefault, torch.nn.DataParallel):
    """
    Multi-gpu inference wrapper with delegation ability.
    """
    DEFAULT_DELEGATE_TO = "module"

    def __init__(self, model):
        """
        Args:
            model [torch.nn.Module]: model to be wrapped
        """
        torch.nn.DataParallel.__init__(self, model)

    def __getattr__(self, name):
        """
        If module is accessed as a member property, do not delegate.
        Args:
            name [str]: name of the accessed attribute
        Returns:
            [any]: attribute
        """
        if name == 'module':
            return torch.nn.DataParallel.__getattr__(self, name)
        else:
            return super().__getattr__(name)

def auto_parallel(model, verbose=True):
    """
    Use multi-gpu wrapper if the number of available GPUs is higher than 1.
    Args:
        model [torch.nn.Module]: model to be wrapped
        verbose [bool]: whether to print GPU count
    Returns:
        [torch.nn.Module]: wrapped model
    """
    if torch.cuda.device_count() > 1:
        try:
            model = model.parallel()
        except AttributeError:
            model = DelegatorDataParallel(model)
        if verbose:
            print("gpu count: {}".format(torch.cuda.device_count()))
    return model

def auto_no_final_parallel(model):
    """
    Create the no-final-layer version of the model.
    Use multi-gpu wrapper if the number of available GPUs is higher than 1.
    Args:
        model [torch.nn.Module]: model to be wrapped
    Returns:
        [torch.nn.Module]: wrapped no-final-layer version model
    """
    try:
        model_ = model.module
    except AttributeError:
        model_ = model
    if isinstance(model_, MAT):
        return model
    try:
        return type(model)(model.module.no_final_version())
    except AttributeError:
        return model.no_final_version()
