import unittest

import numpy as np

from utils.data_helper import cosine


class UtilsTest(unittest.TestCase):
    def test_cosine(self):
        a = np.array([0, 1, 2])
        b = np.array([5, 6, 2])
        self.assertTrue(np.allclose(cosine(a, b), 10 / (65 ** (0.5) * 5 ** (0.5))))

        a = a[None, ...]
        self.assertTrue(np.allclose(cosine(a, b), [10 / (65 ** (0.5) * 5 ** (0.5))]))

        a = np.vstack((a, a - 1))
        self.assertTrue(np.allclose(cosine(a, b), [10 / (65 ** (0.5) * 5 ** (0.5)), -3 / (65 ** (0.5) * 2 ** (0.5))]))

        b = np.vstack((b, b * 2))
        self.assertTrue(np.allclose(cosine(a, b), [10 / (65 ** (0.5) * 5 ** (0.5)), -6 / (260 ** (0.5) * 2 ** (0.5))]))
