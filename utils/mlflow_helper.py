import json

def get_tracking_uri():
    """
    Load mlflow tracking path setting from json file.
    Returns:
        [str]: path
    """
    return json.load(open('path_setting.json', 'r'))['MLFLOW_TRACKING_URI']