# Default values
INIT_LOSS = float('inf')
INIT_ACC = 0
INIT_CALLBACK = lambda: None

def no_check_epoch_zero(func, *args, **kwargs):
    """
    Function decorator for the __call__ method of criteria objects,
    which makes the method not check the zeroth epoch.
    """
    def func_(*args, **kwargs):
        epoch = args[2]
        if epoch == 0:
            return False
        else:
            return func(*args, **kwargs)
    return func_

class CriteriaBase:
    """
    Basic training criteria.
    Stop training when the epoch number exceeds the preset limit.
    Log model weights periodically.
    """
    def __init__(self, MAX_EPOCHS=500, log_interval=5):
        """
        Args:
            MAX_EPOCHS [int]: maximum number of epochs to train
            log_interval [int]: number of epochs between each log for model weights
        """
        self.MAX_EPOCHS = MAX_EPOCHS
        self.converge_epoch = 0
        self.reason = None
        self.log_interval = log_interval
        self.callback = INIT_CALLBACK
        self.final_saved_epoch = -1

    def save_model(self, epoch):
        """
        Save model weights (the saving method is based on self.callback)
        Args:
            epoch [int]: current epoch number
        Returns:
            None
        """
        self.final_saved_epoch = epoch
        self.callback()

    def save_periodically(self, epoch):
        """
        Save model weights if the preset interval of logging is reached.
        Args:
            epoch [int]: current epoch number
        Returns:
            None
        """
        if (epoch - 1) % self.log_interval == 0:
            self.save_model(epoch)

    def update_callback(self, callback):
        """
        Update saving function.
        Args:
            callback [Callable]: saving function
        Returns:
            None
        """
        self.callback = callback

    @no_check_epoch_zero
    def __call__(self, loss, epoch):
        """
        Check if the current status of training satisfies the stopping criteria.
        Args:
            loss [torch.Tensor]: current training loss
            epoch [int]: current epoch number
        Returns:
            [bool]: whether the training should terminate
        """
        self.save_periodically(epoch)
        if epoch >= self.MAX_EPOCHS:
            self.reason = "max epochs ({}) exceeded".format(self.MAX_EPOCHS)
            self.save_model(epoch)
            return True
        else:
            return False

    def __str__(self):
        """
        String representation of this object using the reason of stopping.
        Returns:
            [str]: stopping reason
        """
        if self.reason is not None:
            return self.reason
        else:
            return "still training"

    def profile(self):
        """
        Construct a dictionary describing the settings.
        Returns:
            [dict]: settings
        """
        attrs = {
            "name": "Base",
            "max_epochs": self.MAX_EPOCHS
        }
        return attrs

class CriteriaBest(CriteriaBase):
    """
    Stop training when the loss converges or the epoch number exceeds the preset limit.
    Log model weights when the loss is reduced.
    """
    def __init__(self, CONVERGE_EPOCHS=5, **kwargs):
        """
        Args:
            CONVERGE_EPOCHS [int]: maximum number of epochs to train
            kwargs [dict]: keyword arguments of parent class (CriteriaBase.__init__)
        """
        super().__init__(**kwargs)
        self.CONVERGE_EPOCHS = CONVERGE_EPOCHS
        self.converge_loss = INIT_LOSS

    @no_check_epoch_zero
    def __call__(self, loss, epoch):
        """
        Check if the current status of training satisfies the stopping criteria.
        Args:
            loss [torch.Tensor]: current training loss
            epoch [int]: current epoch number
        Returns:
            [bool]: whether the training should terminate
        """
        if epoch >= self.MAX_EPOCHS:
            if self.converge_loss > loss:
                self.save_model(epoch)
            self.reason = "max epochs ({}) exceeded".format(self.MAX_EPOCHS)
            return True # force stopping
        if self.converge_loss < loss:
            self.converge_epoch += 1
            if self.converge_epoch >= self.CONVERGE_EPOCHS:
                self.reason = "after {} epochs, no better loss achieved".format(self.CONVERGE_EPOCHS)
                return True # after self.CONVERGE_EPOCHS epochs, not better model gotten, stop
            else:
                return False # not better, keep training
        else:
            self.converge_epoch = 0
            self.converge_loss = loss
            self.save_model(epoch)
            return False # better, save model

    def profile(self):
        """
        Construct a dictionary describing the settings.
        Returns:
            [dict]: settings
        """
        prof = super().profile()
        prof["name"] = "Best"
        prof["converge_epochs"] = self.CONVERGE_EPOCHS
        return prof

class WithAcc:
    """
    Wrapper for criteria objects.
    Add an additional stopping criterion which checks if the current accuracy is higher than a threshold.
    """
    def __init__(self, criteria, CONVERGE_ACC=None):
        """
        Args:
            criteria [CriteriaBase/CriteriaBest]: criteria to be wrapped
            CONVERGE_ACC [float]: accuracy threshold
        """
        self.criteria = criteria
        if CONVERGE_ACC is None:
            CONVERGE_ACC = 0.95
        self.CONVERGE_ACC = CONVERGE_ACC
        
    @no_check_epoch_zero
    def __call__(self, *args, acc=None):
        """
        Check if the current status of training satisfies the stopping criteria.
        Args:
            args [list]: list of arguments for the wrapped criteria object
            acc [float]: current accuracy
        Returns:
            [bool]: whether the training should terminate
        """
        epoch = args[1]
        if acc is None:
            return self.criteria(*args)
        return (self.criteria(*args) and acc > self.CONVERGE_ACC) or (epoch >= self.criteria.MAX_EPOCHS)

    def update_callback(self, callback):
        """
        Update saving function.
        Args:
            callback [Callable]: saving function
        Returns:
            None
        """
        self.criteria.callback = callback

    def __str__(self):
        """
        String representation of this object using the reason of stopping.
        Returns:
            [str]: stopping reason
        """
        return str(self.criteria)

    def __getattr__(self, name):
        """
        When accessing final_saved_epoch property, delegate to the wrapped criteria object.
        Args:
            name [str]: name of the attribute (API of the magic method: __getattr__)
        Returns:
            [int]: final saved epoch
        """
        if name == 'final_saved_epoch':
            return self.criteria.final_saved_epoch
        return super().__getattribute__(name)
    
    def profile(self):
        """
        Construct a dictionary describing the settings.
        Returns:
            [dict]: settings
        """
        prof = self.criteria.profile()
        prof["name"] += " w/ Acc"
        prof["converge_acc"] = self.CONVERGE_ACC
        return prof

class AdaptiveWithAcc(WithAcc):
    """
    Wrapper for criteria objects.
    Adjust learning rate when the loss converges but the accuracy threshold is not reached yet.
    """
    def __init__(self, criteria, scheduler=None, CONVERGE_ACC=None):
        """
        Args:
            criteria [CriteriaBase/CriteriaBest]: criteria to be wrapped
            scheduler [Scheduler]: scheduler used to adjust learning rate
            CONVERGE_ACC [float]: accuracy threshold
        """
        super().__init__(criteria, CONVERGE_ACC=CONVERGE_ACC)
        if scheduler is None:
            raise ValueError("AdaptiveWithAcc requires scheduler")
        self.scheduler = scheduler

    @no_check_epoch_zero
    def __call__(self, *args, acc=None):
        """
        Check if the current status of training satisfies the stopping criteria.
        Args:
            args [list]: list of arguments for the wrapped criteria object
            acc [float]: current accuracy
        Returns:
            [bool]: whether the training should terminate
        """
        epoch = args[1]
        if acc is None:
            raise ValueError("AdaptiveWithAcc requires accuracy")
        converge = self.criteria(*args)
        if (epoch >= self.criteria.MAX_EPOCHS):
            return True
        if converge:
            if (acc <= self.CONVERGE_ACC):
                if self.scheduler.should_terminate():
                    return True
                else:
                    self.scheduler.adjust(epoch)
                    self.criteria.converge_epoch = 0
                    return False
            return True
        return False
        
    def profile(self):
        """
        Construct a dictionary describing the settings.
        Returns:
            [dict]: settings
        """
        prof = self.criteria.profile()
        prof["name"] += " w/ Acc (adapt)"
        prof["converge_acc"] = self.CONVERGE_ACC
        return prof

class Scheduler:
    """
    Learning rate scheduler.
    """
    def __init__(self, lr, optimizer, lr_decay_rate, lr_decay_max_times, ext="", mlflow=None):
        """
        Args:
            lr [float]: learning rate
            optimizer [torch.optim.optimizer.Optimizer]: optimizer that contains learning rate
            lr_decay_rate [float]: learning rate adjustment factor
            lr_decay_max_times [int]: maximum number of times of adjustment
            ext [str]: extension of the tag when logging adjustment event epoch using mlflow (see self.adjust)
            mlflow [Module]: mlflow module
        """
        self.lr = lr
        self.optimizer = optimizer
        self.lr_decay_rate = lr_decay_rate
        self.lr_decay_max_times = lr_decay_max_times
        self.adjust_times = 0
        self.ext = ext
        self.mlflow = mlflow

    def reset(self, ext=""):
        """
        Reset scheduler status.
        Args:
            ext [str]: extension of the tag when logging adjustment event epoch using mlflow (see self.adjust)
        Returns:
            [Scheduler]: this object
        """
        self.adjust_times = 0
        self.ext = ext
        return self
        
    def adjust(self, epoch):
        """
        Adjust learning rate.
        Args:
            epoch [int]: current epoch number
        Returns:
            None
        """
        self.adjust_times += 1
        target_lr = self.lr * (self.lr_decay_rate ** self.adjust_times)
        for param_group in self.optimizer.param_groups:
            param_group['lr'] = target_lr
        if self.mlflow is not None:
            self.mlflow.set_tag("adj_to_{:.7f}{}".format(target_lr, self.ext), epoch)
        print("adjusting learning_rate to {}".format(target_lr))

    def should_terminate(self):
        """
        Check if the maximum number of learning rate adjustment is reached.
        Returns:
            [bool]: whether it is reached
        """
        return self.adjust_times >= self.lr_decay_max_times

class PredefinedLearningRates:
    """
    Wrapper of criteria objects.
    Adjust learning rate at predefined n-th epochs.
    """
    def __init__(self, criteria, scheduler, trigger_epochs):
        """
        Args:
            criteria [CriteriaBase/CriteriaBest]: criteria to be wrapped
            scheduler [Scheduler]: scheduler used to adjust learning rate
            trigger_epochs [list]: n-th epochs at which the adjustment should happen
        """
        self.criteria = criteria
        self.scheduler = scheduler.reset(scheduler.ext)
        self.trigger_epochs = trigger_epochs
        
    @no_check_epoch_zero
    def __call__(self, *args, **kwargs):
        """
        Check if the current status of training satisfies the stopping criteria.
        Args:
            args [list]: list of arguments for the wrapped criteria object
            kwargs [dict]: keyword arguments for the wrapped criteria object
        Returns:
            [bool]: whether the training should terminate
        """
        epoch = args[1]
        self.check_epoch(epoch)
        return self.criteria(*args, **kwargs)

    def update_callback(self, callback):
        """
        Update saving function.
        Args:
            callback [Callable]: saving function
        Returns:
            None
        """
        self.criteria.callback = callback

    def __str__(self):
        """
        String representation of this object using the reason of stopping.
        Returns:
            [str]: stopping reason
        """
        return str(self.criteria)

    def __getattr__(self, name):
        """
        When accessing final_saved_epoch property, delegate to the wrapped criteria object.
        Args:
            name [str]: name of the attribute (API of the magic method: __getattr__)
        Returns:
            [int]: final saved epoch
        """
        if name == 'final_saved_epoch':
            return self.criteria.final_saved_epoch
        return super().__getattribute__(name)
    
    def check_epoch(self, epoch):
        """
        Check if the current epoch number is in self.trigger_epochs and adjust learning rate if so.
        Args:
            epoch [int]: current epoch number
        Returns:
            None
        """
        if epoch in self.trigger_epochs:
            self.scheduler.adjust(epoch)
            self.trigger_epochs.remove(epoch)

    def profile(self):
        """
        Construct a dictionary describing the settings.
        Returns:
            [dict]: settings
        """
        prof = self.criteria.profile()
        prof["name"] += " Scheduled on " + str(self.trigger_epochs)
        return prof

def make_criteria(strategy='best', force_with_acc=False, use_adapt=False, scheduler=None, trigger_epochs=None, **kwargs):
    """
    Construct criteria object accordingly to the requested settings.
    Args:
        strategy [str]: stopping criteria strategy (base or best)
        force_with_acc [bool]: whether to use WithAcc even no accuracy threshold is provided
        use_adapt [bool]: whether to use AdaptiveWithAcc
        scheduler [Scheduler]: scheduler object
        trigger_epochs [list]: predefined n-th epochs for learning rate adjustment
        kwargs [dict]: keyword arguments for the criteria object
    Returns:
        [(criteria-related types)]: criteria object
    """
    CONVERGE_ACC = None
    if 'CONVERGE_ACC' in kwargs:
        CONVERGE_ACC = kwargs['CONVERGE_ACC']
        del kwargs['CONVERGE_ACC']

    if strategy == 'best':
        criteria = CriteriaBest(**kwargs)
    elif strategy == 'base':
        criteria = CriteriaBase(**kwargs)
    else:
        print('unknown strategy, use default strategy (best) instead.')
        criteria = CriteriaBest(**kwargs)

    if force_with_acc or CONVERGE_ACC is not None:
        if use_adapt:
            criteria = AdaptiveWithAcc(criteria, CONVERGE_ACC=CONVERGE_ACC, scheduler=scheduler)
        else:
            criteria = WithAcc(criteria, CONVERGE_ACC=CONVERGE_ACC)

    if trigger_epochs is not None:
        if scheduler is None:
            raise ValueError("PredefinedLearningRates requires scheduler")
        criteria = PredefinedLearningRates(criteria, scheduler, trigger_epochs)

    return criteria

def is_with_acc(criteria):
    """
    Check if the criteria object is wrapped by WithAcc.
    Args:
        criteria [(criteria-related types)]: criteria object
    Returns:
        [bool]: whether it is wrapped
    """
    return issubclass(type(criteria), WithAcc)