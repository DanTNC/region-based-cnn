import torch
import torch.nn as nn
import torch.nn.functional as F
from models.MAT import t_s_l2_norm
from torch.autograd import Variable

from utils.training_helper import to_1_hot


class AnchorLoss(torch.nn.Module):
    """
    Implementation of the anchor loss.
    Reference: https://arxiv.org/pdf/1909.11155.pdf
    """
    def __init__(self, gamma, safe_mode=False):
        """
        Args:
            gamma [float]: parameter of the anchor loss
            safe_mode [bool]: whether to add a small number to avoid divide-by-zero error
        """
        super().__init__()
        self.gamma = gamma
        self.softmax_layer = torch.nn.Softmax(dim=1)
        self.epsilon = 1e-8 if safe_mode else 0

    def forward(self, logits, labels):
        """
        Loss computation. (Required for Pytorch Module)
        Args:
            logits [torch.Tensor]: model output logits
            labels [torch.Tensor]: labels of the inputs
        Returns:
            [torch.Tensor]: loss
        """
        logits = self.softmax_layer(logits)
        if len(labels.shape) == 1:
            labels = to_1_hot(labels, logits.shape[1])
        q_star = (logits * labels).sum(axis=1, keepdim=True) + self.epsilon
        loss = -(labels * torch.log(logits + self.epsilon) + (1 - labels) * ((1 + logits - q_star) ** self.gamma) * torch.log(1 - logits + self.epsilon))
        loss = loss.sum(axis=1).mean()
        return loss

def squared_l2_norm(x):
    """
    Compute the square of the l2-norm of a tensor (flatten as a 1-D vector)
    Args:
        x [torch.Tensor]: tensor to compute
    Returns:
        [torch.Tensor]: square of the l2-norm
    """
    return (x ** 2).sum(dim=-1).sum(dim=-1).sum(dim=-1)

def l2_norm(x):
    """
    Compute the l2-norm of a tensor (flatten as a 1-D vector)
    Args:
        x [torch.Tensor]: tensor to compute
    Returns:
        [torch.Tensor]: l2-norm
    """
    return squared_l2_norm(x).sqrt()

def trades_loss(model,
                x_natural,
                y,
                optimizer,
                step_size=0.003,
                epsilon=0.031,
                perturb_steps=10,
                beta=1.0,
                distance='l_inf',
                loss_func=F.cross_entropy,
                proxy=None,
                return_adv=False):
    """
    Compute the TRADES loss.
    Reference: https://github.com/yaodongyu/TRADES
    Args:
        model [torch.nn.Module]: model to compute loss
        x_natural [torch.Tensor]: clean images
        y [torch.Tensor]: class labels
        optimizer [torch.optim.optimizer.Optimizer]: optimizer
        step_size [float]: step size of PGD optimization
        epsilon [float]: perturbation strength
        perturb_steps [int]: number of steps for PGD optimization
        beta [float]: weight of the second term in the TRADES loss
        distance [str]: distance type
        loss_func [Callable]: loss function of the natural classification
        proxy [Callable]: proxy function to wrap the model inference
        e.g. proxy = lambda model, x: model(x) + x.mean()
        return_adv [bool]: whether to also return the adversarial images
    Returns:
        If return_adv is True:
            [torch.Tensor], [torch.Tensor]: loss value, adversarial images
        Else:
            [torch.Tensor]: loss value
    """
    # define KL-loss
    criterion_kl = nn.KLDivLoss(reduction='sum')
    model.eval()
    batch_size = len(x_natural)
    # generate adversarial example
    if proxy is None:
        proxy = lambda model, x: model(x)
    x_adv = x_natural.detach() + 0.001 * torch.randn(x_natural.shape).cuda().detach()
    if distance == 'l_inf':
        for _ in range(perturb_steps):
            x_adv.requires_grad_()
            with torch.enable_grad():
                loss_kl = criterion_kl(F.log_softmax(proxy(model, x_adv), dim=1),
                                        F.softmax(proxy(model, x_natural), dim=1))
            grad = torch.autograd.grad(loss_kl, [x_adv])[0]
            x_adv = x_adv.detach() + step_size * torch.sign(grad.detach())
            x_adv = torch.min(torch.max(x_adv, x_natural - epsilon), x_natural + epsilon)
            x_adv = torch.clamp(x_adv, 0.0, 1.0)
    elif distance == 'l_2':
        for _ in range(perturb_steps):
            x_adv.requires_grad_()
            with torch.enable_grad():
                loss_kl = criterion_kl(F.log_softmax(proxy(model, x_adv), dim=1),
                                        F.softmax(proxy(model, x_natural), dim=1))
            grad = torch.autograd.grad(loss_kl, [x_adv])[0]
            for idx_batch in range(batch_size):
                grad_idx = grad[idx_batch]
                grad_idx_norm = l2_norm(grad_idx)
                grad_idx /= (grad_idx_norm + 1e-8)
                x_adv[idx_batch] = x_adv[idx_batch].detach() + step_size * grad_idx
                eta_x_adv = x_adv[idx_batch] - x_natural[idx_batch]
                norm_eta = l2_norm(eta_x_adv)
                if norm_eta > epsilon:
                    eta_x_adv = eta_x_adv * epsilon / l2_norm(eta_x_adv)
                x_adv[idx_batch] = x_natural[idx_batch] + eta_x_adv
            x_adv = torch.clamp(x_adv, 0.0, 1.0)
    else:
        x_adv = torch.clamp(x_adv, 0.0, 1.0)
    model.train()

    x_adv = Variable(torch.clamp(x_adv, 0.0, 1.0), requires_grad=False)
    # zero gradient
    optimizer.zero_grad()
    # calculate robust loss
    logits = proxy(model, x_natural)
    loss_natural = loss_func(logits, y)
    loss_robust = (1.0 / batch_size) * criterion_kl(F.log_softmax(proxy(model, x_adv), dim=1),
                                                    F.softmax(proxy(model, x_natural), dim=1))
    loss = loss_natural + beta * loss_robust
    if return_adv:
        return loss, x_adv
    else:
        return loss

def bibo_loss(model,
                x_natural,
                y,
                optimizer,
                step_size=0.003,
                epsilon=0.031,
                perturb_steps=10,
                beta=1.0,
                distance='l_inf',
                loss_func=F.cross_entropy):
    """
    Compute the BIBO loss.
    Args:
        model [torch.nn.Module]: model to compute loss
        x_natural [torch.Tensor]: clean images
        y [torch.Tensor]: class labels
        optimizer [torch.optim.optimizer.Optimizer]: optimizer
        step_size [float]: step size of PGD optimization
        epsilon [float]: perturbation strength
        perturb_steps [int]: number of steps for PGD optimization
        beta [float]: weight of the BIBO loss
        distance [str]: distance type
        loss_func [Callable]: loss function of the natural classification
    Returns:
        [torch.Tensor]: loss value
    """
    dis_loss = nn.CosineEmbeddingLoss(reduction='sum')
    criterion = lambda a, b: dis_loss(a, b, torch.ones(a.shape[0]).to(a.device))
    model.eval()
    batch_size = len(x_natural)
    # generate adversarial example
    x_adv = x_natural.detach() + 0.001 * torch.randn(x_natural.shape).cuda().detach()
    if distance == 'l_inf':
        for _ in range(perturb_steps):
            x_adv.requires_grad_()
            with torch.enable_grad():
                loss_mse = criterion(model(x_adv), model(x_natural))
            grad = torch.autograd.grad(loss_mse, [x_adv])[0]
            x_adv = x_adv.detach() + step_size * torch.sign(grad.detach())
            x_adv = torch.min(torch.max(x_adv, x_natural - epsilon), x_natural + epsilon)
            x_adv = torch.clamp(x_adv, 0.0, 1.0)
    elif distance == 'l_2':
        for _ in range(perturb_steps):
            x_adv.requires_grad_()
            with torch.enable_grad():
                loss_mse = criterion(model(x_adv), model(x_natural))
            grad = torch.autograd.grad(loss_mse, [x_adv])[0]
            grad_norm = l2_norm(grad)
            grad /= (grad_norm + 1e-8).unsqueeze(0)
            x_adv = x_adv.detach() + step_size * grad
            eta_x_adv = x_adv - x_natural
            norm_eta = l2_norm(eta_x_adv)
            too_large = norm_eta > epsilon
            eta_x_adv[too_large] = eta_x_adv[too_large] * epsilon / norm_eta[too_large]
            x_adv = x_natural + eta_x_adv
            x_adv = torch.clamp(x_adv, 0.0, 1.0)
    else:
        x_adv = torch.clamp(x_adv, 0.0, 1.0)
    model.train()

    x_adv = Variable(x_adv, requires_grad=False)
    # zero gradient
    optimizer.zero_grad()
    # calculate robust loss
    logits = model(x_natural)
    loss_natural = loss_func(logits, y)
    loss_robust = (1.0 / batch_size) * criterion(model(x_adv), model(x_natural))
    loss = loss_natural + beta * loss_robust
    return loss

def AT_loss(model,
            x_natural,
            y,
            optimizer,
            step_size=0.003,
            epsilon=0.031,
            perturb_steps=10,
            distance='l_inf',
            loss_func=F.cross_entropy,
            criterion=nn.CrossEntropyLoss(reduction='sum')):
    """
    Compute the adversarial training loss.
    Args:
        model [torch.nn.Module]: model to compute loss
        x_natural [torch.Tensor]: clean images
        y [torch.Tensor]: class labels
        optimizer [torch.optim.optimizer.Optimizer]: optimizer
        step_size [float]: step size of PGD optimization
        epsilon [float]: perturbation strength
        perturb_steps [int]: number of steps for PGD optimization
        distance [str]: distance type
        loss_func [Callable]: loss function of the natural classification
        criterion [Callable]: loss function for the PGD optimization
    Returns:
        [torch.Tensor]: loss value
    """
    model.eval()
    batch_size = len(x_natural)
    # generate adversarial example
    x_adv = x_natural.detach() + 0.001 * torch.randn(x_natural.shape).cuda().detach()
    for _ in range(perturb_steps):
        x_adv.requires_grad_()
        with torch.enable_grad():
            loss = criterion(model(x_adv), y)
        grad = torch.autograd.grad(loss, [x_adv])[0]
        x_adv = x_adv.detach() + step_size * torch.sign(grad.detach())
        x_adv = torch.min(torch.max(x_adv, x_natural - epsilon), x_natural + epsilon)
        x_adv = torch.clamp(x_adv, 0.0, 1.0)
    x_adv = Variable(x_adv, requires_grad=False)
    # zero gradient
    optimizer.zero_grad()
    return loss_func(model(x_adv), y)
