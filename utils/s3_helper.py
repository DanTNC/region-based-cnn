import json
import tempfile

import boto3
from botocore.exceptions import ClientError

secrets = json.load(open("secret.json", 'r'))

def download_from_s3(root, filename):
    """
    Download a file from S3.
    Args:
        root [str]: root directory
        filename [str]: file name
    Returns:
        [tempfile.NamedTemporaryFile]: downloaded file
    """
    session = boto3.session.Session(
        aws_access_key_id=secrets['ACCESS_KEY'],
        aws_secret_access_key=secrets['SECRET_KEY']
    )
    s3 = session.client('s3', endpoint_url='https://s3.twcc.ai')

    tmp = tempfile.NamedTemporaryFile()

    with open(tmp.name, 'wb') as f:
        try:
            s3.download_fileobj('daniel', '{}/{}'.format(root, filename), f)
        except ClientError as e:
            raise ValueError(e.response['Error']['Code'])
        
    return tmp

def upload_to_s3(root, filename, saver):
    """
    Upload a file to S3.
    Args:
        root [str]: root directory
        filename [str]: file name
        saver [Callable]: function to save the file using given file name
    Returns:
        None
    """
    session = boto3.session.Session(
        aws_access_key_id=secrets['ACCESS_KEY'],
        aws_secret_access_key=secrets['SECRET_KEY']
    )
    s3 = session.client('s3', endpoint_url='https://s3.twcc.ai')

    tmp = tempfile.NamedTemporaryFile()

    saver(tmp.name)

    s3.upload_file(tmp.name, 'daniel', '{}/{}'.format(root, filename))
