import numpy as np
import torch

from utils.function_helper import broadcastable


def image_to_save(im):
    """
    Prepare an image for saving.
    Args:
        im [numpy.array]: image
    """
    if im.dtype == bool:
        return np.where(im, 255, 0).astype("uint8")
    else:
        return im.astype("uint8")

@broadcastable
def cast2tensor(x):
    """
    Cast an object to a tensor.
    Args:
        x [any]: input object
    Returns:
        [torch.Tensor]: casted tensor
    Raises:
        TypeError: when the input type is not recognized
    """
    if isinstance(x, np.ndarray):
        return torch.from_numpy(x)
    if isinstance(x, (list, tuple)):
        return torch.Tensor(x)
    if isinstance(x, torch.Tensor):
        return x
    raise TypeError("Invalid input type")

@broadcastable
def cast2numpy(x):
    """
    Cast an object to a numpy array.
    Args:
        x [any]: input object
    Returns:
        [numpy.array]: casted numpy array
    Raises:
        TypeError: when the input type is not recognized
    """
    if isinstance(x, np.ndarray):
        return x
    if isinstance(x, (list, tuple)):
        return np.array(x)
    if isinstance(x, torch.Tensor):
        return x.cpu().detach().numpy()
    raise TypeError("Invalid input type")

def channel_order(img, l2f=True):
    """
    Change the place of the channel dimension in all dimensions.
    Args:
        img [numpy.array]: image
        l2f [bool]: whether to switch from channel-last to channel-first
    Returns:
        [numpy.array]: modified image
    """
    if l2f:
        if len(img.shape) == 4:
            dims = (0, 3, 1, 2)
        else:
            dims = (2, 0, 1)
        return np.transpose(img, dims)
    else:
        if len(img.shape) == 4:
            dims = (0, 2, 3, 1)
        else:
            dims = (1, 2, 0)
        return np.transpose(img, dims)
    
def add_channel(imgs, single=False, first=False):
    """
    Add an additional dimension to the channel dimension.
    Args:
        imgs [numpy.array]: input image(s)
        single [bool]: whether the input is a single image
        first [bool]: whether the image array is channel-first
    Returns:
        [numpy.array]: modified image
    """
    if single and len(imgs.shape) == 2:
        if first:
            return imgs[None, ...].astype("float32")
        else:
            return imgs[..., None].astype("float32")
    if not single and len(imgs.shape) == 3:
        if first:
            return imgs[:, None, ...].astype("float32")
        else:
            return imgs[:, ..., None].astype("float32")
    return imgs.astype("float32")

def lp_norm(vector, p, axis=None, keepdims=False):
    """
    Calculate the lp-norm of a numpy array.
    Args:
        vector [numpy.array]: array to calculate lp-norm
        p [int/float]: p
        axis [int]: axis to calculate lp-norm
        keepdims [bool]: whether to keep the dimensions
    Returns:
        [numpy.array]: lp-norm
    """
    p = float(p)
    if p == 0:
        if keepdims:
            return np.expand_dims(np.count_nonzero(vector, axis=axis), axis)
        else:
            return np.count_nonzero(vector, axis=axis)
    elif p == 1:
        return np.abs(vector).sum(axis=axis, keepdims=keepdims)
    elif p == 2:
        return ((vector ** 2).sum(axis=axis, keepdims=keepdims)) ** 0.5
    elif p == float('inf'):
        return vector.max(axis=axis, keepdims=keepdims)

def l0_norm(vector, **kwargs):
    """
    Calculate the l0-norm of a numpy array.
    Args:
        vector [numpy.array]: array to calculate l0-norm
        kwargs [dict]: keyword arguments for the lp_norm function
    Returns:
        [numpy.array]: l0-norm
    """
    return lp_norm(vector, 0, **kwargs)

def l1_norm(vector, **kwargs):
    """
    Calculate the l1-norm of a numpy array.
    Args:
        vector [numpy.array]: array to calculate l1-norm
        kwargs [dict]: keyword arguments for the lp_norm function
    Returns:
        [numpy.array]: l1-norm
    """
    return lp_norm(vector, 1, **kwargs)

def l2_norm(vector, **kwargs):
    """
    Calculate the l2-norm of a numpy array.
    Args:
        vector [numpy.array]: array to calculate l2-norm
        kwargs [dict]: keyword arguments for the lp_norm function
    Returns:
        [numpy.array]: l2-norm
    """
    return lp_norm(vector, 2, **kwargs)

def linf_norm(vector, **kwargs):
    """
    Calculate the linf-norm of a numpy array.
    Args:
        vector [numpy.array]: array to calculate linf-norm
        kwargs [dict]: keyword arguments for the lp_norm function
    Returns:
        [numpy.array]: linf-norm
    """
    return lp_norm(vector, 'inf', **kwargs)

def cosine(vA, vB):
    """
    Calculate the cosine similarity between two vectors.
    Args:
        vA, vB [numpy.array]: vectors
    Returns:
        [numpy.array]: cosine similarity
    """
    assert vA.shape[-1] == vB.shape[-1]
    return np.sum(vA * vB, axis=-1) / (l2_norm(vA, axis=-1) * l2_norm(vB, axis=-1))

def kernel_distance(vector, variances):
    """
    Compute the kernel distance between a vector and a distribution.
    Args:
        vector [numpy.array]: difference of the vector and the center of the distribution
        Note: argument @vector is not the vector but the difference
        variances [numpy.array]: variance of the distribution(s)
    Returns:
        [numpy.array]: distances
    """
    # {f, 0}, {bxf, 0}, {bxf, b}, {bxcxf, 0}, {bxcxf, c} => {0}, {b}, {b}, {bxc}, {bxc}
    return 1 - np.exp(-l2_norm(vector, axis=-1)**2 / 2 / variances)

def aniso_kernel_distance(vector, variances):
    """
    Compute the anisotropic kernel distance between a vector and a distribution.
    Args:
        vector [numpy.array]: difference of the vector and the center of the distribution
        Note: argument @vector is not the vector but the difference
        variances [numpy.array]: variance of the distribution(s)
    Returns:
        [numpy.array]: distances
    """
    # {f, f}, {bxf, bxf}, {bxcxf, cxf} => {0}, {b}, {bxc}
    return 1 - np.exp(-(vector**2) / 2 / variances).prod(axis=-1)

def Mahalanobis_distances(vector, cov, remove_outlier=False):
    """
    Compute the Mahalanobis distance between a vector and a distribution.
    Args:
        vector [numpy.array]: difference of the vector and the center of the distribution
        Note: argument @vector is not the vector but the difference
        cov [numpy.array]: feature covariance of the distribution(s)
        remove_outlier [bool]: whether to set invalid distances to infinity
    Returns:
        [numpy.array]: distances
    """
    # {f, fxf}, {bxf, fxf}, {bxcxf, fxf} => {0}, {b}, {bxc}
    vector_bar = vector[..., None] # {fx1}, {bxfx1}, {bxcxfx1}
    vector_bar_t = np.einsum('...kl->...lk', vector_bar) # {1xf}, {bx1xf}, {bxcx1xf}
    distances = np.einsum('...ij,jk,...kl->...il', vector_bar_t, cov, vector_bar) # {1x1}, {bx1x1}, {bxcx1x1}
    distances = distances[..., 0, 0] # {0}, {b}, {bxc}
    if remove_outlier:
        distances[distances < 0] = float('inf')
    return distances

def Mahalanobis_distances_no_share(vector, cov, remove_outlier=False):
    """
    Compute the Mahalanobis distance between a vector and a distribution.
    Note: the covariance matrix is not shared accross distributions.
    Args:
        vector [numpy.array]: difference of the vector and the center of the distribution
        Note: argument @vector is not the vector but the difference
        cov [numpy.array]: feature covariance of the distribution(s)
        remove_outlier [bool]: whether to set invalid distances to infinity
    Returns:
        [numpy.array]: distances
    """
    # {f, fxf}, {bxf, bxfxf}, {bxcxf, cxfxf} => {0}, {b}, {bxc}
    vector_bar = vector[..., None] # {fx1}, {bxfx1}, {bxcxfx1}
    vector_bar_t = np.einsum('...kl->...lk', vector_bar) # {1xf}, {bx1xf}, {bxcx1xf}
    if len(cov.shape) == 2:
        distances = np.einsum('...ij,jk,...kl->...il', vector_bar_t, cov, vector_bar) # {1x1}
    else:
        distances = np.einsum('...aij,ajk,...akl->...ail', vector_bar_t, cov, vector_bar) # {bx1x1}, {bxcx1x1}
    distances = distances[..., 0, 0] # {0}, {b}, {bxc}
    if remove_outlier:
        distances[distances < 0] = float('inf')
    return distances

def t_lp_norm(vector, p, dim=[], keepdim=False):
    """
    Calculate the lp-norm of a tensor.
    Args:
        vector [torch.Tensor]: array to calculate lp-norm
        p [int/float]: p
        dim [int/list]: axis to calculate lp-norm
        keepdim [bool]: whether to keep the dimensions
    Returns:
        [torch.Tensor]: lp-norm
    """
    p = float(p)
    if p == 1:
        return torch.abs(vector).sum(dim=dim, keepdim=keepdim)
    elif p == 2:
        return ((vector ** 2).sum(dim=dim, keepdim=keepdim)) ** 0.5
    elif p == float('inf'):
        return vector.max(dim=dim, keepdim=keepdim)[0]

def t_l1_norm(vector, **kwargs):
    """
    Calculate the l1-norm of a tensor.
    Args:
        vector [torch.Tensor]: array to calculate l1-norm
        kwargs [dict]: keyword arguments for the t_lp_norm function
    Returns:
        [torch.Tensor]: l1-norm
    """
    return t_lp_norm(vector, 1, **kwargs)

def t_l2_norm(vector, **kwargs):
    """
    Calculate the l2-norm of a tensor.
    Args:
        vector [torch.Tensor]: array to calculate l2-norm
        kwargs [dict]: keyword arguments for the t_lp_norm function
    Returns:
        [torch.Tensor]: l2-norm
    """
    return t_lp_norm(vector, 2, **kwargs)

def t_linf_norm(vector, **kwargs):
    """
    Calculate the linf-norm of a tensor.
    Args:
        vector [torch.Tensor]: array to calculate v-norm
        kwargs [dict]: keyword arguments for the t_lp_norm function
    Returns:
        [torch.Tensor]: linf-norm
    """
    return t_lp_norm(vector, 'inf', **kwargs)

def t_s_l2_norm(vector, dim=[], keepdim=False):
    """
    Calculate the square of the l2-norm of a tensor.
    Args:
        vector [torch.Tensor]: array to calculate square of the l2-norm
        p [int/float]: p
        dim [int/list]: axis to calculate square of the l2-norm
        keepdim [bool]: whether to keep the dimensions
    Returns:
        [torch.Tensor]: square of the l2-norm
    """
    return ((vector ** 2).sum(dim=dim, keepdim=keepdim))

def t_cosine(vA, vB):
    """
    Calculate the cosine similarity between two vectors.
    Args:
        vA, vB [torch.Tensor]: vectors
    Returns:
        [torch.Tensor]: cosine similarity
    """
    assert vA.shape[-1] == vB.shape[-1]
    res = torch.sum(vA * vB, dim=-1) / (t_l2_norm(vA, dim=-1) * t_l2_norm(vB, dim=-1))
    res[torch.isnan(res)] = 0
    return res

def t_kernel_distance(vector, variances):
    """
    Compute the kernel distance between a vector and a distribution.
    Args:
        vector [torch.Tensor]: difference of the vector and the center of the distribution
        Note: argument @vector is not the vector but the difference
        variances [torch.Tensor]: variance of the distribution(s)
    Returns:
        [torch.Tensor]: distances
    """
    # {f, 0}, {bxf, 0}, {bxf, b}, {bxcxf, 0}, {bxcxf, c} => {0}, {b}, {b}, {bxc}, {bxc}
    return 1 - torch.exp(-t_l2_norm(vector, dim=-1)**2 / 2 / variances)

def t_aniso_kernel_distance(vector, variances):
    """
    Compute the anisotropic kernel distance between a vector and a distribution.
    Args:
        vector [torch.Tensor]: difference of the vector and the center of the distribution
        Note: argument @vector is not the vector but the difference
        variances [torch.Tensor]: variance of the distribution(s)
    Returns:
        [torch.Tensor]: distances
    """
    # {f, f}, {bxf, bxf}, {bxcxf, cxf} => {0}, {b}, {bxc}
    return 1 - torch.exp(-(vector**2) / 2 / variances).prod(dim=-1)

def t_Mahalanobis_distances(vector, cov, remove_outlier=False):
    """
    Compute the Mahalanobis distance between a vector and a distribution.
    Args:
        vector [torch.Tensor]: difference of the vector and the center of the distribution
        Note: argument @vector is not the vector but the difference
        cov [torch.Tensor]: feature covariance of the distribution(s)
        remove_outlier [bool]: whether to set invalid distances to infinity
    Returns:
        [torch.Tensor]: distances
    """
    # {f, fxf}, {bxf, fxf}, {bxcxf, fxf} => {0}, {b}, {bxc}
    vector_bar = vector.unsqueeze(-1) # {fx1}, {bxfx1}, {bxcxfx1}
    vector_bar_t = torch.einsum('...kl->...lk', vector_bar) # {1xf}, {bx1xf}, {bxcx1xf}
    distances = torch.einsum('...ij,jk,...kl->...il', vector_bar_t, cov, vector_bar) # {1x1}, {bx1x1}, {bxcx1x1}
    distances = distances[..., 0, 0] # {0}, {b}, {bxc}
    if remove_outlier:
        distances[distances < 0] = float('inf')
    return distances

def t_Mahalanobis_distances_no_share(vector, cov, remove_outlier=False):
    """
    Compute the Mahalanobis distance between a vector and a distribution.
    Note: the covariance matrix is not shared accross distributions.
    Args:
        vector [torch.Tensor]: difference of the vector and the center of the distribution
        Note: argument @vector is not the vector but the difference
        cov [torch.Tensor]: feature covariance of the distribution(s)
        remove_outlier [bool]: whether to set invalid distances to infinity
    Returns:
        [torch.Tensor]: distances
    """
    # {f, fxf}, {bxf, bxfxf}, {bxcxf, cxfxf} => {0}, {b}, {bxc}
    vector_bar = vector.unsqueeze(-1) # {fx1}, {bxfx1}, {bxcxfx1}
    vector_bar_t = torch.einsum('...kl->...lk', vector_bar) # {1xf}, {bx1xf}, {bxcx1xf}
    if len(cov.shape) == 2:
        distances = torch.einsum('...ij,jk,...kl->...il', vector_bar_t, cov, vector_bar) # {1x1}
    else:
        distances = torch.einsum('...aij,ajk,...akl->...ail', vector_bar_t, cov, vector_bar) # {bx1x1}, {bxcx1x1}
    distances = distances[..., 0, 0] # {0}, {b}, {bxc}
    if remove_outlier:
        distances[distances < 0] = float('inf')
    return distances

def channel_mean(features):
    """
    Compute the mean of each feature channel.
    Args:
        features [torch.Tensor]: features
    """
    b, c = features.shape[:2]
    return features.view(b, c, -1).mean(dim=2)

class CountingBin:
    """
    Histogram constructor.
    """
    def __init__(self, thresholds):
        """
        Args:
            thresholds [list]: thresholds between bins
        """
        thresholds = list(sorted(thresholds))
        self.thresholds = thresholds
        self.bins = [0] * (len(thresholds) + 1)

    def add(self, x):
        """
        Put a value into the histogram.
        Args:
            x [int/float]: value to be added
        Returns:
            [int]: which bin the value is put into
        """
        for i, threshold in enumerate(self.thresholds):
            if x <= threshold:
                self.bins[i] += 1
                return i
        self.bins[-1] += 1
        return len(self.bins) - 1
    
    def results(self):
        """
        Return the counts.
        Returns:
            [list]: counts
        """
        return self.bins

    def percentages(self):
        """
        Return the counts in percentage.
        Returns:
            [list]: counts in percentage
        """
        total = sum(self.bins)
        return list(map(lambda x: x / total, self.bins))

    def clear(self):
        """
        Clear the histogram.
        Returns:
            None
        """
        self.bins = [0] * (len(self.thresholds) + 1)
