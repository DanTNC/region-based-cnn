import foolbox
import foolbox.attacks as Attacks
import mlflow
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from foolbox.criteria import TargetClass
from torch.autograd import Variable

from utils.data_helper import cast2numpy, cast2tensor
from utils.training_helper import to_1_hot


class AttacksBox:
    """
    Container for adversarial attacks (functions).
    """
    def __init__(self, fmodel=None):
        """
        Args:
            fmodel [foolbox.models.PyTorchModel]: foolbox wrapped model for conducting foolbox.attacks
        """
        self.fmodel = fmodel
        self.attacks = {}
        self.distances = {
            'L2': foolbox.distances.MSE,
            'L1': foolbox.distances.MAE,
            'L0': foolbox.distances.L0,
            'Linf': foolbox.distances.Linf
        }

    def add(self, name, attack_method, d, *args, overwrite=False, **kwargs):
        """
        Add new attack function and corresponding parameters into the container.
        Args:
            name [str]: name of the attack
            attack_method [Callable]: attack function
            d [str]: distance type (e.g. Linf, L2, ...)
            args [list]: list of arguments for the attack function
            overwrite [bool]: whether to overwrite existing attack function with the same name
            kwargs [dict]: keyword arguments for the attack function
        Returns:
            [AttacksBox]: this object
        """
        if not overwrite and name in self.attacks:
            raise KeyError("warning: this name is already used")
        self.attacks[name] = (attack_method, d, args, kwargs)
        return self

    def set_fmodel(self, fmodel):
        """
        Set (update) fmodel.
        Args:
            fmodel [foolbox.models.PyTorchModel]: foolbox wrapped model for conducting foolbox.attacks
        Returns:
            [AttacksBox]: this object
        """
        self.fmodel = fmodel
        return self

    def check_fmodel(self):
        """
        Check if fmodel is set.
        Raises:
            Exception: if fmodel is not set
        """
        if self.fmodel is None:
            raise Exception("fmodel should be set before conducting attacks")

    def attack(self, name, inputs, labels):
        """
        Conduct a certain attack of the given name.
        Args:
            name [str]: name of the attack
            inputs [numpy.array]: input images
            labels [numpy.array]: class labels
        Returns:
            [numpy.array], [numpy.array]: success flags, adversarial examples
        """
        self.check_fmodel()
        att, d, args, kwargs = self.attacks[name]
        with torch.enable_grad():
            adv = att(self.fmodel, distance=self.distances[d])(inputs, labels, *args, **kwargs)
        b_size = adv.shape[0]
        success = (~np.isnan(adv).reshape(b_size, -1).all(axis=1))
        return success, adv

    def attack_safe(self, name, inputs, labels):
        """
        Conduct a certain attack of the given name using torch.Tensor.
        Args:
            name [str]: name of the attack
            inputs [torch.Tensor]: input images
            labels [torch.Tensor]: class labels
        Returns:
            [torch.Tensor], [torch.Tensor]: success flags, adversarial examples
        """
        inputs, labels = cast2numpy(inputs), cast2numpy(labels)
        success, adv = self.attack(name, inputs, labels)
        return cast2tensor(success), cast2tensor(adv)

    def attack_targeted(self, name, inputs, labels, target):
        """
        Conduct a certain targeted attack of the given name.
        Args:
            name [str]: name of the attack
            inputs [numpy.array]: input images
            labels [numpy.array]: class labels
            target [int]: target class
        Returns:
            [numpy.array], [numpy.array]: success flags, adversarial examples
        """
        self.check_fmodel()
        att, d, args, kwargs = self.attacks[name]
        with torch.enable_grad():
            adv = att(self.fmodel, distance=self.distances[d], criterion=TargetClass(target))(inputs, labels, *args, **kwargs)
        b_size = adv.shape[0]
        success = (~np.isnan(adv).reshape(b_size, -1).all(axis=1))
        return success, adv

    def attack_safe_targeted(self, name, inputs, labels, target):
        """
        Conduct a certain targeted attack of the given name using torch.Tensor.
        Args:
            name [str]: name of the attack
            inputs [torch.Tensor]: input images
            labels [torch.Tensor]: class labels
            target [int]: target class
        Returns:
            [torch.Tensor], [torch.Tensor]: success flags, adversarial examples
        """
        inputs, labels = cast2numpy(inputs), cast2numpy(labels)
        success, adv = self.attack_targeted(name, inputs, labels, target)
        return cast2tensor(success), cast2tensor(adv)
    
    def attack_tensor(self, name, inputs, labels, pred_func=lambda x: x.max(1)[1]):
        """
        Conduct a certain attack (designed for torch.Tensor) of the given name using torch.Tensor.
        Args:
            name [str]: name of the attack
            inputs [torch.Tensor]: input images
            labels [torch.Tensor]: class labels
            pred_func [Callable]: predict function that maps the model output to prediction
        Returns:
            [torch.Tensor], [torch.Tensor]: success flags, adversarial examples
        """
        self.check_fmodel()
        att, d, args, kwargs = self.attacks[name]
        with torch.enable_grad():
            adv = att(self.fmodel, distance=self.distances[d])(inputs, labels, *args, **kwargs)
        success = pred_func(self.fmodel.forward(adv)) != labels
        return success, adv

    def attack_tensor_random(self, name, inputs, labels, pred_func=lambda x: x.max(1)[1], iterations=100):
        """
        Apply random noises (designed for torch.Tensor) using torch.Tensor.
        Args:
            name [str]: name of the attack
            inputs [torch.Tensor]: input images
            labels [torch.Tensor]: class labels
            pred_func [Callable]: predict function that maps the model output to prediction
            iterations [int]: number of iterations for each sample
        Returns:
            [torch.Tensor], [torch.Tensor]: success counts, adversarial examples
        """
        self.check_fmodel()
        att, d, args, kwargs = self.attacks[name]
        for i in range(iterations):
            with torch.enable_grad():
                adv = att(self.fmodel, distance=self.distances[d])(inputs, labels, *args, **kwargs)
            success = pred_func(self.fmodel.forward(adv)) != labels # b
            if i == 0:
                success_count = success.float() # b
            else:
                success_count += success.float() # b
        return success_count / iterations, adv

    def attack_all(self, inputs, labels):
        """
        Conduct all contained attacks.
        Args:
            inputs [numpy.array]: input images
            labels [numpy.array]: class labels
        Yields:
            [str], [numpy.array], [numpy.array]: attack name, success flags, adversarial examples
        """
        for key in self.attacks:
            yield key, self.attack(key, inputs, labels)

    def attack_all_targeted(self, inputs, labels, target):
        """
        Conduct all contained attacks (using targeted settings).
        Args:
            inputs [numpy.array]: input images
            labels [numpy.array]: class labels
            target [int]: target class
        Yields:
            [str], [numpy.array], [numpy.array]: attack name, success flags, adversarial examples
        """
        for key in self.attacks:
            yield key, self.attack_targeted(key, inputs, labels, target)

    def attack_all_safe(self, inputs, labels):
        """
        Conduct all contained attacks using torch.Tensor.
        Args:
            inputs [torch.Tensor]: input images
            labels [torch.Tensor]: class labels
        Yields:
            [str], [torch.Tensor], [torch.Tensor]: attack name, success flags, adversarial examples
        """
        for key in self.attacks:
            yield key, self.attack_safe(key, inputs, labels)

    def attack_all_safe_targeted(self, inputs, labels, target):
        """
        Conduct all contained attacks using torch.Tensor.
        Args:
            inputs [torch.Tensor]: input images
            labels [torch.Tensor]: class labels
            target [int]: target class
        Yields:
            [str], [torch.Tensor], [torch.Tensor]: attack name, success flags, adversarial examples
        """
        for key in self.attacks:
            yield key, self.attack_safe_targeted(key, inputs, labels, target)

    def keys(self):
        """
        Return all names of contained attacks.
        """
        return self.attacks.keys()

    def __iter__(self):
        """
        Return all names of contained attacks (as a generator).
        Usage:
            >>> attacks = AttacksBox().add('a', 'Linf', attack)
            >>> for name in attacks:
            ...     print(name)
            ...
            a
        """
        return iter(self.keys())

    def to_str(self, name):
        """
        Construct a string representation of a certain attack.
        Args:
            name [str]: name of the attack
        Returns:
            [str]: string representation
        """
        att, d, args, kwargs = self.attacks[name]
        return "{}${}$".format(name, d) + "&".join(str(arg) for arg in args) + "$" + "&".join("{}:{}".format(k, v) for k, v in kwargs.items())

    def all_to_str(self):
        """
        Construct a string representation of all attacks.
        Yields:
            [str], [str]: attack name, string representation
        """
        for name in self.attacks:
            yield name, self.to_str(name)

DEFAULT_ATTACKS = AttacksBox() \
    .add('L2PGD', Attacks.L2BasicIterativeAttack, 'L2', binary_search=False, epsilon=0.3) \
    .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.031)

def create_attacks(model, num_classes=10, requested_attacks=DEFAULT_ATTACKS, mlflow_track=True, pure_model=False):
    """
    Create an AttacksBox object containing requested attacks and a foolbox wrapped model.
    Args:
        model [torch.nn.Module]: model to be attacked
        num_classes [int]: number of classes
        requested_attacks [AttacksBox]: requested attacks
        mlflow_track [bool]: whether to log attacks using mlflow
        pure_model [bool]: whether to use Pytorch model directly instead of foolbox wrapped model
    Returns:
        [foolbox.models.PyTorchModel], [AttacksBox]: foolbox wrapped model, requested attacks
    """
    try:
        model = model.module
    except AttributeError:
        pass
    if pure_model:
        fmodel = model
    else:
        fmodel = foolbox.models.PyTorchModel(model, bounds=(0, 1), num_classes=num_classes)
    attacks = requested_attacks.set_fmodel(fmodel)
    if mlflow_track:
        mlflow.log_param('attacks', ' & '.join(attacks.keys()))
        mlflow.log_param('attack_params', ';'.join(s for _, s in attacks.all_to_str()))
    
    return fmodel, attacks

class Adapt_BIM:
    """
    Adaptive BIM attack.
    """
    def __init__(self, fmodel, distance):
        """
        Args:
            fmodel [foolbox.models.PyTorchModel]: foolbox wrapped model
            distance [foolbox.distances.Distance]: distance type
        """
        self.fmodel = fmodel

    def __call__(self, images, labels, loss_func, binary_search=True, epsilon=0.3, stepsize=0.05, iterations=10, random_start=False, return_early=True, ast_func=None):
        """
        Perform the attack.
        Args:
            images [torch.Tensor]: images to be attacked
            labels [torch.Tensor]: class labels of @images
            loss_func [Callable]: loss function to be optimized (input: adversarial image, label)
            binary_search [bool]: !!!NOT USED!!! only to match the API of foolbox.attacks.LinfBIM
            epsilon [float]: perturbation strength
            stepsize [float]: step size for iterative optimization
            iterations [int]: number of iterations for optimization
            random_start [bool]: whether to start with a random perturbation
            return_early [bool]: !!!NOT USED!!! only to match the API of foolbox.attacks.LinfBIM
            ast_func [Callable]: assitant loss function (input: clean image, adversarial image)
        """
        if random_start:
            x_adv = images.detach() + 0.001 * torch.randn(images.shape).cuda().detach()
        else:
            x_adv = images.detach()
        for _ in range(iterations):
            x_adv.requires_grad_()
            with torch.enable_grad():
                loss = loss_func(self.fmodel.forward(x_adv), labels)
                if ast_func is not None:
                    loss += ast_func(images, x_adv)
            grad = torch.autograd.grad(loss, [x_adv])[0]
            x_adv = x_adv.detach() + stepsize * torch.sign(grad.detach())
            x_adv = torch.min(torch.max(x_adv, images - epsilon), images + epsilon)
            x_adv = torch.clamp(x_adv, 0.0, 1.0)
        return x_adv

class Projected_Attack:
    """
    Base class providing a function for projecting a perturbation onto a Lp-norm ball.
    """
    def project(self, images, noise, epsilon):
        """
        Project a perturbation onto a Lp-norm ball.
        Args:
            images [torch.Tensor]: clean images
            noise [torch.Tensor]: perturbation
            epsilon [float]: perturbation strength limit
        Returns:
            [torch.Tensor]: projected perturbation
        """
        batch_size = images.shape[0]
        if self.distance == foolbox.distances.MSE:
            noise_norm = noise.view(batch_size, -1).norm(2, dim=-1).view(batch_size, 1, 1, 1)
            noise = noise / noise_norm * epsilon
        elif self.distance == foolbox.distances.Linf:
            noise = torch.min(torch.max(noise, torch.full_like(noise, -epsilon)), torch.full_like(noise, epsilon))
        else:
            raise ValueError("Unknown distance")
        return noise
        
class Adapt_DDN:
    """
    Adaptive DDN attack.
    Reference: https://github.com/jeromerony/fast_adversarial/blob/master/fast_adv/attacks/ddn.py
    DDN attack: decoupling the direction and norm of the perturbation to achieve a small L2-norm in few steps.
    """

    def __init__(self, fmodel, distance):
        """
        Args:
            fmodel [foolbox.models.PyTorchModel]: foolbox wrapped model
            distance [foolbox.distances.Distance]: distance type
        """
        self.fmodel = fmodel

    def __call__(self, images, labels, loss_func, targeted = False,
                steps = 100,
                gamma = 0.05,
                init_norm = 1.,
                quantize = True,
                levels = 256,
                max_norm = None,
                pred_func = lambda x: x.max(1)[1],
                ast_func = None):
        """
        Perform the attack.
        Args:
            model [torch.nn.Module]: model to attack.
            inputs [torch.Tensor]: batch of samples to attack. Values should be in the [0, 1] range.
            labels [torch.Tensor]: labels of the samples to attack if untargeted, else labels of targets.
            targeted [bool]: whether to perform a targeted attack or not.
            steps [int]: number of steps for the optimization.
            gamma [float]: factor by which the norm will be modified. new_norm = norm * (1 + or - gamma).
            init_norm [float]: initial value for the norm.
            quantize [bool]: if True, the returned adversarials will have quantized values to the specified number of levels.
            levels [int]: number of levels to use for quantization (e.g. 256 for 8 bit images).
            max_norm [float/None]: if specified, the norms of the perturbations will not be greater than this value which might lower success rate.
        Returns:
            [torch.Tensor]: batch of samples modified to be adversarial to the model.
        """

        if images.min() < 0 or images.max() > 1: raise ValueError('Input values should be in the [0, 1] range.')

        batch_size = images.shape[0]
        multiplier = 1 if targeted else -1
        delta = torch.zeros_like(images, requires_grad=True)
        norm = torch.full((batch_size,), init_norm, device=images.device, dtype=torch.float)
        worst_norm = torch.max(images, 1 - images).view(batch_size, -1).norm(p=2, dim=1)

        # Setup optimizers
        optimizer = optim.SGD([delta], lr=1)
        scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=steps, eta_min=0.01)

        best_l2 = worst_norm.clone()
        best_delta = torch.zeros_like(images)
        adv_found = torch.zeros(images.size(0), dtype=torch.uint8, device=images.device)

        for i in range(steps):
            l2 = delta.data.view(batch_size, -1).norm(p=2, dim=1)
            adv = images + delta
            outputs = self.fmodel.forward(adv)
            pred_labels = pred_func(outputs)
            apt_loss = loss_func(outputs, labels) * batch_size
            loss = multiplier * apt_loss
            if ast_func is not None:
                loss += ast_func(images, adv)

            is_adv = (pred_labels == labels) if targeted else (pred_labels != labels)
            is_smaller = l2 < best_l2
            is_both = is_adv * is_smaller
            adv_found[is_both] = 1
            best_l2[is_both] = l2[is_both]
            best_delta[is_both] = delta.data[is_both]

            optimizer.zero_grad()
            loss.backward()
            # renorming gradient
            grad_norms = delta.grad.view(batch_size, -1).norm(p=2, dim=1)
            delta.grad.div_(grad_norms.view(-1, 1, 1, 1))
            # avoid nan or inf if gradient is 0
            if (grad_norms == 0).any():
                delta.grad[grad_norms == 0] = torch.randn_like(delta.grad[grad_norms == 0])

            optimizer.step()
            scheduler.step()

            norm.mul_(1 - (2 * is_adv.float() - 1) * gamma)
            norm = torch.min(norm, worst_norm)

            delta.data.mul_((norm / delta.data.view(batch_size, -1).norm(2, 1)).view(-1, 1, 1, 1))
            delta.data.add_(images)
            if quantize:
                delta.data.mul_(levels - 1).round_().div_(levels - 1)
            delta.data.clamp_(0, 1).sub_(images)

        if max_norm:
            best_delta.renorm_(p=2, dim=0, maxnorm=max_norm)
            if quantize:
                best_delta.mul_(levels - 1).round_().div_(levels - 1)

        return images + best_delta

class GaussianNoise(Projected_Attack):
    """
    Random Gaussian noise.
    """
    def __init__(self, fmodel, distance):
        """
        Args:
            fmodel [foolbox.models.PyTorchModel]: foolbox wrapped model
            distance [foolbox.distances.Distance]: distance type
        """
        self.fmodel = fmodel
        self.distance = distance

    def __call__(self, images, labels, epsilon=0.3):
        """
        Apply Gaussian noise.
        Args:
            images [torch.Tensor]: images to be added noise
            labels [torch.Tensor]: labels of the images
            epsilon [float]: perturbation strength
        Returns:
            [torch.Tensor]: perturbed images
        """
        noise = torch.randn_like(images)
        noise = self.project(images, noise, epsilon)
        x_adv = images + noise
        x_adv = torch.clamp(x_adv, 0.0, 1.0)
        return x_adv

class SPSA(Projected_Attack):
    """
    Implementation of the SPSA attack.
    PyTorch version of https://github.com/tensorflow/cleverhans/blob/4b5ce5421ced09e8531b112f97468869980884f2/cleverhans/attacks/spsa.py#L375 (Tensorflow)
    """
    def __init__(self, fmodel, distance):
        """
        Args:
            fmodel [foolbox.models.PyTorchModel]: foolbox wrapped model
            distance [foolbox.distances.Distance]: distance type
        """
        self.fmodel = fmodel
        self.distance = distance

    def get_delta(self, images, delta, spsa_samples):
        """
        Construct the random vectors to approximate the gradient.
        Args:
            images [torch.Tensor]: clean images
            delta [float]: norm of the random vectors
        Returns:
            [torch.Tensor]: random vectors
        """
        shape = [images.shape[0], spsa_samples] + list(images.shape[1:])
        return delta * torch.sign(torch.rand(shape) * 2 -1).to(images.device)

    def get_loss(self, x, y_star, spsa_samples):
        """
        Compute the loss to be optimized.
        Args:
            x [torch.Tensor]: clean images
            y_star [torch.Tensor]: class labels
            spsa_samples [int]: number of samples for SPSA optimization
        """
        y_star = y_star.unsqueeze(1).repeat(1, spsa_samples * 2).view(x.shape[0])
        y = self.fmodel.forward(x) # bxcls
        y_mask = to_1_hot(y_star, y.shape[1])
        y_of_label = (y * y_mask).sum(dim=1) # b
        y[y_mask == 1] = -float("inf")
        y_of_max_non_label = y.max(1)[0] # b
        return y_of_label - y_of_max_non_label

    def __call__(self, images, labels, epsilon=0.3, iterations=10, lr=0.01, delta=0.01, spsa_samples=16, spsa_iters=8):
        """
        Perform the attack.
        Args:
            images [torch.Tensor]: clean images
            labels [torch.Tensor]: class labels
            epsilon [float]: perturbation strength
            iterations [int]: number of iterations for the optimization
            lr [float]: learning rate
            delta [float]: norm of random vectors used in gradient approximation
            spsa_samples [int]: number of samples for the SPSA optimization
            spsa_iters [int]: number of iterations for gradient approximation in each optimization iteration
        Returns:
            [torch.Tensor]: adversarial images
        """
        with torch.no_grad():
            spsa_samples = spsa_samples // 2 # spsa_samples must be an even number
            x_adv = images.detach() + (2 * epsilon * torch.randn(images.shape) - epsilon).to(images.device).detach()
            x_0 = images.detach()
            x_adv = self.project(images, x_adv - x_0, epsilon) + x_0
            x_adv = torch.clamp(x_adv, 0.0, 1.0)
            batch_size = images.shape[0]
            for ii in range(iterations):
                all_grads = []
                for i in range(spsa_iters):
                    delta_x = self.get_delta(images, delta, spsa_samples) # bx0.5sxcxhxw
                    delta_x = torch.cat([delta_x, -delta_x], dim=1) # bxsxcxhxw
                    x_adv_ = x_adv.unsqueeze(1) # bx1xcxhxw
                    delta_images = (x_adv_ + delta_x).view(batch_size * spsa_samples * 2, *list(images.shape[1:])) # bsxcxhxw
                    loss = self.get_loss(delta_images, labels, spsa_samples).view(batch_size, 2 * spsa_samples, 1, 1, 1) # bxsx1x1x1
                    avg_grad = ((loss * delta_x).mean(dim=1) / delta) # bxcxhxw
                    all_grads.append(avg_grad.unsqueeze(1)) # bx1xcxhxw
                avg_grad = torch.cat(all_grads, dim=1).mean(dim=1) # bxcxhxw
                x_adv = x_adv + lr * avg_grad
                x_adv = self.project(images, x_adv - x_0, epsilon) + x_0
                x_adv = torch.clamp(x_adv, 0.0, 1.0)
        return x_adv
