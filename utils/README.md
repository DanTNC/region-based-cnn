# commonly-used functions across all python scripts

## Introduction of codebase

### Modules
* **criteria.py**  
classes used for controlling training process \(deciding when to terminate training and when to save model weights\)

Classes:  

| Name | Description |
| -------- | -------- |
| CriteriaBase  | save model for each certain interval, stop when max epoch exceeds  |
| CriteriaBest  | save model when lower validation loss is reached, stop when after some number of epochs no better model is found  |
| WithAcc  | a wrapper for Criteria objects, keep training when the target accuracy is not reached  |


Helper functions:

| Name | Description |
| -------- | -------- |
| make_criteria  | create criteria object according to given parameters  |
| is_with_acc  | return if a criteria is wrapped by WithAcc  |

Usage:
```python
converge = CriteriaBest(CONVERGE_EPOCHS=20, MAX_EPOCHS=100)
converge = WithAcc(criteria, CONVERGE_ACC=0.85)
# or
converge = make_criteria(strategy='best', 
            CONVERGE_EPOCHS=20, MAX_EPOCHS=100,
            CONVERGE_ACC=0.85)

print(converge.profile()) # show criteria settings
loss_val = None
acc = INIT_ACC
epoch = 0
while not converge(loss_val, epoch, acc=acc):
    # training
    converge.update_callback(<NEW_CALLBACK>)
    # update loss_val, epoch, and acc
print(converge) # show termination reason
```

More examples are in ```train_CNN.py``` and ```utils/training_helper.py:training_loop```

* **s3_helper.py**  
helper functions for uploading or downloading files to or from S3  

* **model_manager.py**  
helper functions for saving or loading model weights to or from S3  

* **mlflow_helper.py**  
helper functions for setting mlflow backend uri  

* **loss.py**  
1. PyTorch implementation of AnchorLoss [paper](https://arxiv.org/abs/1909.11155)
2. code from TRADES released repo containing PyTorch version of trade loss [GitHub](https://github.com/yaodongyu/TRADES)  
3. BIBO loss
4. adversarial training loss

* **adversarial.py**  
helper functions for ```foolbox==2.3.0``` and custom adversarial attacks  

* **data_helper.py**
helper functions for convering data format or doing data-related math  

* **function_helper.py**  
helper functions for wrapping functions or classes  

* **torch_helper.py**  
helper functions for torch-related common settup  

* **training_helper.py**  
helper functions for CNN training  

* **cluster_helper.py**  
helper functions for cluster predictors  

* **path_helper.py**  
helper functions for constructing paths  

* **performance.py**  
helper functions for examining code performance  