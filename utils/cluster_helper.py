import numpy as np
import torch
from sklearn.covariance import OAS, EmpiricalCovariance
from sklearn.neighbors import NearestNeighbors

from utils.data_helper import (Mahalanobis_distances,
                               Mahalanobis_distances_no_share,
                               aniso_kernel_distance, cast2numpy, cast2tensor,
                               kernel_distance, l2_norm,
                               t_aniso_kernel_distance, t_kernel_distance,
                               t_l2_norm, t_Mahalanobis_distances,
                               t_Mahalanobis_distances_no_share)

DIST_MAPPING = {
    "numpy": {
        "l2": lambda c, v, remove_outlier=False: l2_norm(c, axis=-1),
        "kd": lambda c, v, remove_outlier=False: kernel_distance(c, v),
        "akd": lambda c, v, remove_outlier=False: aniso_kernel_distance(c, v),
        "Md": Mahalanobis_distances,
        "CLWMd": Mahalanobis_distances,
        "Mdns": Mahalanobis_distances_no_share
    },
    "torch": {
        "l2": lambda c, v, remove_outlier=False: t_l2_norm(c, dim=-1),
        "kd": lambda c, v, remove_outlier=False: t_kernel_distance(c, v),
        "akd": lambda c, v, remove_outlier=False: t_aniso_kernel_distance(c, v),
        "Md": t_Mahalanobis_distances,
        "CLWMd": t_Mahalanobis_distances,
        "Mdns": t_Mahalanobis_distances_no_share
    }
}

class PseudoClusters:
    """
    Pseudo clustering object immitating the behavior of sklearn clustering objects, and use classes directly as clusters.
    """
    def __init__(self, labels_, num_classes=10):
        """
        Args:
            labels_ [numpy.array]: class labels
            num_classes [int]: number of classes
        """
        self.labels_ = labels_
        self.num_classes = num_classes

    def get_mapping(self):
        """
        Construct the mapping vector describing class counts in each cluster. 
        Note: the mapping vector is for computing dominant classes, therefore the class counts are set to 1
            instead of the true number of members in each class since the dominant classes are the same.
        Returns:
            [dict]: mapping vector
        """
        mapping = {}
        for i in range(self.num_classes):
            mapping[i] = [0] * self.num_classes
            mapping[i][i] = 1
        return mapping

class ClusterPredictor:
    """
    Cluster predictor that finds the closest cluster to an input sample and use the dominant class of the cluster as the prediction.
    """
    def __init__(self, clusters, features, preds, mapping, dist="l2", remove_outlier=True):
        """
        Args:
            clusters [sklearn.base.ClusterMixin]: sklean clustering result
            features [numpy.array]: features of the training data
            preds [numpy.array]: model predictions of the training data
            mapping [dict]: mapping vector
            dist [str]: distance type
            remove_outlier [bool]: whether to ignore outliers when computing centers
        """
        self.dist_func = DIST_MAPPING['numpy'][dist]
        new_mapping = {}
        for cls, feature, pred in zip(clusters.labels_, features, preds):
            if cls not in new_mapping:
                new_mapping[cls] = {'dom_class': np.argmax(mapping[cls]), 'center': [], 'mask': []}
            new_mapping[cls]['center'].append(feature)
            new_mapping[cls]['mask'].append(pred)
        cluster_centers = []
        var_related_values = []
        dom_classes = []
        cls_ids = []

        Cov = EmpiricalCovariance(assume_centered=False)

        for cls, m in new_mapping.items():
            cls_features = np.vstack(m['center'])
            cls_preds = np.array(m['mask'])
            if remove_outlier:
                cls_features = cls_features[cls_preds == m['dom_class']]
            center = cls_features.mean(axis=0)

            if dist == "kd":
                var = cls_features.var()
                var_related_values.append(var)
            elif dist == "akd":
                var = cls_features.var(axis=0)
                var_related_values.append(var)
            elif dist in ["Md", "Mdns", "CLWMd"]:
                if dist == "Mdns":
                    cls_features_bar = (cls_features - center) # bxf
                    Cov.fit(cls_features_bar)
                    var_related_values.append(Cov.precision_[None, ...]) # 1xfxf
                else:
                    var_related_values.append((cls_features - center)) # bxf

            cluster_centers.append(center)
            dom_classes.append(m['dom_class'])
            cls_ids.append(cls)

        if dist == "kd":
            self.var_related_values = np.array(var_related_values) # c
        elif dist == "akd":
            self.var_related_values = np.vstack(var_related_values) # cxf
        elif dist == "Md" or dist == "CLWMd":
            var_related_values = np.concatenate(var_related_values, axis=0) # nxf
            Cov.fit(var_related_values)
            self.var_related_values = Cov.precision_ # fxf
        elif dist == "Mdns":
            self.var_related_values = np.concatenate(var_related_values, axis=0) # cxfxf
        else:
            self.var_related_values = np.array(0) # placeholder
        
        self.cluster_centers = np.vstack(cluster_centers)
        self.dom_classes = np.array(dom_classes)
        self.cls_ids = np.array(cls_ids)

    def get_distances(self, features):
        """
        Compute distance of input features to each cluster.
        Args:
            features [numpy.array]: input features
        Returns:
            [numpy.array]: distances
        """
        differences = features[:, None, :] - self.cluster_centers # {bx1xf - clrxf = bxclrxf}
        distances = self.dist_func(differences, self.var_related_values, remove_outlier=True) # {bxclr}
        return distances

    def _predict(self, features):
        """
        Find closet clusters to each sample feature.
        Args:
            features [numpy.array]: input features
        Returns:
            [numpy.array], [numpy.array], [numpy.array]: closest cluster indices, min distances, distances to each cluster
        """
        # eval distance
        distances = self.get_distances(features)

        # find nearest cluster
        indices = np.argmin(distances, axis=1) # {b} nearest cluster indices
        min_ds = np.take_along_axis(distances, indices[..., None], axis=1).flatten() # {b} distance to nearest cluster
        return indices, min_ds, distances

    def get_logits(self, features):
        """
        Compute logits by:
            logits = exp(-dist)
        Args:
            features [numpy.array]: input features
        Returns:
            [numpy.array]: logits
        """
        distances = self.get_distances(features)
        NUM_CLASSES = self.dom_classes.max() + 1
        logits = np.zeros((features.shape[0], NUM_CLASSES))
        for c in range(NUM_CLASSES):
            d_c = distances[:, np.where(self.dom_classes == c)[0]] # {bx|d=c|}
            min_d_c = d_c.min(axis=1) # {b}
            logits[:, c] = np.exp(-min_d_c)
        return logits

    def predict(self, features):
        """
        Find the closest cluster to each sample feature.
        Args:
            features [numpy.array]: input features
        Returns:
            [numpy.array], [numpy.array]: closest cluster ids, min distances
        """
        indices, min_ds, distances = self._predict(features)
        cluster_ids = self.cls_ids[indices] # {b} nearest cluster ids
        return cluster_ids, min_ds

    def predict_and_match(self, features, labels=None):
        """
        Make predictions using dominant class of the closest cluster to each sample feature.
        Note: when labels is given, the search of closest cluster only considers those with the same dominant classes as the class labels.
        Args:
            features [numpy.array]: input features
            labels [numpy.array/None]: labels of the features
        Returns:
            [numpy.array], [numpy.array]: matched classes, min distances
        """
        indices, min_ds, distances = self._predict(features)
        match_classes = self.dom_classes[indices] # {b} nearest cluster's dominant classes

        if labels is not None:
            # find smallest distance to a cluster whose class = label
            mask = self.dom_classes != labels.reshape(-1, 1) # {clr == bx1 = bxclr}
            distances[mask] = float('inf') # {bxclr} make sure clusters with wrong class won't be found by argmin
            indices = np.argmin(distances, axis=1) # {b}
            min_ds = np.take_along_axis(distances, indices[..., None], axis=1).flatten() # {b}

        return match_classes, min_ds

class TorchClusterPredictor(ClusterPredictor):
    """
    Cluster predictor that finds the closest cluster to an input sample and use the dominant class of the cluster as the prediction.
    Using torch.Tensor.
    """
    def __init__(self, clusters, features, preds, mapping, dist='l2', remove_outlier=True):
        """
        Args:
            clusters [sklearn.base.ClusterMixin]: sklean clustering result
            features [torch.Tensor]: features of the training data
            preds [torch.Tensor]: model predictions of the training data
            mapping [dict]: mapping vector
            dist [str]: distance type
            remove_outlier [bool]: whether to ignore outliers when computing centers
        """
        features, preds = cast2numpy(features), cast2numpy(preds)
        super().__init__(clusters, features, preds, mapping, dist=dist, remove_outlier=remove_outlier)
        self.dist_func = DIST_MAPPING['torch'][dist]
        self.var_related_values = cast2tensor(self.var_related_values).double()
        self.cluster_centers = cast2tensor(self.cluster_centers).double()
        self.dom_classes = cast2tensor(self.dom_classes).double()
        self.cls_ids = cast2tensor(self.cls_ids).double()

    def to(self, device):
        """
        Move member data (torch.Tensor) to a certain device.
        Args:
            device [torch.device]: device to move data to
        Returns:
            [TorchClusterPredictor]: this object
        """
        self.var_related_values = self.var_related_values.to(device)
        self.cluster_centers = self.cluster_centers.to(device)
        self.dom_classes = self.dom_classes.to(device)
        self.cls_ids = self.cls_ids.to(device)
        return self

    def get_distances(self, features):
        """
        Compute distance of input features to each cluster.
        Args:
            features [torch.Tensor]: input features
        Returns:
            [torch.Tensor]: distances
        """
        differences = features.unsqueeze(1) - self.cluster_centers # {bx1xf - clrxf = bxclrxf}
        distances = self.dist_func(differences, self.var_related_values, remove_outlier=True) # {bxclr}
        return distances

    def _predict(self, features):
        """
        Find closet clusters to each sample feature.
        Args:
            features [torch.Tensor]: input features
        Returns:
            [torch.Tensor], [torch.Tensor], [torch.Tensor]: closest cluster indices, min distances, distances to each cluster
        """
        # eval distance
        distances = self.get_distances(features) # {bxclr}

        # find nearest cluster
        indices = distances.min(1)[1] # {b} nearest cluster indices
        min_ds = distances[range(distances.shape[0]), indices] # {b} distance to nearest cluster
        return indices, min_ds, distances

    def get_logits(self, features):
        """
        Compute logits by:
            logits = exp(-dist)
        Args:
            features [torch.Tensor]: input features
        Returns:
            [torch.Tensor]: logits
        """
        distances = self.get_distances(features) # {bxclr}
        NUM_CLASSES = self.dom_classes.max() + 1
        logits = torch.zeros((features.shape[0], NUM_CLASSES))
        for c in range(NUM_CLASSES):
            d_c = distances[:, torch.where(self.dom_classes == c)[0]] # {bx|d=c|}
            min_d_c = d_c.min(1)[0] # {b}
            logits[:, c] = torch.exp(-min_d_c)
        return logits

    def predict(self, features):
        """
        Find the closest cluster to each sample feature.
        Args:
            features [torch.Tensor]: input features
        Returns:
            [torch.Tensor], [torch.Tensor]: closest cluster ids, min distances
        """
        indices, min_ds, distances = self._predict(features)
        cluster_ids = self.cls_ids[indices] # {b} nearest cluster ids
        return cluster_ids, min_ds

    def predict_centers(self, features, labels=None):
        """
        Return the center of the closest cluster to each sample feature.
        Note: when labels is given, the search of closest cluster only considers those with the same dominant classes as the class labels.
        Args:
            features [torch.Tensor]: input features
            labels [torch.Tensor/None]: labels of the features
        Returns:
            [torch.Tensor], [torch.Tensor], [torch.Tensor]: cluster centers, min distances, closest cluster indices
        """
        indices, min_ds, distances = self._predict(features)
        cluster_centers = self.cluster_centers[indices] # {bxf} nearest cluster centers

        if labels is not None:
            # find smallest distance to a cluster whose class = label
            mask = self.dom_classes != labels.view(-1, 1) # {clr == bx1 = bxclr}
            distances[mask] = float('inf') # {bxclr} make sure clusters with wrong class won't be found by argmin
            indices = distances.min(1)[1] # {b}
            min_ds = distances[range(distances.shape[0]), indices] # {b} distance to nearest cluster
            cluster_centers = self.cluster_centers[indices] # {bxf} nearest cluster centers

        return cluster_centers, min_ds, indices

    def predict_and_match(self, features, labels=None):
        """
        Make predictions using dominant class of the closest cluster to each sample feature.
        Note: when labels is given, the search of closest cluster only consider those with the same dominant classes as the class labels.
        Args:
            features [torch.Tensor]: input features
            labels [torch.Tensor/None]: labels of the features
        Returns:
            [torch.Tensor], [torch.Tensor]: matched classes, min distances
        """
        indices, min_ds, distances = self._predict(features)
        match_classes = self.dom_classes[indices] # {b} nearest cluster's dominant classes

        if labels is not None:
            mask = self.dom_classes != labels.view(-1, 1) # {clr == bx1 = bxclr}
            distances[mask] = torch.inf # {bxclr} make sure clusters with wrong class won't be found by argmin
            indices = indices = distances.min(1)[1] # {b}
            min_ds = distances[range(distances.shape[0]), indices] # {b} distance to nearest cluster

        return match_classes, min_ds

class KNNPredictor:
    """
    KNN predictor that finds the major class around an input sample and use that class as the prediction.
    """
    def __init__(self, clusters, features, preds, mapping, dist=None, remove_outlier=False, k=20):
        """
        Args:
            clusters [sklearn.base.ClusterMixin]: !!! NOT USED !!! only to match the API of ClusterPredictor
            features [numpy.array]: features of the training data
            preds [numpy.array]: model predictions of the training data
            mapping [dict]: !!! NOT USED !!! only to match the API of ClusterPredictor
            dist [str]: !!! NOT USED !!! only to match the API of ClusterPredictor
            remove_outlier [bool]: !!! NOT USED !!! only to match the API of ClusterPredictor
            k [int]: number of neighbors to be considered when computing KNN
        """
        self.KNN = NearestNeighbors(n_neighbors=k).fit(features)
        self.preds = preds

    def predict(self, features):
        """
        Find the major class around the input sample features.
        Use the following value as the distance to the major class:
                Count(Major Class) - Count(Second Major Class)
            1 - ----------------------------------------------
                            Count(Major Class)
        Args:
            features [numpy.array]: input features
        Returns:
            [numpy.array], [numpy.array]: major classes, Single Dominance distances
        """
        knns = self.KNN.kneighbors(features, return_distance=False) # {bxk}
        knn_preds = self.preds[knns] # {bxk}
        class_counts = np.apply_along_axis(lambda x: np.bincount(x, minlength=10), axis=-1, arr=knn_preds) # {bxcls} 
        knn_classes = class_counts.argmax(axis=-1) # {b}
        M_pos = (knn_preds == knn_classes[..., None]) # {bxk}
        M_counts = M_pos.sum(axis=-1) # {b}
        np.put_along_axis(class_counts, knn_classes[..., None], -1, axis=1)
        knn_2nd_classes = class_counts.argmax(axis=-1) # {b}
        m_counts = (knn_preds == knn_2nd_classes[..., None]).sum(axis=-1) # {b}
        SD_dist = 1 - (M_counts - m_counts) / (M_counts) # {b}
        return knn_classes, SD_dist

    def predict_and_match(self, features):
        """
        Same as self.predict
        """
        return self.predict(features)
