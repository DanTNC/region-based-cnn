import os

def make_path(model_name, tag, *args, root=".", file_ext=".pt"):
    """
    Construct a path based on the given parameters.
    Args:
        model_name [str]: model name
        tag [str]: tag
        args [list]: additional information
        root [str]: root directory
        file_ext [str]: file extension
    Returns:
        [str]: constructed path
    """
    return "{}/{}_{}_".format(root, model_name, tag) + "_".join(args) + file_ext

class PathWRoot:
    """
    Path maker with default root.
    """
    def __init__(self, root):
        """
        Args:
            root [str]: default root
        """
        self.root = root

    def make_path(self, model_name, tag, *args, file_ext=".pt"):
        """
        Construct path with the default root.
        Args:
            model_name [str]: model name
            tag [str]: tag
            args [list]: additional information
            file_ext [str]: file extension
        Returns:
            [str]: constructed path
        """
        return make_path(model_name, tag, *args, root=self.root, file_ext=file_ext)

class PathWDefaults(PathWRoot):
    """
    Path maker with default root and file extension.
    """
    def __init__(self, root, file_ext):
        """
        Args:
            root [str]: default root
            file_ext [str]: default file extension
        """
        super().__init__(root)
        self.file_ext = file_ext

    def make_path(self, model_name, tag, *args):
        """
        Construct path with the default root.
        Args:
            model_name [str]: model name
            tag [str]: tag
            args [list]: additional information
        Returns:
            [str]: constructed path
        """
        return super().make_path(model_name, tag, *args, file_ext=self.file_ext)

def path_w_split(path_maker, model_name, tag, observe_testing, *args):
    """
    Use a certain path maker to construct a path with split information.
    Args:
        path_maker [PathWRoot/PathWDefaults]: path maker
        model_name [str]: model name
        tag [str]: tag
        observe_testing [bool]: whether the split is test
        args [list]: additional information
    Returns:
        [str]: constructed path
    """
    split = "test" if observe_testing else "train"
    return path_maker.make_path(model_name, tag, split, *args)

# aliases
output_path = path_w_split
joblib_path = path_w_split

def path_exists(path_maker, model_name, tag, observe_testing, args_list=[[]]):
    """
    Check if some files exist.
    Args:
        path_maker [PathWRoot/PathWDefaults]: path maker
        model_name [str]: model name
        tag [str]: tag
        observe_testing [bool]: whether the split is test
        args_list [list]: list of additional information for different paths
    Returns:
        [bool]: whether all the paths exist
    """
    res = True
    for args in args_list:
        res = res and os.path.isfile(path_w_split(path_maker, model_name, tag, observe_testing, *args))
    return res

def output_exists(path_maker, model_name, tag, observe_testing, args_list=[['features'], ['preds'], ['labels']]):
    """
    Check if the output files of classic inference exist.
    Args:
        path_maker [PathWRoot/PathWDefaults]: path maker
        model_name [str]: model name
        tag [str]: tag
        observe_testing [bool]: whether the split is test
        args_list [list]: list of additional information for different paths
    Returns:
        [bool]: whether all the paths exist
    """
    return path_exists(path_maker, model_name, tag, observe_testing, args_list=args_list)

def joblib_exists(path_maker, model_name, tag, observe_testing):
    """
    Check if the output files of clustering exist.
    Args:
        path_maker [PathWRoot/PathWDefaults]: path maker
        model_name [str]: model name
        tag [str]: tag
        observe_testing [bool]: whether the split is test
    Returns:
        [bool]: whether all the paths exist
    """
    return path_exists(path_maker, model_name, tag, observe_testing)