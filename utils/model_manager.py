import datetime

import torch

from utils.s3_helper import download_from_s3, json, upload_to_s3

"""
version_control.json = {
    <model_type>: {
        <datetime>: <tag>,
        ...
    },
    ...
}
"""

def get_version_controller():
    """
    Download the version controller from S3.
    Returns:
        [dict]: version controller
    """
    tmp = download_from_s3('models', 'version_control.json')
    version_control = json.load(open(tmp.name, 'r'))
    return version_control

def update_version_controller(version_control):
    """
    Update the version controller on S3.
    Args:
        version_control [dict]: version controller
    Returns:
        None
    """
    saver = lambda name: json.dump(version_control, open(name, 'w'))
    upload_to_s3('models', 'version_control.json', saver)

def find_latest(model_name):
    """
    Find the latest tag of a given model name.
    Args:
        model_name [str]: model name
    Returns:
        [str]: latest tag
    """
    version_control = get_version_controller()
    final_timestamp = max(map(float, version_control[model_name].keys()))
    return version_control[model_name][str(final_timestamp)]

def load_checkpoint(model_name, tag='latest', return_tag=False):
    """
    Load checkpoint of a certain tag for a given model name.
    Args:
        model_name [str]: model name
        tag [str]: specified tag
        return_tag [bool]: whether to also return the final inferred tag
    Returns:
        If return_tag is True:
            [collections.OrderedDict], [str]: loaded checkpoint, inferred tag
        Else:
            [collections.OrderedDict]: loaded checkpoint
    """
    if tag == 'latest':
        tag = find_latest(model_name)
    try:
        tmp = download_from_s3('models', '{}-{}.pt'.format(model_name, tag))
    except ValueError as e:
        if str(e) == '404':
            raise ValueError("The tag is not available for this model name.")
    return (torch.load(tmp.name), tag) if return_tag else torch.load(tmp.name)

def save_checkpoint(model_name, state_dict, tag='latest'):
    """
    Save checkpoint using a specified tag for a given model name.
    Args:
        model_name [str]: model name
        state_dict [collections.OrderedDict]: checkpoint to be saved
        tag [str]: specified tag
    Returns:
        [str]: inferred tag
    """
    now = str(datetime.datetime.now().timestamp())
    if tag == 'latest':
        tag = now
    version_control = get_version_controller()
    if model_name not in version_control:
        version_control[model_name] = {}
    if now in version_control[model_name]:
        raise ValueError('fatal: current time is already in version controller')
    while tag in version_control[model_name].values():
        tag += "_r"
    version_control[model_name][now] = tag
    update_version_controller(version_control)
    upload_to_s3('models', '{}-{}.pt'.format(model_name, tag), lambda name: torch.save(state_dict, name))
    return tag
