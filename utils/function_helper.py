from skimage.io import imsave
from typing import Callable

class Cascade:
    """
    Create a sequential function.
    Useful when conducting image morphological operations.
    """
    def __init__(self, intermediate_prefix=None):
        """
        Args:
            intermediate_prefix [str]: prefix to use when saving intermediate results.
        """
        self.funcs = []
        self.save_intermediate = intermediate_prefix is not None
        self.intermediate_prefix = intermediate_prefix

    def append(self, func, *args, **kwargs):
        """
        Append a function into the sequence with arguments (additional to first argument).
        Args:
            func [Callable/str]: function to append
            Note: if @func is a string, call a member method of the input instead of calling @func with the input as its first argument.
            args [list]: list of arguments for @func
            kwargs [dict]: keyword parameters for @func
        Returns:
            [Cascade]: this object
        """
        self.funcs.append((func, args, kwargs))
        return self

    def __call__(self, x):
        """
        Conduct sequence of functions on the input @x.
        Args:
            x [any]: input
        Returns:
            [any]: output of the last function
        """
        if self.save_intermediate:
            i = 0
        for func, args, kwargs in self.funcs:
            if isinstance(func, str):
                x = getattr(x, func)(*args, **kwargs)
            elif isinstance(func, Callable):
                x = func(x, *args, **kwargs)
            if self.save_intermediate:
                try: 
                    imsave("{}_{}.png".format(self.intermediate_prefix, i), image_to_save(x))
                    i += 1
                except Exception as e:
                    print(str(e))
        return x

def broadcastable(func):
    """
    Function decorator to make a function able to broadcast for multiple inputs.
    Usage:
        @broadcastable
        def add_k(x, k=1):
            return x + k

        >>> add_k(2)
        3
        >>> add_k(3, 4, 2, k=2)
        [5, 6, 4]
    """
    def func_(*args, **kwargs):
        if len(args) > 1:
            return [func(arg, **kwargs) for arg in args]
        else:
            return func(*args, **kwargs)
    return func_

class Delegator:
    """
    A object wrapper that delegates certain methods to inner objects.
    Modified from the code on https://www.michaelcho.me/article/method-delegation-in-python.
    Usage:
        class Gun:
            def shoot(self):
                print("Boom!!!")

        class Human(Delegator):
            DELEGATED_METHODS = {
                "my_gun": ["shoot"]
            }
            def __init__(self):
                self.my_gun = Gun()

            def shout(self):
                print("Stop!!!")

        >>> human = Human()
        >>> human.shout()
        Stop!!!
        >>> human.shoot()
        Boom!!!
    """
    EXCEPTION_NAMES = ['DELEGATED_METHODS']

    def __getattr__(self, called_method):
        """
        Delegate to inner object if the called method is in the delegation dictionary.
        Note: this method is only called when the called method is not a member of the current object.
        """
        if called_method in self.EXCEPTION_NAMES:
            return object.__getattr__(self, called_method)
        delegation_config = getattr(self, 'DELEGATED_METHODS', None)
        if not isinstance(delegation_config, dict):
            self.__raise_exception(called_method)
            
        for delegate_object_str, delegated_methods in delegation_config.items():
            if called_method in delegated_methods:
                break
        else:
            self.__raise_exception(called_method)

        def wrapper(*args, **kwargs):
            delegate_object = getattr(self, delegate_object_str, None)

            return getattr(delegate_object, called_method)(*args, **kwargs)

        return wrapper

    def __raise_exception(self, called_method):
        """
        Custom error.
        """
        raise AttributeError("'%s' object has no attribute '%s'" % (self.__class__.__name__, called_method))

class DelegatorWDefault(Delegator):
    EXCEPTION_NAMES = ['DELEGATED_METHODS', 'DEFAULT_DELEGATE_TO']

    def __getattr__(self, called_method):
        """
        Delegate to inner object if the called method is in the delegation dictionary.
        Otherwise, delegate to the inner object which is set as default.
        Note: this method is only called when the called method is not a member of the current object.
        """
        if called_method in self.EXCEPTION_NAMES:
            return object.__getattr__(self, called_method)
        delegation_config = getattr(self, 'DELEGATED_METHODS', None)
        if not isinstance(delegation_config, dict):
            return self.__default_delegation(called_method)
            
        for delegate_object_str, delegated_methods in delegation_config.items():
            if called_method in delegated_methods:
                break
        else:
            return self.__default_delegation(called_method)

        def wrapper(*args, **kwargs):
            delegate_object = getattr(self, delegate_object_str, None)

            return getattr(delegate_object, called_method)(*args, **kwargs)

        return wrapper

    def __default_delegation(self, called_method):
        """
        Delegate to the inner object which is set as default.
        """
        default_delegation_config = getattr(self, 'DEFAULT_DELEGATE_TO', None)
        if not isinstance(default_delegation_config, str):
            self.__raise_exception(called_method)
        
        def wrapper(*args, **kwargs):
            delegate_object = getattr(self, default_delegation_config, None)

            return getattr(delegate_object, called_method)(*args, **kwargs)

        return wrapper