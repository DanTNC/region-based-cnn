import argparse

import torch

import models
from configs.arg_helper import (add_cnn_config_args, model_according_to,
                                model_path_by)
from configs.cnn_config import MODEL_DATASET_MAPPING
from models.MAT import MAT
from utils import loss as custom_losses
from utils.model_manager import save_checkpoint

parser = argparse.ArgumentParser(description='Checkpoint Uploader')
add_cnn_config_args(parser)
parser.add_argument('-c', '--checkpoint', type=str, required=True,
                    help='path of the checkpoint to upload')
parser.add_argument('-m', '--mat', default=False, action='store_true',
                    help='if it is MAT model')

args = parser.parse_args()

assert args.tag != "latest", "Please set a custom tag (cannot be 'latest') via --tag command-line argument"

model_type = model_according_to(args.model, args.dataset)
dataset = MODEL_DATASET_MAPPING[model_type]
model_name = model_path_by(dataset, args.natural, mat=args.mat)

checkpoint = torch.load(args.checkpoint)

save_checkpoint(model_name, checkpoint, tag=args.tag)
