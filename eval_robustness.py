import matplotlib.pyplot as plt
import mlflow
import numpy as np
import torch
from skimage.io import imsave
from sklearn.manifold import TSNE

import models
from configs.arg_helper import (add_cnn_config_args, model_according_to,
                                model_path_by)
from configs.attack_config import (ATTACK_CONFIGS, ATTACK_MULTIPLE_CONFIGS,
                                   get_adapt_configs)
from configs.cnn_config import load_config_into
from configs.constant import NUM_CLASSES
from data.dataset import get_data_loaders
from models.MAT import MAT
from utils import loss as custom_losses
from utils.adversarial import AttacksBox, GaussianNoise, create_attacks
from utils.data_helper import (cast2numpy, channel_order, image_to_save,
                               t_cosine, t_l2_norm, t_lp_norm)
from utils.mlflow_helper import get_tracking_uri
from utils.model_manager import load_checkpoint
from utils.path_helper import PathWRoot
from utils.torch_helper import (auto_device, auto_no_final_parallel,
                                auto_parallel)
from utils.training_helper import score

TSNE_path_maker = PathWRoot('MATSNE')

NUM_CLASSES_ = 10
ATTACKS = ATTACK_CONFIGS['TRADES']
MULTI_ATTACKS = ATTACK_MULTIPLE_CONFIGS['TRADES']

def eval_robustness(model, feature_model, test_loader, dataset, device, mat=False, multi=False):
    """
    Evaluate robust accuracy of a model.
    Args:
        model [torch.nn.Module]: model to be evaluated
        feature_model [torch.nn.Module]: model that outputs features (e.g. no-final-layer version of traditional CNNs)
        test_loader [torhc.utils.data.DataLoader]: testing data loader
        dataset [str]: dataset name
        device [torch.device]: device
        mat [bool]: whether the model is MAT-trained
        multi [bool]: whether to use multi-epsilon attacks
    Returns:
        None
    """
    if mat:
        model = auto_parallel(model.for_attack_version())
        model.eval()
    class_centers = model.module.class_centers.cpu()
        
    requested_attacks = MULTI_ATTACKS[dataset] if multi else ATTACKS[dataset] 
    fmodel, attacks = create_attacks(model, requested_attacks=requested_attacks, mlflow_track=True)

    features = []
    preds = []
    suc = {}
    a_features = {}
    a_preds = {}

    number = 0
    for b_idx, (data, target) in enumerate(test_loader):
        data, target = data.to(device), target.to(device)
        y = model(data)
        p = y.max(1)[1]
        features.append(feature_model(data).cpu())
        preds.append(p.cpu())
        correct = (p == target)
        for name, (success, adv) in attacks.attack_all_safe(data, target):
            success, adv = success.to(device), adv.to(device)
            if name not in suc:
                suc[name] = 0.
                a_features[name] = []
                a_preds[name] = []
            suc[name] += success.sum()
            # true advs
            success = correct & success
            if success.sum() != 0:
                true_adv = adv[success].to(device)
                a_features[name].append(feature_model(true_adv).cpu())
                a_preds[name].append(model(true_adv).max(1)[1].cpu())
        print("[{}/{}]".format(b_idx * target.shape[0], len(test_loader.dataset)), end="\r")
    print("")

    features = torch.cat(features, dim=0)
    preds = torch.cat(preds, dim=0)
    no_adv = []
    for name in attacks:
        if len(a_features[name]) == 0:
            no_adv.append(name)
            continue
        a_features[name] = torch.cat(a_features[name], dim=0)
        a_preds[name] = torch.cat(a_preds[name], dim=0)
    cos_stats = {'nat': [[], []]}
    l2_stats = {'nat': [[], []]}
    for c in range(NUM_CLASSES_):
        center = class_centers[c].cpu()
        c_features = features[preds == c]
        if c_features.shape[0] != 0:
            coses = t_cosine(c_features, center)
            l2s = t_l2_norm(c_features - center, dim=-1)
            if torch.isnan(coses).any():
                print(t_l2_norm(c_features, dim=-1))
            cos_stats['nat'][0].append(coses.mean())
            cos_stats['nat'][1].append(coses.std(unbiased=False))
            l2_stats['nat'][0].append(l2s.mean())
            l2_stats['nat'][1].append(l2s.std(unbiased=False))
        for name in attacks:
            if name in no_adv:
                continue
            a_c_features = a_features[name][a_preds[name] == c]
            if a_c_features.shape[0] != 0:
                a_coses = t_cosine(a_c_features, center)
                a_l2s = t_l2_norm(a_c_features - center, dim=-1)
                if torch.isnan(a_coses).any():
                    print(t_l2_norm(a_c_features, dim=-1))
                if name not in cos_stats:
                    cos_stats[name] = [[], []]
                    l2_stats[name] = [[], []]
                cos_stats[name][0].append(a_coses.mean())
                cos_stats[name][1].append(a_coses.std(unbiased=False))
                l2_stats[name][0].append(a_l2s.mean())
                l2_stats[name][1].append(a_l2s.std(unbiased=False))

    mlflow.log_metric("cosine_mean", float(np.mean(cos_stats['nat'][0])))
    mlflow.log_metric("cosine_std", float(np.mean(cos_stats['nat'][1])))
    mlflow.log_metric("l2_mean", float(np.mean(l2_stats['nat'][0])))
    mlflow.log_metric("l2_std", float(np.mean(l2_stats['nat'][1])))

    for name in attacks:
        if name not in no_adv:
            mlflow.log_metric("{}_cosine_mean".format(name), float(np.mean(cos_stats[name][0])))
            mlflow.log_metric("{}_cosine_std".format(name), float(np.mean(cos_stats[name][1])))
            mlflow.log_metric("{}_l2_mean".format(name), float(np.mean(l2_stats[name][0])))
            mlflow.log_metric("{}_l2_std".format(name), float(np.mean(l2_stats[name][1])))
        mlflow.log_metric("{}_robust_accuracy".format(name), 1 - (float(suc[name] / len(test_loader.dataset))))

    mlflow.log_metric("test_accuracy", score(model, device, test_loader))

def eval_robustness_adapt(model, test_loader, dataset, device, mat=False, multi=False, variant="cosine", ast=False, attack_type=None):
    """
    Evaluate adaptive robust accuracy of a model.
    Args:
        model [torch.nn.Module]: model to be evaluated
        test_loader [torhc.utils.data.DataLoader]: testing data loader
        dataset [str]: dataset name
        device [torch.device]: device
        mat [bool]: whether the model is MAT-trained
        multi [bool]: whether to use multi-epsilon attacks
        variant [str]: variant of MAT loss function
        ast [bool]: whether to consider assistant loss function
        attack_type [str]: attack type name
    Returns:
        None
    """
    global beta
    attack_type = attack_type.upper() if attack_type is not None else "TRADES"

    if mat:
        loss_func = lambda y, y_star: model.get_loss(y, y_star, device, variant=variant)
        ast_func = (lambda x_nat, x_adv: beta * model.cosloss(model(x_adv), model(x_nat), torch.ones(x_adv.shape[0]).to(device)).mean()) if ast else None
        pred_func = lambda x: model.predict_by_features(x, device)
    else:
        loss_func = torch.nn.functional.cross_entropy
        ast_func = (lambda x_nat, x_adv: beta * torch.nn.KLDivLoss(reduction='batchmean')(torch.nn.functional.log_softmax(model(x_adv), dim=1), torch.nn.functional.softmax(model(x_nat), dim=1))) if ast else None
        pred_func = lambda x: x.max(1)[1]

    requested_attacks = get_adapt_configs(attack_type, dataset, loss_func, pred_func, ast_func, multi=multi)
    fmodel, attacks = create_attacks(model, requested_attacks=requested_attacks, mlflow_track=True, pure_model=True)

    suc = {}
    norm = {}
    f_norm = {}

    number = 0
    for b_idx, (data, target) in enumerate(test_loader):
        data, target = data.to(device), target.to(device)
        y = model(data)
        correct = (pred_func(y) == target)
        for name in attacks:
            d = attacks.attacks[name][1]
            success, adv = attacks.attack_tensor(name, data, target, pred_func=pred_func)
            success, adv = success.to(device), adv.to(device)
            if name not in suc:
                norm[name] = []
                suc[name] = 0.
                f_norm[name] = []
            suc[name] += success.sum()
            success = correct & success
            if dataset == 'mnist':
                d_adv = adv.mean(dim=1)
                d_data = data.mean(dim=1)
            else:
                d_adv = adv
                d_data = data
            diff = (d_adv - d_data).view(d_data.shape[0], -1)
            p = d[1:]
            distances = t_lp_norm(diff, p, dim=-1)
            if success.sum() != 0:
                norm[name] += distances[success].tolist()
                adv_y = model(adv[success])
                f_norm[name] += adv_y.norm(2, dim=1).tolist()

        print("[{}/{}]".format(b_idx * target.shape[0], len(test_loader.dataset)), end="\r")
    print("")

    for name in attacks.keys():
        d = attacks.attacks[name][1]
        mlflow.log_metric("{}_robust_accuracy".format(name), 1 - (float(suc[name] / len(test_loader.dataset))))
        mlflow.log_metric("{}_feature_norm".format(name), np.mean(f_norm[name]))
        mlflow.log_metric("{}_mean_{}_norm".format(name, d), sum(norm[name]) / len(norm[name]))

    mlflow.log_metric("test_accuracy", score(model, device, test_loader, pred_func=pred_func))

def eval_robustness_tensor(model, test_loader, dataset, device, mat=False, multi=False, targeted=False):
    """
    Evaluate robust accuracy of a model using attacks designed for torch.Tensor.
    Args:
        model [torch.nn.Module]: model to be evaluated
        test_loader [torhc.utils.data.DataLoader]: testing data loader
        dataset [str]: dataset name
        device [torch.device]: device
        mat [bool]: whether the model is MAT-trained
        multi [bool]: whether to use multi-epsilon attacks
        targeted [bool]: whether to use targeted-attacks
    Returns:
        None
    """
    if mat:
        model = auto_parallel(model.for_attack_version()).to(device).eval()

    requested_attacks = MULTI_ATTACKS[dataset] if multi else ATTACKS[dataset]
    pred_func = lambda x: x.max(1)[1]

    fmodel, attacks = create_attacks(model, requested_attacks=requested_attacks, mlflow_track=True, pure_model=True)

    suc = {}
    norm = {}
    f_norm = {}

    number = 0
    for b_idx, (data, target) in enumerate(test_loader):
        data, target = data.to(device), target.to(device)
        y = model(data)
        correct = (pred_func(y) == target)

        if targeted:
            labels = target.clone().detach().to(device)
            for i, label in enumerate(target):
                l = torch.randint(NUM_CLASSES_, ())
                while l == label:
                    l = torch.randint(NUM_CLASSES_, ())
                labels[i] = l
        else:
            labels = target

        for name in attacks:
            d = attacks.attacks[name][1]
            success, adv = attacks.attack_tensor(name, data, labels, pred_func=pred_func)
            success, adv = success.to(device), adv.to(device)
            if name not in suc:
                norm[name] = []
                suc[name] = 0.
                f_norm[name] = []
            success = pred_func(model(adv)) != target
            suc[name] += success.sum()
            success = correct & success
            if dataset == 'mnist':
                d_adv = adv.mean(dim=1)
                d_data = data.mean(dim=1)
            else:
                d_adv = adv
                d_data = data
            diff = (d_adv - d_data).view(d_data.shape[0], -1)
            p = d[1:]
            distances = t_lp_norm(diff, p, dim=-1)
            if success.sum() != 0:
                norm[name] += distances[success].tolist()
                adv_y = model(adv[success])
                f_norm[name] += adv_y.norm(2, dim=1).tolist()

        print("[{}/{}]".format(b_idx * target.shape[0], len(test_loader.dataset)), end="\r")
    print("")

    for name in attacks.keys():
        d = attacks.attacks[name][1]
        mlflow.log_metric("{}_robust_accuracy".format(name), 1 - (float(suc[name] / len(test_loader.dataset))))
        mlflow.log_metric("{}_feature_norm".format(name), np.mean(f_norm[name]))
        mlflow.log_metric("{}_mean_{}_norm".format(name, d), sum(norm[name]) / len(norm[name]))

    mlflow.log_metric("test_accuracy", score(model, device, test_loader, pred_func=pred_func))

def eval_robustness_norm(model, feature_model, test_loader, dataset, device, mat=False, multi=False):
    """
    Evaluate average adversarial perturbation norm of a model.
    Args:
        model [torch.nn.Module]: model to be evaluated
        feature_model [torch.nn.Module]: model that outputs features (e.g. no-final-layer version of traditional CNNs)
        test_loader [torhc.utils.data.DataLoader]: testing data loader
        dataset [str]: dataset name
        device [torch.device]: device
        mat [bool]: whether the model is MAT-trained
        multi [bool]: whether to use multi-epsilon attacks
    Returns:
        None
    """
    if mat:
        model = auto_parallel(model.for_attack_version())
        model.eval()
    class_centers = model.module.class_centers.cpu()
    requested_attacks = MULTI_ATTACKS[dataset] if multi else ATTACKS[dataset] 
    fmodel, attacks = create_attacks(model, requested_attacks=requested_attacks, mlflow_track=True)

    features = []
    preds = []
    norm = {}
    a_features = {}
    a_preds = {}

    number = 0
    for b_idx, (data, target) in enumerate(test_loader):
        data, target = data.to(device), target.to(device)
        y = model(data)
        p = y.max(1)[1]
        features.append(feature_model(data).cpu())
        preds.append(p.cpu())
        correct = (p == target)
        for name, (success, adv) in attacks.attack_all_safe(data, target):
            d = attacks.attacks[name][1]
            success, adv = success.to(device), adv.to(device)
            if name not in norm:
                norm[name] = []
                a_features[name] = []
                a_preds[name] = []
            success = correct & success
            if dataset == 'mnist':
                d_adv = adv.mean(dim=1)
                d_data = data.mean(dim=1)
            else:
                d_adv = adv
                d_data = data
            diff = (d_adv - d_data).view(d_data.shape[0], -1)
            p = d[1:]
            distances = t_lp_norm(diff, p, dim=-1)
            if success.sum() != 0:
                norm[name] += distances[success].tolist()
                true_adv = adv[success].to(device)
                a_features[name].append(feature_model(true_adv).cpu())
                a_preds[name].append(model(true_adv).max(1)[1].cpu())
        print("[{}/{}]".format(b_idx * target.shape[0], len(test_loader.dataset)), end="\r")
    print("")

    features = torch.cat(features, dim=0)
    preds = torch.cat(preds, dim=0)
    no_adv = []
    for name in attacks:
        if len(a_features[name]) == 0:
            no_adv.append(name)
            continue
        a_features[name] = torch.cat(a_features[name], dim=0)
        a_preds[name] = torch.cat(a_preds[name], dim=0)
    cos_stats = {'nat': [[], []]}
    l2_stats = {'nat': [[], []]}
    for c in range(NUM_CLASSES_):
        center = class_centers[c].cpu()
        c_features = features[preds == c]
        if c_features.shape[0] != 0:
            coses = t_cosine(c_features, center)
            l2s = t_l2_norm(c_features - center, dim=-1)
            cos_stats['nat'][0].append(coses.mean())
            cos_stats['nat'][1].append(coses.std(unbiased=False))
            l2_stats['nat'][0].append(l2s.mean())
            l2_stats['nat'][1].append(l2s.std(unbiased=False))
        for name in attacks:
            if name in no_adv:
                continue
            a_c_features = a_features[name][a_preds[name] == c]
            if a_c_features.shape[0] != 0:
                a_coses = t_cosine(a_c_features, center)
                a_l2s = t_l2_norm(a_c_features - center, dim=-1)
                if name not in cos_stats:
                    cos_stats[name] = [[], []]
                    l2_stats[name] = [[], []]
                cos_stats[name][0].append(a_coses.mean())
                cos_stats[name][1].append(a_coses.std(unbiased=False))
                l2_stats[name][0].append(a_l2s.mean())
                l2_stats[name][1].append(a_l2s.std(unbiased=False))

    mlflow.log_metric("cosine_mean", float(np.mean(cos_stats['nat'][0])))
    mlflow.log_metric("cosine_std", float(np.mean(cos_stats['nat'][1])))
    mlflow.log_metric("l2_mean", float(np.mean(l2_stats['nat'][0])))
    mlflow.log_metric("l2_std", float(np.mean(l2_stats['nat'][1])))

    for name in attacks:
        d = attacks.attacks[name][1]
        if name not in no_adv:
            mlflow.log_metric("{}_cosine_mean".format(name), float(np.mean(cos_stats[name][0])))
            mlflow.log_metric("{}_cosine_std".format(name), float(np.mean(cos_stats[name][1])))
            mlflow.log_metric("{}_l2_mean".format(name), float(np.mean(l2_stats[name][0])))
            mlflow.log_metric("{}_l2_std".format(name), float(np.mean(l2_stats[name][1])))
            mlflow.log_metric("{}_min_{}_norm".format(name, d), min(norm[name]))
            mlflow.log_metric("{}_mean_{}_norm".format(name, d), sum(norm[name]) / len(norm[name]))

    mlflow.log_metric("test_accuracy", score(model, device, test_loader))

def eval_robustness_random(model, test_loader, dataset, device, mat=False, multi=False):
    """
    Evaluate robust accuracy of a model using random noises.
    Args:
        model [torch.nn.Module]: model to be evaluated
        test_loader [torhc.utils.data.DataLoader]: testing data loader
        dataset [str]: dataset name
        device [torch.device]: device
        mat [bool]: whether the model is MAT-trained
        multi [bool]: whether to use multi-epsilon attacks
    Returns:
        None
    """

    if mat:
        pred_func = lambda x: model.predict_by_features(x, device)
    else:
        pred_func = lambda x: x.max(1)[1]

    requested_attacks = {
        'mnist': AttacksBox()\
            .add('random', GaussianNoise, 'Linf', epsilon=0.3),
        'cifar': AttacksBox()\
            .add('random', GaussianNoise, 'Linf', epsilon=0.031),
        'svhn': AttacksBox()\
            .add('random', GaussianNoise, 'Linf', epsilon=0.031),
    }[dataset]
    fmodel, attacks = create_attacks(model, requested_attacks=requested_attacks, mlflow_track=True, pure_model=True)

    suc = {}

    number = 0
    for b_idx, (data, target) in enumerate(test_loader):
        data, target = data.to(device), target.to(device)
        y = model(data)
        correct = (pred_func(y) == target)
        for name in attacks:
            success_rate, adv = attacks.attack_tensor_random(name, data, target, pred_func=pred_func)
            success_rate, adv = success_rate.to(device), adv.to(device)
            if name not in suc:
                suc[name] = 0.
            suc[name] += success_rate.sum()

        print("[{}/{}]".format(b_idx * target.shape[0], len(test_loader.dataset)), end="\r")
    print("")

    for name in attacks.keys():
        mlflow.log_metric("{}_robust_accuracy".format(name), 1 - (float(suc[name] / len(test_loader.dataset))))

    mlflow.log_metric("test_accuracy", score(model, device, test_loader, pred_func=pred_func))

def eval_robustness_targeted(model, test_loader, dataset, device, mat=False, multi=False, random_target=False):
    """
    Evaluate robust accuracy of a model using targeted-attacks and corresponding evaluation criterion.
    Args:
        model [torch.nn.Module]: model to be evaluated
        test_loader [torhc.utils.data.DataLoader]: testing data loader
        dataset [str]: dataset name
        device [torch.device]: device
        mat [bool]: whether the model is MAT-trained
        multi [bool]: whether to use multi-epsilon attacks
        random_target [bool]: whether to use random targets (use all classes other than the label class as targets if not set)
    Returns:
        None
    """
    if mat:
        model = auto_parallel(model.for_attack_version())
        model.eval()
        
    requested_attacks = MULTI_ATTACKS[dataset] if multi else ATTACKS[dataset] 
    fmodel, attacks = create_attacks(model, requested_attacks=requested_attacks, mlflow_track=True)

    suc = {}
    data_groups = [[] for i in range(NUM_CLASSES_)]

    if random_target:
        target_groups = [[] for i in range(NUM_CLASSES_)]
        for b_idx, (data, target) in enumerate(test_loader):
            labels = target.clone().detach().cpu()
            for i, label in enumerate(target):
                l = torch.randint(NUM_CLASSES_, ())
                while l == label:
                    l = torch.randint(NUM_CLASSES_, ())
                labels[i] = l
            for c in range(NUM_CLASSES_):
                data_groups[c].append(data[labels == c]) # nxchxhxw
                target_groups[c].append(target[labels == c]) # n
    
        for c in range(NUM_CLASSES_):
            data_groups[c] = torch.cat(data_groups[c], dim=0) # Nxchxhxw
            target_groups[c] = torch.cat(target_groups[c], dim=0) # N

    else:
        for b_idx, (data, target) in enumerate(test_loader):
            for c in range(NUM_CLASSES_):
                data_groups[c].append(data[target == c]) # nxchxhxw

        for c in range(NUM_CLASSES_):
            data_groups[c] = torch.cat(data_groups[c], dim=0) # Nxchxhxw
    
    batch_size = 128
    processed = 0
    suc = {}

    all_amount = len(test_loader.dataset) * (1 if random_target else (NUM_CLASSES_ - 1))

    for c in range(NUM_CLASSES_):
        if random_target:
            N = data_groups[c].shape[0]
            for st_idx in range(0, N, batch_size):
                data = data_groups[c][st_idx:st_idx + batch_size]
                target = target_groups[c][st_idx:st_idx + batch_size]
                wrong = model(data.to(device)).max(1)[1] != target.to(device)
                for name, (success, adv) in attacks.attack_all_safe_targeted(data, target, c):
                    if name not in suc:
                        suc[name] = 0.
                    success, adv = success.to(device), adv.to(device)
                    suc[name] += (success | wrong).sum()
                processed += data.shape[0]
        else:
            suc_c = {name: [] for name in attacks}
            for ac in range(NUM_CLASSES_):
                if c == ac: continue
                for name in attacks:
                    suc_c[name].append([])
                N = data_groups[c].shape[0]
                for st_idx in range(0, N, batch_size):
                    data = data_groups[c][st_idx:st_idx + batch_size]
                    for name, (success, adv) in attacks.attack_all_safe_targeted(data, torch.full((data.shape[0],), c), ac):
                        if name not in suc:
                            suc[name] = 0.
                        success, adv = success.to(device), adv.to(device)
                        suc_c[name][-1] += success.tolist()
                    processed += data.shape[0]
            for name in attacks:
                suc_c_atk = torch.tensor(suc_c[name])
                suc[name] += suc_c_atk.any(dim=0).sum()
        print("[{}/{}]".format(processed, all_amount), end="\r")
    print("")    

    for name in attacks:
        mlflow.log_metric("{}_robust_accuracy".format(name), 1 - (float(suc[name] / all_amount)))

    mlflow.log_metric("test_accuracy", score(model, device, test_loader))

def run_TSNE(model, data_loader, device, model_name, tag):
    """
    Generate TSNE embeddings of features outputted by a MAT-trained model.
    Args:
        model [torch.nn.Module]: MAT-trained model
        data_loader [torch.utils.data.DataLoader]: data loader
        device [torch.device]: device
        model_name [str]: model name
        tag [str]: tag
    Returns:
        None
    """
    features = []
    preds = []
    with torch.no_grad():
        for data, target in data_loader:
            data, target = data.to(device), target.to(device)
            f = model(data)
            features.append(f)
            preds.append(model.predict_by_features(f, device))
    features = torch.cat(features, axis=0)
    preds = torch.cat(preds, axis=0)
    features = cast2numpy(features)
    preds = cast2numpy(preds)
    
    tsne = TSNE(n_components=2, metric='cosine', perplexity=50)
    embeddings = tsne.fit_transform(features)

    np.save(TSNE_path_maker.make_path(model_name, tag, "features", file_ext=".npy"), features)
    np.save(TSNE_path_maker.make_path(model_name, tag, "preds", file_ext=".npy"), preds)
    np.save(TSNE_path_maker.make_path(model_name, tag, "embed", file_ext=".npy"), embeddings)

    plt.clf()
    cmap = "tab10"
    scatter = plt.scatter(embeddings[:, 0], embeddings[:, 1], s=2, c=preds.astype(float), cmap=cmap)
    plt.savefig(TSNE_path_maker.make_path(model_name, tag, "tsne", file_ext=".png"))

def main():
    import argparse
    
    parser = argparse.ArgumentParser(description='PyTorch Model Stats Rendering')
    add_cnn_config_args(parser)
    parser.add_argument('-a', '--attacks', type=str, default=None,
                        help='attack configs to use')
    parser.add_argument('--split', type=str, default="test",
                        help='split of dataset to render')
    parser.add_argument('-b', '--batch-size', type=int, default=16,
                        help='batch size for each gpu')
    parser.add_argument('-m', '--mat', default=False, action='store_true',
                        help='if using MAT model')
    parser.add_argument('--mm', default=False, action='store_true',
                        help='whether to use max-mahalanobis class centers')
    parser.add_argument('--multi', default=False, action='store_true',
                        help='whether to test multiple perturbation strength')
    parser.add_argument('--norm', default=False, action='store_true',
                        help='whether to record norm statistics instead of accuracy')
    parser.add_argument('--adapt', default=False, action='store_true',
                        help='whether to conduct adapt attacks')
    parser.add_argument('--tensor', default=False, action='store_true',
                        help='whether to conduct attacks directly on tensors')
    parser.add_argument('--random', default=False, action='store_true',
                        help='whether to conduct random noise evaluation')
    parser.add_argument('--targeted', default=False, action='store_true',
                        help='whether to conduct targeted attacks')
    parser.add_argument('--variant', type=str, default="cosine",
                        help='variant of loss function to use (for adapt attacks)')
    parser.add_argument('--tsne', default=False, action='store_true',
                        help='whether to run tsne embedding (MAT-only)')
    parser.add_argument('--random-target', default=False, action='store_true',
                        help='whether to generate random targets for targeted attacks')
    
    args = parser.parse_args()

    if args.attacks is not None:
        global ATTACKS
        ATTACKS = ATTACK_CONFIGS[args.attacks.upper()]

    render_testing = args.split.lower() in ["test", "testing"]
    
    model_type = model_according_to(args.model, args.dataset)
    this_module = __import__(__name__)
    load_config_into(this_module, model_type, models, custom_losses, natural=args.natural, training=True)

    global NUM_CLASSES_
    NUM_CLASSES_ = NUM_CLASSES[dataset]

    device, kwargs = auto_device()
    model = use_model(**model_init_kwargs)
    if args.mat:
        model = MAT(model, MM=args.mm)
    model = auto_parallel(model)
    model_name = model_path_by(dataset, args.natural, mat=args.mat)
    checkpoint, tag = load_checkpoint(model_name, tag=args.tag, return_tag=True)
    model.load_state_dict(checkpoint)
    model.to(device)
    model.eval()

    train_loader, test_loader = get_data_loaders(torch.cuda.device_count() * args.batch_size, dataset, kwargs,
                                                transform_train=transform_train, transform_test=transform_test,
                                                download=args.download)

    data_loader = test_loader if render_testing else train_loader

    if not (args.adapt or args.tsne or args.random or args.targeted or args.tensor):
        if not args.mat:
            feature_model = auto_no_final_parallel(model).to(device)
            feature_model.eval()
            features = []
            preds = []
            print("Generating features...")
            with torch.no_grad():
                for data, target in train_loader:
                    data, target = data.to(device), target.to(device)
                    features.append(feature_model(data))
                    preds.append(model(data).max(1)[1])
            features = torch.cat(features, axis=0)
            preds = torch.cat(preds, axis=0)
            class_centers = []
            for c in range(NUM_CLASSES_):
                c_features = features[preds == c]
                class_centers.append(c_features.mean(dim=0, keepdim=True))
            print("Generation of features done.")
            class_centers = torch.cat(class_centers, axis=0)
            model.module.class_centers = class_centers
        else:
            feature_model = model
            feature_model.eval()

    if args.tsne:
        if not args.mat:
            raise ValueError("TSNE in this script is only for MAT models, for non-MAT models please use clustering.py")
        run_TSNE(model, data_loader, device, model_name, tag)
        exit()

    mlflow.set_tracking_uri(get_tracking_uri())

    mlflow.set_experiment("robustness")
    mlflow.start_run()

    mlflow.log_param('model_name', model_name)
    mlflow.log_param('tag', tag)

    model.eval()

    with torch.no_grad():
        if args.adapt:
            eval_robustness_adapt(model, data_loader, dataset, device, mat=args.mat, multi=args.multi, variant=args.variant, ast=not args.natural, attack_type=args.attacks)
        elif args.tensor:
            eval_robustness_tensor(model, data_loader, dataset, device, mat=args.mat, multi=args.multi, targeted=args.targeted)
        elif args.random:
            eval_robustness_random(model, data_loader, dataset, device, mat=args.mat, multi=args.multi)
        elif args.targeted:
            eval_robustness_targeted(model, data_loader, dataset, device, mat=args.mat, multi=args.multi, random_target=args.random_target)
        elif args.norm:
            eval_robustness_norm(model, feature_model, data_loader, dataset, device, mat=args.mat, multi=args.multi)
        else:
            eval_robustness(model, feature_model, data_loader, dataset, device, mat=args.mat, multi=args.multi)
    mlflow.end_run()
    
if __name__ == "__main__":
    main()
