import mlflow
import torch

import models
from configs.arg_helper import (add_cnn_config_args, model_according_to,
                                model_path_by)
from configs.attack_config import ATTACK_CONFIGS
from configs.cnn_config import load_config_into
from configs.constant import NUM_CLASSES
from data.dataset import get_data_loaders
from models.MAT import MAT
from utils import loss as custom_losses
from utils.adversarial import create_attacks
from utils.mlflow_helper import get_tracking_uri
from utils.model_manager import load_checkpoint
from utils.torch_helper import (auto_device, auto_no_final_parallel,
                                auto_parallel)

ATTACKS = ATTACK_CONFIGS['TRADES'] # Default attack type, you can replace "TRADES" with any string.
NUM_CLASSES_ = 10 # Default number of classes, you can replace 10 with any integer.

def main():
    import argparse
    
    parser = argparse.ArgumentParser(description='Template for Scripts Utilizing The Trained Models Using This Code Base')
    add_cnn_config_args(parser)
    parser.add_argument('-a', '--attacks', type=str, default=None,
                        help='attack configs to use')
    parser.add_argument('-m', '--mat', default=False, action='store_true',
                        help='if using MAT model')
    parser.add_argument('--mm', default=False, action='store_true',
                        help='whether to use max-mahalanobis class centers')
    
    args = parser.parse_args()

    if args.attacks is not None:
        global ATTACKS
        ATTACKS = ATTACK_CONFIGS[args.attacks.upper()]

    model_type = model_according_to(args.model, args.dataset)
    this_module = __import__(__name__)
    load_config_into(this_module, model_type, models, custom_losses, natural=args.natural)

    global NUM_CLASSES_
    NUM_CLASSES_ = NUM_CLASSES[dataset]

    device, kwargs = auto_device()
    model = use_model(**model_init_kwargs)
    if args.mat:
        model = MAT(model, MM=args.mm)
    model = auto_parallel(model)
    model_name = model_path_by(dataset, args.natural, mat=args.mat)
    checkpoint, tag = load_checkpoint(model_name, tag=args.tag, return_tag=True)
    model.load_state_dict(checkpoint)
    model.to(device)
    model.eval()

    """
    Note that the above code create non-MAT models that outputs logits or MAT models that outputs features.
    If you want to compute features, call
    ```
        model = auto_no_final_parallel(model).to(device).eval()
    ```
    for non-MAT models and do nothing for MAT models.

    If you want to compute logits, call
    ```
        model = model.for_attack_version().to(device).eval()
    ```
    for MAT models and do nothing for non-MAT models.

    And remember to put all tensors and models on the same device all the time,
    e.g.
    ```
        device, _ = auto_device()
        model = model.to(device)
        for data, target in train_loader:
            data, target = data.to(device), target.to(device)
            y = model(data)
            ...

            perturb = torch.rand(data.shape)
            yy = y + perturb.to(device)
            ...

    ```
    , otherwise error will be raised.
    """

    train_loader, test_loader = get_data_loaders(torch.cuda.device_count() * 16, dataset, kwargs,
                                                transform_train=transform_train, transform_test=transform_test,
                                                download=args.download)

    """If you need to log results on MLFlow"""
    mlflow.set_tracking_uri(get_tracking_uri())

    mlflow.set_experiment("template") # Set your experiment name here.
    mlflow.start_run()
    """===================================="""

    """If adversarial attacks are required"""
    attacks, fmodel = create_attacks(model, num_classes=NUM_CLASSES_, requested_attacks=ATTACKS, mlflow_track=True)
    # Make sure that the model is in the right form, i.e., for normal attacks like PGD, MAT models need to be transformed 
    # into models outputting logits first.
    # Better call model.eval() before calling create_attacks()
    # Set mlflow_track=True if the logging of attack names and params used is needed.

    # Check eval_robustness.py for how to use 'attacks'.
    """==================================="""

    # Write your code here.

    """If you need to log results on MLFlow"""
    mlflow.end_run()
    """===================================="""
