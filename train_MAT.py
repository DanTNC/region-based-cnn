import argparse
import os

import mlflow
import torch
import torch.nn.functional as F

import models
from configs.arg_helper import (add_cnn_config_args, model_according_to,
                                model_path_by)
from configs.cnn_config import load_config_into
from data.dataset import get_data_loaders
from models.MAT import MAT
from utils import loss as custom_losses
from utils.criteria import Scheduler, make_criteria
from utils.mlflow_helper import get_tracking_uri
from utils.model_manager import save_checkpoint
from utils.torch_helper import auto_device, auto_parallel
from utils.training_helper import score, training_loop

parser = argparse.ArgumentParser(description='PyTorch CNN MAT')

add_cnn_config_args(parser)
parser.add_argument('--print-interval', type=int, default=100,
                    help='print loss for each this # of iterations')
parser.add_argument('--mm', default=False, action='store_true',
                    help='whether to use max-mahalanobis class centers')
parser.add_argument('--no-rr', default=False, action='store_true',
                    help='whether to skip the random rotation for mm')
parser.add_argument('--variant', default='cosine', type=str,
                    help='which variant of loss to use')
parser.add_argument('--at', default=False, action='store_true',
                    help='whether to use adversarial training instead of BIBO loss')
args = parser.parse_args()

mlflow.set_tracking_uri(get_tracking_uri())
mlflow.set_experiment("MAT")

device, kwargs = auto_device()

model_type = model_according_to(args.model, args.dataset)
load_config_into(__import__(__name__), model_type, models, custom_losses, natural=args.natural, training=True)

global model_dir
model_dir += "-clus"

if not os.path.exists(model_dir):
    os.makedirs(model_dir)

model = use_model(pretrained=pretrained, **model_init_kwargs) if pretrained else use_model(**model_init_kwargs)
model = MAT(model, MM=args.mm, RR=not args.no_rr)
model = auto_parallel(model, verbose=False).to(device)

train_loader, test_loader = get_data_loaders(torch.cuda.device_count() * 16, dataset, kwargs, 
                                            transform_train=transform_train, transform_test=transform_test,
                                            download=args.download)

optimizer = getattr(torch.optim, opt)(model.parameters(), lr=lr, **opt_kwargs)

if "trigger_epochs" in early_stop_kwargs and early_stop_kwargs["trigger_epochs"] is not None:
    scheduler = Scheduler(lr, optimizer, lr_decay_rate, lr_decay_max_times, mlflow=mlflow)

    early_stop_kwargs["scheduler"] = scheduler

early_stop = make_criteria(**early_stop_kwargs)

print(early_stop.profile())

if args.natural:
    Loss = lambda y, y_star, x: model.module.get_loss(y, y_star, device, variant=args.variant)
else:
    if args.at:
        Loss = lambda y, y_star, x: custom_losses.AT_loss(model=model,
                                    x_natural=x,
                                    y=y_star,
                                    optimizer=optimizer,
                                    step_size=step_size,
                                    epsilon=epsilon_,
                                    perturb_steps=num_steps,
                                    loss_func=lambda y, y_star: model.module.get_loss(y, y_star, device, variant=args.variant),
                                    criterion=lambda f, y: torch.nn.CosineEmbeddingLoss(reduction='sum')(f, model.get_class_centers()[y].to(f.device), torch.ones(f.shape[0]).to(f.device)))
    else:
        Loss = lambda y, y_star, x: custom_losses.bibo_loss(model=model,
                                    x_natural=x,
                                    y=y_star,
                                    optimizer=optimizer,
                                    step_size=step_size,
                                    epsilon=epsilon_,
                                    perturb_steps=num_steps,
                                    beta=beta,
                                    loss_func=lambda y, y_star: model.module.get_loss(y, y_star, device, variant=args.variant))

e_loss = lambda y, y_star, x: model.module.get_loss(y, y_star, device, variant=args.variant)

save_func = lambda model, epoch: (
                    torch.save(model.state_dict(), '{}/model-epoch{:03d}.pt'.format(model_dir, epoch)),
                    mlflow.log_artifact('{}/model-epoch{:03d}.pt'.format(model_dir, epoch)),
                    print("got better model")
                )

pred_func = lambda y: model.module.predict_by_features(y, device)

mlflow.start_run()

mlflow.log_param('model', model_type)
mlflow.log_param('opt', str(optimizer).replace("\n", "&"))
mlflow.log_param('dataset', dataset)
mlflow.log_param('loss', str(loss).replace("\n", "&"))
mlflow.log_param('learning_rate', lr)
mlflow.log_param('epsilon', epsilon_)
mlflow.log_param('num_steps', num_steps)
mlflow.log_param('step_size', step_size)
mlflow.log_param('beta', beta)
mlflow.log_param('model_dir', model_dir)
mlflow.log_param('pretrained', str(pretrained))
mlflow.log_param('early_stop', str(early_stop.profile()))
mlflow.log_param('natural', str(args.natural))
mlflow.log_param('mm', str(args.mm))
mlflow.log_param('rr', str(not args.no_rr))
mlflow.log_param('at', str(args.at))

mlflow.log_param('loss_variant', args.variant)

epoch_st = 0

model, epoch = training_loop(model, train_loader, Loss, optimizer, early_stop, device,
                print_interval=args.print_interval,
                test_data_loader=test_loader, save_func=save_func, mlflow_track=True,
                pred_func=pred_func, e_loss=e_loss)

fse = early_stop.final_saved_epoch + epoch_st
test_score = score(model, device, test_loader, pred_func=pred_func)
print("final saved epoch:", fse)
print("testing accuracy:", test_score)

mlflow.log_metric('final_saved_epoch', fse)
mlflow.log_metric('test_acc', test_score)
mlflow.set_tag('stopping reason', str(early_stop))

yn = input("upload the final saved model to S3? [y/n]\n")
if yn.upper() == "Y":
    tag = input("set a tag for this model: (use timestamp if left blank)\n")
    if tag == "":
        tag = "latest"
    state_dict = torch.load('{}/model-epoch{:03d}.pt'.format(model_dir, fse))
    tag = save_checkpoint(model_path_by(dataset, args.natural, mat=True), state_dict, tag=tag)

    mlflow.set_tag('tag', tag)

mlflow.end_run()
