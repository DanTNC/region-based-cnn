import foolbox.attacks as Attacks
import numpy as np
import torch
from utils.adversarial import SPSA, Adapt_BIM, Adapt_DDN, AttacksBox

ATTACK_CONFIGS = {# attack configs for each dataset and attack type
    'TRADES': {
        'mnist': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.3, iterations=40, stepsize=0.01, random_start=True),
        'fmnist': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.3, iterations=40, stepsize=0.01, random_start=True),
        'cifar': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.031, iterations=20, stepsize=0.003, random_start=True),
        'svhn': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.047, iterations=20, stepsize=0.003, random_start=True),
    },
    'TRADES10': {
        'mnist': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.3, iterations=10, stepsize=0.01, random_start=True),
        'fmnist': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.3, iterations=10, stepsize=0.01, random_start=True),
        'cifar': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.031, iterations=10, stepsize=0.003, random_start=True),
        'svhn': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.047, iterations=10, stepsize=0.003, random_start=True),
    },
    'TRADES50': {
        'mnist': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.3, iterations=50, stepsize=0.01, random_start=True),
        'fmnist': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.3, iterations=50, stepsize=0.01, random_start=True),
        'cifar': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.031, iterations=50, stepsize=0.003, random_start=True),
        'svhn': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.047, iterations=50, stepsize=0.003, random_start=True),
    },
    'TRADES10_D': {
        'mnist': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.6, iterations=10, stepsize=0.01, random_start=True),
        'fmnist': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.6, iterations=10, stepsize=0.01, random_start=True),
        'cifar': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.062, iterations=10, stepsize=0.003, random_start=True),
        'svhn': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.094, iterations=10, stepsize=0.003, random_start=True),
    },
    'TRADES50_D': {
        'mnist': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.6, iterations=50, stepsize=0.01, random_start=True),
        'fmnist': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.6, iterations=50, stepsize=0.01, random_start=True),
        'cifar': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.062, iterations=50, stepsize=0.003, random_start=True),
        'svhn': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.094, iterations=50, stepsize=0.003, random_start=True),
    },
    'ALL': {
        'mnist': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.3, iterations=40, stepsize=0.01, random_start=True)\
            .add('DDN', Attacks.DecoupledDirectionNormL2Attack, 'L2')\
            .add('C_and_W', Attacks.CarliniWagnerL2Attack, 'L2', max_iterations=100, learning_rate=0.01, initial_const=1.0, binary_search_steps=1),
        'fmnist': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.3, iterations=40, stepsize=0.01, random_start=True)\
            .add('DDN', Attacks.DecoupledDirectionNormL2Attack, 'L2')\
            .add('C_and_W', Attacks.CarliniWagnerL2Attack, 'L2', max_iterations=100, learning_rate=0.01, initial_const=1.0, binary_search_steps=1),
        'cifar': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.031, iterations=20, stepsize=0.003, random_start=True)\
            .add('DDN', Attacks.DecoupledDirectionNormL2Attack, 'L2')\
            .add('C_and_W', Attacks.CarliniWagnerL2Attack, 'L2', max_iterations=100, learning_rate=0.01, initial_const=1.0, binary_search_steps=1),
        'svhn': AttacksBox()\
            .add('LinfBIM', Attacks.BIM, 'Linf', binary_search=False, epsilon=0.047, iterations=20, stepsize=0.003, random_start=True)\
            .add('DDN', Attacks.DecoupledDirectionNormL2Attack, 'L2')\
            .add('C_and_W', Attacks.CarliniWagnerL2Attack, 'L2', max_iterations=100, learning_rate=0.01, initial_const=1.0, binary_search_steps=1),
    },
    'BLACK': {
        'mnist': AttacksBox()\
            .add('Gen', Attacks.GenAttack, 'Linf', binary_search=False, epsilon=0.3),
        'fmnist': AttacksBox()\
            .add('Gen', Attacks.GenAttack, 'Linf', binary_search=False, epsilon=0.3),
        'cifar': AttacksBox()\
            .add('Gen', Attacks.GenAttack, 'Linf', binary_search=False, epsilon=0.031),
        'svhn': AttacksBox()\
            .add('Gen', Attacks.GenAttack, 'Linf', binary_search=False, epsilon=0.047),
    },
    'SPSA': {
        'mnist': AttacksBox()\
            .add('SPSA', SPSA, 'Linf', epsilon=0.3),
        'fmnist': AttacksBox()\
            .add('SPSA', SPSA, 'Linf', epsilon=0.3),
        'cifar': AttacksBox()\
            .add('SPSA', SPSA, 'Linf', epsilon=0.031),
        'svhn': AttacksBox()\
            .add('SPSA', SPSA, 'Linf', epsilon=0.047),
    },
    'DDN': {
        'mnist': AttacksBox()\
            .add('DDN', Attacks.DecoupledDirectionNormL2Attack, 'L2'),
        'fmnist': AttacksBox()\
            .add('DDN', Attacks.DecoupledDirectionNormL2Attack, 'L2'),
        'cifar': AttacksBox()\
            .add('DDN', Attacks.DecoupledDirectionNormL2Attack, 'L2'),
        'svhn': AttacksBox()\
            .add('DDN', Attacks.DecoupledDirectionNormL2Attack, 'L2'),
    },
    'NORM': {
        'mnist': AttacksBox()\
            .add('C_and_W', Attacks.CarliniWagnerL2Attack, 'L2', max_iterations=100, learning_rate=0.01, initial_const=1.0, binary_search_steps=1)\
            .add('DDN', Attacks.DecoupledDirectionNormL2Attack, 'L2'),
        'fmnist': AttacksBox()\
            .add('C_and_W', Attacks.CarliniWagnerL2Attack, 'L2', max_iterations=100, learning_rate=0.01, initial_const=1.0, binary_search_steps=1)\
            .add('DDN', Attacks.DecoupledDirectionNormL2Attack, 'L2'),
        'cifar': AttacksBox()\
            .add('C_and_W', Attacks.CarliniWagnerL2Attack, 'L2', max_iterations=100, learning_rate=0.01, initial_const=1.0, binary_search_steps=1)\
            .add('DDN', Attacks.DecoupledDirectionNormL2Attack, 'L2'),
        'svhn': AttacksBox()\
            .add('C_and_W', Attacks.CarliniWagnerL2Attack, 'L2', max_iterations=100, learning_rate=0.01, initial_const=1.0, binary_search_steps=1)\
            .add('DDN', Attacks.DecoupledDirectionNormL2Attack, 'L2'),
    },
    'TARGETED_TRADES': {
        'mnist': AttacksBox()\
            .add('AdaptLinfBIM', Adapt_BIM, 'Linf', lambda y, y_star: -torch.nn.functional.cross_entropy(y, y_star), binary_search=False, epsilon=0.3, iterations=40, stepsize=0.01, random_start=True),
        'cifar': AttacksBox()\
            .add('AdaptLinfBIM', Adapt_BIM, 'Linf', lambda y, y_star: -torch.nn.functional.cross_entropy(y, y_star), binary_search=False, epsilon=0.031, iterations=20, stepsize=0.003, random_start=True),
        'svhn': AttacksBox()\
            .add('AdaptLinfBIM', Adapt_BIM, 'Linf', lambda y, y_star: -torch.nn.functional.cross_entropy(y, y_star), binary_search=False, epsilon=0.031, iterations=20, stepsize=0.003, random_start=True),
    }
}

def multi_epsilon(ab, range_, precision, prefix, *args, **kwargs):
    """
    Create multiple attacks for a given range of epsilons. (For constructing robust accuracy vs. epsilon curve.)
    Args:
        ab [utils.adversarial.AttacksBox]: container for attacks
        range_ [numpy.array]: range of epsilons
        precision [int]: n-th decimal digit the epsilon should be rounded to (to eliminate the inprecision of float numbers introduced by np.linspace)
        prefix [str]: prefix for the attack name
        args [list]: list of parameters for the attack
        kwargs [dict]: keyword parameters for the attack
    Returns:
        [utils.adversarial.AttacksBox] the AttacksBox object
    """
    for eps in range_:
        eps = np.round(eps, precision)
        ab.add(prefix + str(eps), *args, epsilon=eps, **kwargs)
    return ab

ATTACK_MULTIPLE_CONFIGS = {# attack configs of multiple-epsilon attacks for each dataset and attack type
    'TRADES': {
        'mnist': multi_epsilon(AttacksBox(), np.linspace(0.1, 0.5, 5), 1, "LinfBIM", Attacks.BIM, 'Linf', binary_search=False, iterations=40, stepsize=0.01, random_start=True),
        'fmnist': multi_epsilon(AttacksBox(), np.linspace(0.1, 0.5, 5), 1, "LinfBIM", Attacks.BIM, 'Linf', binary_search=False, iterations=40, stepsize=0.01, random_start=True),
        'cifar': multi_epsilon(AttacksBox(), np.linspace(0.011, 0.051, 5), 3, "LinfBIM", Attacks.BIM, 'Linf', binary_search=False, iterations=20, stepsize=0.003, random_start=True),
        'svhn': multi_epsilon(AttacksBox(), np.linspace(0.011, 0.051, 5), 3, "LinfBIM", Attacks.BIM, 'Linf', binary_search=False, iterations=20, stepsize=0.003, random_start=True),
    },
}

def get_adapt_configs(attack_type, dataset, loss_func, pred_func, ast_func, multi=False):
    """
    Construct adaptive attacks for given paramters.
    Args:
        attack_type [str]: attack type (e.g., TRADES)
        dataset [str]: dataset name
        loss_func [Callable]: loss function for adaptive attacks to maximize
        pred_func [Callable]: prediction function for adaptive attacks to examine success samples
        ast_func [Callable]: assistant loss function, which has different signature of loss_func
        multi [bool]: whether to construct multiple-epsilon attacks
    Returns:
    [utils.adversarial.AttacksBox] AttacksBox containing generated attacks
    """
    if multi:
        requested_attacks = {
            'TRADES':{
                'mnist': multi_epsilon(AttacksBox(), np.linspace(0.1, 0.5, 5), 1, "AdaptLinfBIM", Adapt_BIM, 'Linf', loss_func, binary_search=False, iterations=40, stepsize=0.01, random_start=True, ast_func=ast_func),
                'fmnist': multi_epsilon(AttacksBox(), np.linspace(0.1, 0.5, 5), 1, "AdaptLinfBIM", Adapt_BIM, 'Linf', loss_func, binary_search=False, iterations=40, stepsize=0.01, random_start=True, ast_func=ast_func),
                'cifar': multi_epsilon(AttacksBox(), np.linspace(0.011, 0.051, 5), 3, "AdaptLinfBIM", Adapt_BIM, 'Linf', loss_func, binary_search=False, iterations=20, stepsize=0.003, random_start=True, ast_func=ast_func),
                'svhn': multi_epsilon(AttacksBox(), np.linspace(0.011, 0.051, 5), 3, "AdaptLinfBIM", Adapt_BIM, 'Linf', loss_func, binary_search=False, iterations=20, stepsize=0.003, random_start=True, ast_func=ast_func),
            },
        }[attack_type][dataset]
    else:
        requested_attacks = {
            'TRADES':{
                'mnist': AttacksBox()\
                    .add('AdaptLinfBIM', Adapt_BIM, 'Linf', loss_func, binary_search=False, epsilon=0.3, iterations=40, stepsize=0.01, random_start=True, ast_func=ast_func),
                'fmnist': AttacksBox()\
                    .add('AdaptLinfBIM', Adapt_BIM, 'Linf', loss_func, binary_search=False, epsilon=0.3, iterations=40, stepsize=0.01, random_start=True, ast_func=ast_func),
                'cifar': AttacksBox()\
                    .add('AdaptLinfBIM', Adapt_BIM, 'Linf', loss_func, binary_search=False, epsilon=0.031, iterations=20, stepsize=0.003, random_start=True, ast_func=ast_func),
                'svhn': AttacksBox()\
                    .add('AdaptLinfBIM', Adapt_BIM, 'Linf', loss_func, binary_search=False, epsilon=0.047, iterations=20, stepsize=0.003, random_start=True, ast_func=ast_func),
            },
            'NORM':{
                'mnist': AttacksBox()\
                    .add('AdaptDDN', Adapt_DDN, 'L2', loss_func, pred_func=pred_func, ast_func=ast_func),
                'fmnist': AttacksBox()\
                    .add('AdaptDDN', Adapt_DDN, 'L2', loss_func, pred_func=pred_func, ast_func=ast_func),
                'cifar': AttacksBox()\
                    .add('AdaptDDN', Adapt_DDN, 'L2', loss_func, pred_func=pred_func, ast_func=ast_func),
                'svhn': AttacksBox()\
                    .add('AdaptDDN', Adapt_DDN, 'L2', loss_func, pred_func=pred_func, ast_func=ast_func),
            },
        }[attack_type][dataset]
        
    return requested_attacks
