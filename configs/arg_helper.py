from configs.cnn_config import DATASET_MODEL_MAPPING

def model_according_to(model_, dataset_):
    """
    Infer model (architecture) from dataset by DATASET_MODEL_MAPPING.
    Or simply return model name when it is given.
    Args:
        model_ [str]: model architecture name
        dataset_ [str]: dataset name
    Returns:
        [str] model name
    """
    if model_ is not None:
        return model_
    else:
        return DATASET_MODEL_MAPPING[dataset_]

def model_path_by(dataset_, natural_, mat):
    """
    Construct path which encodes dataset and training method.
    Note: '-TRADES' is a meaningless postfix (a mistake), which is kept because of the difficulty of renaming existing model weight files.
    Args:
        dataset_ [str]: dataset name
        natural_ [bool]: whether is trained using natural training
        mat [bool]: whether is trained using MAT
    Returns:
        [str] model path (for local weight-saving directory and key string for distinguishing weight files on S3.)
    """
    return "model-{}-{}{}-TRADES".format(dataset_, "natural" if natural_ else "trades", "-clus" if mat else "")

def add_cnn_config_args(parser):
    """
    Add command-line arguments for common CNN configs.
    Args:
        parser [argparse.ArgumentParser]: command-line argument parser
    Returns:
        None
    """
    parser.add_argument('--natural', action='store_true', default=False,
                        help='training without adversarial defense')
    parser.add_argument('--model', default=None,
                        help='model to train')
    parser.add_argument('--dataset', type=str, default="mnist",
                        help='dataset to train (ignored if model is set)')
    parser.add_argument('--tag', type=str, default="latest",
                        help='tag of model weights to load')
    parser.add_argument('--download', action='store_true', default=False,
                        help='whether to download the dataset')