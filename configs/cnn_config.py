import json

import torch
from torchvision import transforms

model_dir_root = json.load(open('path_setting.json', 'r'))['STORAGE_ROOT'] + "/"

DATASET_MODEL_MAPPING = {# mapping from dataset to model architecture
    "mnist": "vgg16_m",
    "fmnist": "vgg16_f",
    "cifar": "wideresnet",
    "stl": "resnext101",
    "tinyimagenet": "resnext101_t",
    "svhn": "resnet18",
}

# mapping from model architecture to dataset
MODEL_DATASET_MAPPING = {v: k for k, v in DATASET_MODEL_MAPPING.items()}

def load_config_into(module, model, models, custom_losses, natural=False, training=False):
    """
    Set CNN-related (and training-related) parameters as global variables for a module.
    Args:
        module [Module]: module to load parameters
        model [str]: model architecture name
        models [Package]: the package containing all model definitions (models/)
        custom_losses [Module]: the module containing all custom loss functions (utils/loss.py)
        natural [bool]: whether is trained using natural training
        training [bool]: whether to load training-related parameters
    Returns:
        None
    Raises:
        ValueError: if @model is not valid
    """
    if training:
        module.lr_decay_rate = 0.1
        module.lr_decay_max_times = 3
        module.opt_kwargs = {}
    module.model_init_kwargs = {}
    if model == "smallcnn":
        module.use_model = models.small_cnn.SmallCNN
        module.transform_train = transforms.ToTensor()
        module.transform_test = transforms.ToTensor()
        module.loss = torch.nn.CrossEntropyLoss()
        if training:
            module.lr = 1e-2
            module.epsilon_ = 0.3
            module.num_steps = 40
            module.step_size = 0.01
            module.beta = 5.0
            module.model_dir = model_dir_root + 'TRADES-model-mnist-smallCNN_'
            module.pretrained = False
            module.early_stop_kwargs = {'strategy': 'base', 'MAX_EPOCHS': 100, 'log_interval': 5, 'trigger_epochs': [54, 74, 89]}
            module.opt = 'Adam'
    elif model == "resnet18":
        module.use_model = models.resnet_wrapper.resnet18
        module.transform_train = transforms.Compose([
            transforms.ToTensor(),
        ])
        module.transform_test = transforms.ToTensor()
        module.loss = torch.nn.CrossEntropyLoss()
        if training:
            module.lr = 0.1
            module.epsilon_ = 0.047
            module.num_steps = 10
            module.step_size = 0.007
            module.beta = 6.0
            module.model_dir = model_dir_root + 'TRADES-model-svhn-resnet18'
            module.pretrained = False
            module.early_stop_kwargs = {'strategy': 'base', 'MAX_EPOCHS': 100, 'log_interval': 5, 'trigger_epochs': [74, 89, 99]}
            module.opt_kwargs = {'momentum': 0.9}
            module.opt = 'SGD'
    elif model == "wideresnet":
        module.use_model = models.wideresnet.WideResNet
        module.transform_train = transforms.Compose([
            transforms.RandomCrop(32, padding=4),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
        ])
        module.transform_test = transforms.ToTensor()
        module.loss = torch.nn.CrossEntropyLoss()
        if training:
            module.lr = 0.01
            module.epsilon_ = 0.031
            module.num_steps = 10
            module.step_size = 0.007
            module.beta = 3.0
            module.model_dir = model_dir_root + 'TRADES-model-cifar-wideResNetReLU'
            module.pretrained = False
            module.early_stop_kwargs = {'strategy': 'base', 'MAX_EPOCHS': 100, 'log_interval': 5, 'trigger_epochs': [74, 89, 99]}
            module.opt_kwargs = {'momentum': 0.9, 'weight_decay': 2e-4}
            module.opt = 'SGD'
    elif model == "vgg16_m":
        module.use_model = models.vgg_wrapper.vgg16_bn
        module.transform_train = transforms.Compose([
            transforms.Resize(32),
            transforms.ToTensor(),
            transforms.Lambda(lambda x: x.repeat(3, 1, 1)),
        ])
        module.transform_test = transforms.Compose([
            transforms.Resize(32),
            transforms.ToTensor(),
            transforms.Lambda(lambda x: x.repeat(3, 1, 1)),
        ])
        module.loss = torch.nn.CrossEntropyLoss()
        if training:
            module.lr = 5e-3
            module.epsilon_ = 0.3
            module.num_steps = 40
            module.step_size = 0.01
            module.beta = 3.0
            module.model_dir = model_dir_root + 'TRADES-model-mnist-vgg16_bn'
            module.pretrained = False
            module.early_stop_kwargs = {'strategy': 'base', 'MAX_EPOCHS': 100, 'log_interval': 5, 'trigger_epochs': [54, 74, 89]}
            module.opt_kwargs = {'momentum': 0.9}
            module.opt = 'SGD'
    elif model == "vgg16_f":
        module.use_model = models.vgg_wrapper.vgg16_bn
        module.transform_train = transforms.Compose([
            transforms.Resize(32),
            transforms.ToTensor(),
        ])
        module.transform_test = transforms.Compose([
            transforms.Resize(32),
            transforms.ToTensor(),
        ])
        module.loss = torch.nn.CrossEntropyLoss()
        module.model_init_kwargs['gray_level'] = True
        if training:
            module.lr = 5e-3
            module.epsilon_ = 0.3
            module.num_steps = 40
            module.step_size = 0.01
            module.beta = 3.0
            module.model_dir = model_dir_root + 'TRADES-model-fmnist-vgg16_bn'
            module.pretrained = False
            module.early_stop_kwargs = {'strategy': 'base', 'MAX_EPOCHS': 100, 'log_interval': 5, 'trigger_epochs': [54, 74, 89]}
            module.opt_kwargs = {'momentum': 0.9}
            module.opt = 'SGD'
    elif model == "resnext101":
        module.use_model = models.resnet_wrapper.resnext101
        module.transform_train = transforms.Compose([
            transforms.RandomResizedCrop(96),
            transforms.RandomOrder([
                transforms.RandomHorizontalFlip(),
                transforms.RandomRotation(10)
            ]),
            transforms.ToTensor(),
        ])
        module.transform_test = transforms.ToTensor()
        module.loss = custom_losses.AnchorLoss(0.5)
        if training:
            module.lr = 1e-5
            module.epsilon_ = 0.031
            module.num_steps = 20
            module.step_size = 0.003
            module.beta = 1.0
            module.model_dir = model_dir_root + 'model-stl10-resnext101'
            module.pretrained = True
            module.early_stop_kwargs = {'strategy': 'base', 'MAX_EPOCHS': 100, 'log_interval': 1, 'trigger_epochs': [74, 89, 99]}
            module.opt = 'Adam'
    elif model == "resnext101_t":
        module.use_model = models.resnet_wrapper.resnext101
        module.transform_train = transforms.Compose([
            transforms.RandomResizedCrop(64),
            transforms.RandomOrder([
                transforms.RandomHorizontalFlip(),
                transforms.RandomRotation(10)
            ]),
            transforms.ToTensor(),
        ])
        module.transform_test = transforms.ToTensor()
        module.loss = torch.nn.CrossEntropyLoss()
        module.model_init_kwargs['num_classes'] = 200
        if training:
            module.lr = 5e-2
            module.epsilon_ = 0.031
            module.num_steps = 10
            module.step_size = 0.003
            module.beta = 1.0
            module.model_dir = model_dir_root + 'model-tiny-resnext101'
            module.pretrained = False
            module.early_stop_kwargs = {'strategy': 'base', 'MAX_EPOCHS': 100, 'log_interval': 1, 'trigger_epochs': [74, 89, 99]}
            module.opt = 'SGD'
            module.opt_kwargs = {'momentum': 0.9}
    elif model == "resnext101_c100":
        module.use_model = models.resnet_wrapper.resnext101
        module.transform_train = transforms.Compose([
            transforms.RandomResizedCrop(96),
            transforms.RandomOrder([
                transforms.RandomHorizontalFlip(),
                transforms.RandomRotation(10)
            ]),
            transforms.ToTensor(),
        ])
        module.transform_test = transforms.ToTensor()
        module.loss = torch.nn.CrossEntropyLoss()
        module.model_init_kwargs['num_classes'] = 100
        if training:
            module.lr = 0.1
            module.epsilon_ = 0.031
            module.num_steps = 20
            module.step_size = 0.003
            module.beta = 1.0
            module.model_dir = model_dir_root + 'model-cifar100-resnext101'
            module.pretrained = True
            module.early_stop_kwargs = {'strategy': 'base', 'MAX_EPOCHS': 100, 'log_interval': 1, 'trigger_epochs': [74, 89, 99]}
            module.opt_kwargs = {'momentum': 0.9, 'weight_decay': 2e-4}
            module.opt = 'SGD'
    else:
        raise ValueError("unknown model")
    module.dataset = MODEL_DATASET_MAPPING[model]
    if natural and training:
        module.model_dir += '-natural'
