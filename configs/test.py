import unittest

from configs.arg_helper import (add_cnn_config_args, model_according_to,
                                model_path_by)
from configs.cnn_config import DATASET_MODEL_MAPPING


class ArgHelpersTest(unittest.TestCase):
    def test_model_according_to(self):
            self.assertEqual(model_according_to('smallcnn', None), 'smallcnn')
            self.assertEqual(model_according_to('smallcnn', 'mnist'), 'smallcnn')
            self.assertEqual(model_according_to(None, 'mnist'), 'smallcnn')
            with self.assertRaises(KeyError):
                model_according_to(None, 'st')

    def test_model_path_by(self):
        self.assertEqual(model_path_by('mnist', False), 'model-mnist-trades')

    def test_add_args(self):
        import argparse
        parser = argparse.ArgumentParser()
        add_cnn_config_args(parser)
        args = parser.parse_args()
        for arg in ['dataset', 'model', 'natural', 'tag']:
            self.assertIn(arg, args)
        self.assertEqual(args.tag, 'latest')
