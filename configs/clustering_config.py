def load_config_into(module, model, natural=False):
    """
    Set clutering-related parameters as global variables for a module.
    Args:
        module [Module]: module to load parameters
        model [str]: model architecture name
        natural [bool]: whether is trained using natural training
    Returns:
        None
    Raises:
        ValueError: if @model is not valid
    """
    if model == "smallcnn":
        module.search_range = (15, 36, 5)
        if natural:
            module.best_threshold = 320 # 320
        else:
            module.best_threshold = 255 # 255
    elif model == "wideresnet_no_relu":
        if natural:
            module.search_range = (25, 31, 1)
            module.best_threshold = 30
        else:
            module.search_range = (1, 6, 1)
            module.best_threshold = 3 ###
    elif model == "wideresnet":
        if natural:
            module.search_range = (20, 100, 1)
            module.best_threshold = 77 # 77
        else:
            module.search_range = (20, 100, 1)
            module.best_threshold = 16.5 # 16.5 # 24!, 10, 8: June10, 15.5!, 6: May26, 16.5!: TRADES_paper
    elif model == "vgg19":
        if natural:
            module.search_range = (1, 10, 1)
            module.best_threshold = 9
        else:
            module.search_range = (6, 10, 1)
            module.best_threshold = 14
    elif model == "resnet34":
        module.search_range = (15, 36, 5)
        if natural:
            module.best_threshold = 27.5
        else:
            module.best_threshold = 117.5
    elif model == "resnext101":
        module.search_range = (5, 26, 5)
        if natural:
            module.best_threshold = 102.5
        else:
            module.best_threshold = 87.5
    else:
        raise ValueError("unknown model")