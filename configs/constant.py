NUM_CLASSES = {# number of classes in each dataset
    'mnist': 10,
    'cifar': 10,
    'svhn': 10,
    'fmnist': 10,
    'stl': 10,
    'cifar100': 100,
    'tinyimagenet': 200,
}