# helpers for configurations of command-line arguments and model settings

## Introduction of codebase

### Modules
* **arg_helpers.py**  
helpers for command-line arguments

* **cnn_config.py**  
configurations about model-dataset pair and training hyper-parameters  

* **clustering_config.py**  
configurations about threshold searching settings used in clustering.py  

* **constants.py**  
constants used across all scripts 

* **attack_config.py**  
configurations about adversarial attack parameters
