import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from skimage.io import imread, imsave

matplotlib.rcParams.update({'font.size': 22})

def plot(dataset):
    # cifar
    # dataset = "mnist"
    if dataset == "mnist":
        title = "MNIST"
        xs = [0.1, 0.2, 0.3, 0.4, 0.5]
    else:
        title = "CIFAR10"
        xs = [0.011, 0.021, 0.031, 0.041, 0.051]

    with open("curve/curve_{}.csv".format(dataset), "r") as f:
        lines = f.readlines()

    plt.figure(figsize=(8, 6))
    markers = ['^', 's', 'o']
    for i, line in enumerate(lines):
        values = line.strip().split(",")
        name = values[0]
        ys = list(map(float, values[1:]))
        plt.plot(xs, ys, label=name, marker=markers[i], linewidth=5, markersize=20)
    plt.title(title)
    plt.xticks(xs)
    plt.grid()
    plt.legend()
    plt.xlabel('perturbation strength')
    plt.ylabel('robust accuracy')
    plt.tight_layout()
    plt.savefig("test_curve_{}.png".format(dataset), dpi=160)

def combine():
    m = imread("test_curve_mnist.png")
    c = imread("test_curve_cifar.png")
    w = np.concatenate([m, c], axis=1)
    imsave("test_curve.png", w)

plot("mnist")
plot("cifar")
combine()
